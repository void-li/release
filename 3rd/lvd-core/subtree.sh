#!/bin/sh
set -e -x

# config

remote="lvd-core"
branch="${2:-master}"
giturl="git@gitlab.com:void-li/${remote}.git"

# source

case "$1" in
  make)
    git remote add "$remote" "$giturl"
    ;;

  init)
    git fetch "$remote" "$branch"
    git subtree --prefix "3rd/$remote" add "$remote" "$branch" --squash
    ;;

  pull)
    git fetch "$remote" "$branch"
    git subtree --prefix "$(dirname "$0" | sed 's,^\./,,')" pull "$remote" "$branch" --squash
    ;;

  push)
    git fetch "$remote" "$branch"
    git subtree --prefix "$(dirname "$0" | sed 's,^\./,,')" push "$remote" "$branch"
    ;;

  *)
    echo usage: $(basename "$0") \<make, init, pull, push\>
    exit 1
    ;;
esac
