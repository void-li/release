/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QByteArray>
#include <QString>
#include <QStringRef>

// ----------

namespace lvd {
namespace __impl__ {

template <class R, class T, class U>
R strip(const T& thing,
        const U& skip,
        int      limit_l,
        int      limit_r) {
  int i = 0;
  int n = thing.size() - 1;

  for (; i <= n && (--limit_l != -1); i++) {
    if (!skip.contains(thing[i])) {
      break;
    }
  }

  for (; i <= n && (--limit_r != -1); n--) {
    if (!skip.contains(thing[n])) {
      break;
    }
  }

  return thing.mid(i, n - i + 1);
}

}  // namespace __impl__

// ----------

inline
QByteArray lstrip(const QByteArray& thing,
                  const QByteArray& skip = " \r\n\t",
                  int               limit = -1) {
  return __impl__::strip<QByteArray, QByteArray, QByteArray>(thing, skip, limit,     0);
}

inline
QStringRef lstrip(const QStringRef& thing,
                  const QString   & skip = " \r\n\t",
                  int               limit = -1) {
  return __impl__::strip<QStringRef, QStringRef, QString   >(thing, skip, limit,     0);
}

inline
QStringRef lstrip(const QString   & thing,
                  const QString   & skip = " \r\n\t",
                  int               limit = -1) {
  QStringRef qsref(&thing);
  return __impl__::strip<QStringRef, QStringRef, QString   >(qsref, skip, limit,     0);
}

// ----------

inline
QByteArray rstrip(const QByteArray& thing,
                  const QByteArray& skip = " \r\n\t",
                  int               limit = -1) {
  return __impl__::strip<QByteArray, QByteArray, QByteArray>(thing, skip,     0, limit);
}

inline
QStringRef rstrip(const QStringRef& thing,
                  const QString   & skip = " \r\n\t",
                  int               limit = -1) {
  return __impl__::strip<QStringRef, QStringRef, QString   >(thing, skip,     0, limit);
}

inline
QStringRef rstrip(const QString   & thing,
                  const QString   & skip = " \r\n\t",
                  int               limit = -1) {
  QStringRef qsref(&thing);
  return __impl__::strip<QStringRef, QStringRef, QString   >(qsref, skip,     0, limit);
}

// ----------

inline
QByteArray  strip(const QByteArray& thing,
                  const QByteArray& skip = " \r\n\t",
                  int               limit = -1) {
  return __impl__::strip<QByteArray, QByteArray, QByteArray>(thing, skip, limit, limit);
}

inline
QStringRef  strip(const QStringRef& thing,
                  const QString   & skip = " \r\n\t",
                  int               limit = -1) {
  return __impl__::strip<QStringRef, QStringRef, QString   >(thing, skip, limit, limit);
}

inline
QStringRef  strip(const QString   & thing,
                  const QString   & skip = " \r\n\t",
                  int               limit = -1) {
  QStringRef qsref(&thing);
  return __impl__::strip<QStringRef, QStringRef, QString   >(qsref, skip, limit, limit);
}

}  // namespace lvd
