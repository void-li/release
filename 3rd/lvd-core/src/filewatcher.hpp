/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QFileSystemWatcher>
#include <QObject>
#include <QString>
#include <QTimer>

#include "logger.hpp"

// ----------

namespace lvd {

class Filewatcher : public QObject {
  Q_OBJECT LVD_LOGGER

 public:
  Filewatcher(const QString& path,
              QObject*       parent = nullptr);

  ~Filewatcher() override;

 public:
  QString path() const {
    return path_;
  }
  QString base() const {
    return base_;
  }

 public:
  int      interval() const {
    return interval_;
  }
  void set_interval(int interval) {
    interval_ = interval;
    initialize_watimer();
  }

 signals:
  void changed();
  void removed();

 private:
  bool setup_dirs();
  void close_dirs();

  bool watching_dirs() const;

  bool setup_file();
  void close_file();

  bool watching_file() const;

  void initialize_watimer();

 private slots:
  void on_dirs_changed();
  void on_file_changed();

  void on_timeout();

 private:
  QString path_;
  QString base_;

 private:
  QFileSystemWatcher* watcher_;
  QTimer*             watimer_;

  static const
  int Interval_;
  int interval_ = -1;
};

}  // namespace lvd
