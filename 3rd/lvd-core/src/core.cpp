/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "core.hpp"  // IWYU pragma: keep

#include <cstdio>

// ----------

namespace lvd {
namespace __impl__ {

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-variable-declarations"

QTextStream  qStdOut(stdout);
QTextStream* qStdOutPtr = nullptr;

QTextStream  qStdErr(stderr);
QTextStream* qStdErrPtr = nullptr;

#pragma clang diagnostic pop

}  // namespace __impl__

QTextStream& qStdOut() {
  if (__impl__::qStdOutPtr) {
    return *__impl__::qStdOutPtr;
  }

  return __impl__::qStdOut;
}

QTextStream& qStdErr() {
  if (__impl__::qStdErrPtr) {
    return *__impl__::qStdErrPtr;
  }

  return __impl__::qStdErr;
}

}  // namespace lvd
