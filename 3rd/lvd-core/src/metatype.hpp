/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <functional>

#include <QMetaType>
#include <QVector>

// ----------

namespace lvd {

class Metatype {
 private:
  using  Declaration  =         std::function<void(void)> ;
  using  Declarations = QVector<std::function<void(void)>>;

  static Declarations& declarations();

 public:
  template <class T>
  static bool declaration(const char* name) {
    declarations().append([=] {
      ::qRegisterMetaType<T>(name);
    });

    return true;
  }

  static void metatype();
};

// ----------

#ifndef L_DECLARE_METATYPE
#define L_DECLARE_METATYPE(Type, Name) \
  inline const bool Name ## __ ## Decl = lvd::Metatype::declaration<Type>(#Name);
#endif

}  // namespace lvd
