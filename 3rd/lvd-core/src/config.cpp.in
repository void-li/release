/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "config.hpp"
#include "core.hpp"  // IWYU pragma: keep

#define CONFIG(Type, Name, Data)   \
  Type Name() {                    \
    static const Type Name = Data; \
    return Name;                   \
  }

// ----------

namespace lvd::config {

CONFIG(QString, App_Name, "@PROJECT_NAME@")
CONFIG(QString, App_Vers, "@LVD_VERSION@")

CONFIG(QString, Org_Name, "void.li")
CONFIG(QString, Org_Addr, "void.li")

// ----------

CONFIG(QString, SystemPath_Etc, "@CMAKE_INSTALL_FULL_SYSCONFDIR@")

}  // namespace lvd::config
