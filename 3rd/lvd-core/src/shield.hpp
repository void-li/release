/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <exception>
#include <utility>

#include "logger.hpp"

// ----------

namespace lvd {

template <class F, class G, class... Args>
auto shield(F f, G failure, Args&&... args) {
  LVD_LOGFUN

  try {
    return f(std::forward<Args>(args)...);
  }
  catch (const std::exception& ex) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "exception:" << ex.what();

    if constexpr (!std::is_null_pointer_v<G>) {
      return failure(message);
    }
    else {
      using T = decltype(f(std::forward<Args>(args)...));
      return T();
    }
  }
  catch (...) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "exception!";

    if constexpr (!std::is_null_pointer_v<G>) {
      return failure(message);
    }
    else {
      using T = decltype(f(std::forward<Args>(args)...));
      return T();
    }
  }
}

// ----------

#define LVD_SHIELD lvd::shield([&] { nullptr

#define LVD_SHIELD_END    }, nullptr)

#define LVD_SHIELD_FUN(...) },                                      \
  [&] (const auto& message) {                                       \
    auto g = __VA_ARGS__;                                           \
    if constexpr (std::is_member_function_pointer_v<decltype(g)>) { \
      return (this->*g)(message);                                   \
    } else {                                                        \
      return (       g)(message);                                   \
    }})

}  // namespace lvd
