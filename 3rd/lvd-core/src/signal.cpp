/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "signal.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <sys/socket.h>
#include <unistd.h>

#include <QMutex>
#include <QMutexLocker>
#include <QSocketNotifier>

#include "logger.hpp"

// ----------

namespace {

void sigSockWriter(int S[]) {
  char s = 42;
  ::write(S[0], &s, sizeof(s));
}

void sigSockReader(int S[]) {
  char s = 42;
  ::read (S[1], &s, sizeof(s));
}

static int              sigIntxSocket[2] = { ~0, ~0 };
static QSocketNotifier* sigIntxNotify    = nullptr;

inline void sigIntxWriter(int) { sigSockWriter(sigIntxSocket); }
inline void sigIntxReader(int) { sigSockReader(sigIntxSocket); }

static int              sigTermSocket[2] = { ~0, ~0 };
static QSocketNotifier* sigTermNotify    = nullptr;

inline void sigTermWriter(int) { sigSockWriter(sigTermSocket); }
inline void sigTermReader(int) { sigSockReader(sigTermSocket); }

static int              sigUsr1Socket[2] = { ~0, ~0 };
static QSocketNotifier* sigUsr1Notify    = nullptr;

inline void sigUsr1Writer(int) { sigSockWriter(sigUsr1Socket); }
inline void sigUsr1Reader(int) { sigSockReader(sigUsr1Socket); }

static int              sigUsr2Socket[2] = { ~0, ~0 };
static QSocketNotifier* sigUsr2Notify    = nullptr;

inline void sigUsr2Writer(int) { sigSockWriter(sigUsr2Socket); }
inline void sigUsr2Reader(int) { sigSockReader(sigUsr2Socket); }

inline QMutex& qmutex() {
  static QMutex  qmutex;
  return qmutex;
}

}  // namespace

// ----------

namespace lvd {

Signal::Signal(int signal, QObject* parent)
    : QObject(parent) {
  QMutexLocker qmutexlocker(&qmutex());

  int*              socket = nullptr;
  QSocketNotifier** notify = nullptr;

  void (*writer)(int) = nullptr;
  void (*reader)(int) = nullptr;

  switch (signal) {
    case SIGINT :
      socket = &sigIntxSocket[0];
      notify = &sigIntxNotify;

      writer = &sigIntxWriter;
      reader = &sigIntxReader;
      break;

    case SIGTERM:
      socket = &sigTermSocket[0];
      notify = &sigTermNotify;

      writer = &sigTermWriter;
      reader = &sigTermReader;
      break;

    case SIGUSR1:
      socket = &sigUsr1Socket[0];
      notify = &sigUsr1Notify;

      writer = &sigUsr1Writer;
      reader = &sigUsr1Reader;
      break;

    case SIGUSR2:
      socket = &sigUsr2Socket[0];
      notify = &sigUsr2Notify;

      writer = &sigUsr2Writer;
      reader = &sigUsr2Reader;
      break;

    default:
      LVD_LOG_F("unknown signal '%i'", signal);
  }

  if ( socket[0] == ~0) {
    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, socket)) {
      LVD_LOG_F("socketpair failed");
    }

    struct sigaction siga;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdisabled-macro-expansion"
    siga.sa_handler = writer;
#pragma clang diagnostic pop

    siga.sa_flags = SA_RESTART;
    sigemptyset(&siga.sa_mask);

    if (sigaction(signal, &siga, nullptr)) {
      LVD_LOG_F("sigactions failed");
    }
  }

  if (*notify == nullptr) {
    *notify = new QSocketNotifier(socket[1], QSocketNotifier::Read);
    connect(*notify, &QSocketNotifier::activated, [reader] { (reader)(42); });
  }

  connect(*notify, &QSocketNotifier::activated, this, &Signal::activated);
}

}  // namespace lvd
