/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "application.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <QDebug>

#include "shield.hpp"

// ----------

namespace lvd {

Application::Application(int& argc, char** argv)
    : LVD_APPLICATION_BASE(argc, argv) {
  sigintx_ = new Signal(SIGINT , this);
  connect(sigintx_, &Signal::activated,
          this, &Application::quit,
          Qt::QueuedConnection);

  sigterm_ = new Signal(SIGTERM, this);
  connect(sigterm_, &Signal::activated,
          this, &Application::quit,
          Qt::QueuedConnection);
}

Application::~Application() = default;

// ----------

bool Application::notify(QObject* receiver, QEvent* event) {
  return
  LVD_SHIELD;

  return LVD_APPLICATION_BASE::notify(receiver, event);

  LVD_SHIELD_FUN([&] (const QString& message) {
    emit failure(message);
    return true;
  });
}

// ----------

void Application::print() {
  LVD_LOG_I() >> &QDebug::noquote
              << Application::applicationName   ()
              << Application::applicationVersion();
}



}  // namespace lvd
