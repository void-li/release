/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <functional>
#include <optional>
#include <utility>

#include <QtGlobal>

#include <QByteArray>
#include <QCommandLineParser>
#include <QDebug>
#include <QLoggingCategory>
#include <QString>
#include <QStringList>

// ----------

namespace lvd {

class Logger {
 public:
  enum class Level {
    C = QtMsgType::QtCriticalMsg,
    W = QtMsgType::QtWarningMsg,
    I = QtMsgType::QtInfoMsg,
    D = QtMsgType::QtDebugMsg,
    T = 123
  };

  static bool LevelIsBuiltIn(Level level);

  using Category = std::function<QLoggingCategory&(Level)>;

 public:
  Logger(Category    category,
         Level       level,
         const char* file,
         int         line,
         const char* func,
         QString*    buffer = nullptr);

  ~Logger();

  template <class R>
  Logger& operator>>(R (QDebug::*f)()) {
    if (qdebug_.has_value()) {
      (*qdebug_.*f)();
    }

    return *this;
  }

  template <class T>
  Logger& operator<<(T&& t) {
    if (qdebug_.has_value()) {
      if constexpr(std::is_invocable_v<T>) {
        *qdebug_ <<                 t();
      } else {
        *qdebug_ << std::forward<T>(t);
      }
    }

    return *this;
  }

  static QByteArray logger_category(const char* pretty);
  static QByteArray logfun_category(const char* pretty);

 public:
  static void install();
  static void install_logger();

  static void restore();
  static void restore_logger();

  static void refresh();

  static void setup_arguments(QCommandLineParser& qcommandlineparser);
  static void parse_arguments(QCommandLineParser& qcommandlineparser);

  static void add_logconf(const QString& logconf);
  static void clear_logconfs();

  static void add_logrule(const QString& logrule);
  static void clear_logrules();

 private:
  std::optional<QDebug> qdebug_;
  QString*              buffer_ = nullptr;

 private:
  static QtMessageHandler qtmessagehandler_;

  static QStringList logconfs_;
  static QStringList logrules_;
};

// ----------

#define LVD_LOGGER \
        LVD_LOGGER_IMPL(lvd::Logger::logger_category(__PRETTY_FUNCTION__))

#define LVD_LOGGER_IMPL(C) \
static                     \
QLoggingCategory& __lvd_log__(lvd::Logger::Level loglevel) {    \
  static const QByteArray lvd_log_cat = C;                      \
  if (loglevel == lvd::Logger::Level::T) {                      \
    static const QByteArray  category = lvd_log_cat + ".trace"; \
    static QLoggingCategory  qloggingcategory(category);        \
    return qloggingcategory;                                    \
  } else {                                                      \
    static const QByteArray& category = lvd_log_cat;            \
    static QLoggingCategory  qloggingcategory(category);        \
    return qloggingcategory;                                    \
  }                                                             \
}

#define LVD_LOGGER_LIKE(C) \
static                     \
QLoggingCategory& __lvd_log__(lvd::Logger::Level loglevel) {    \
  return C::__lvd_log__(loglevel);                              \
}

#define LVD_LOGFUN \
        LVD_LOGFUN_IMPL(lvd::Logger::logfun_category(__PRETTY_FUNCTION__))

#define LVD_LOGFUN_IMPL(C) \
static                     \
auto __lvd_log__ = [] (lvd::Logger::Level loglevel) -> QLoggingCategory& { \
  static const QByteArray lvd_log_cat = C;                      \
  if (loglevel == lvd::Logger::Level::T) {                      \
    static const QByteArray  category = lvd_log_cat + ".trace"; \
    static QLoggingCategory  qloggingcategory(category);        \
    return qloggingcategory;                                    \
  } else {                                                      \
    static const QByteArray& category = lvd_log_cat;            \
    static QLoggingCategory  qloggingcategory(category);        \
    return qloggingcategory;                                    \
  }                                                             \
};

#define LVD_LOGFUN_LIKE(C) \
static                     \
auto __lvd_log__ = [] (lvd::Logger::Level level) -> QLoggingCategory& { \
  return C::__lvd_log__(level);                                 \
};

// ----------

#define LVD_LOG_F(...) qFatal(__VA_ARGS__)

#define LVD_LOG_C(...) lvd::Logger(__lvd_log__, lvd::Logger::Level::C, \
  QT_MESSAGELOG_FILE, QT_MESSAGELOG_LINE, QT_MESSAGELOG_FUNC, ##__VA_ARGS__)

#define LVD_LOG_W(...) lvd::Logger(__lvd_log__, lvd::Logger::Level::W, \
  QT_MESSAGELOG_FILE, QT_MESSAGELOG_LINE, QT_MESSAGELOG_FUNC, ##__VA_ARGS__)

#define LVD_LOG_I(...) lvd::Logger(__lvd_log__, lvd::Logger::Level::I, \
  QT_MESSAGELOG_FILE, QT_MESSAGELOG_LINE, QT_MESSAGELOG_FUNC, ##__VA_ARGS__)

#define LVD_LOG_D(...) lvd::Logger(__lvd_log__, lvd::Logger::Level::D, \
  QT_MESSAGELOG_FILE, QT_MESSAGELOG_LINE, QT_MESSAGELOG_FUNC, ##__VA_ARGS__)

#define LVD_LOG_T(...) lvd::Logger(__lvd_log__, lvd::Logger::Level::T, \
  QT_MESSAGELOG_FILE, QT_MESSAGELOG_LINE, QT_MESSAGELOG_FUNC, ##__VA_ARGS__) << __FUNCTION__

#define LVD_LOGBUF QString

#define LVD_LOGHULL(E) [&] { return E; }
#define LVD_LOGHUSK(E) [&] {        E; }

#define LVD_LOGLINE(E) E.leftRef(E.indexOf('\n')) << "..."

}  // namespace lvd
