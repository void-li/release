/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <QObject>
#include <QString>

#include "access.hpp"

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class Object {
 private:
  int normal_member                      = 42;

  static
  int static_member;

  int normal_method(      )       { return 42; }
  int normal_method(int dv)       { return dv; }

  static
  int static_method(      )       { return 42; }
  static
  int static_method(int dv)       { return dv; }

  int normal_stable(      ) const { return 42; }
  int normal_stable(int dv) const { return dv; }
};

int Object::static_member = 42;

LVD_ACCESS_NORMAL_MEMBER(Object_NormalMember , Object, normal_member, int);
LVD_ACCESS_STATIC_MEMBER(Object_StaticMember , Object, static_member, int);

LVD_ACCESS_NORMAL_METHOD(Object_NormalMethod , Object, normal_method, int);
LVD_ACCESS_NORMAL_METHOD(Object_NormalMethod2, Object, normal_method, int, int);

LVD_ACCESS_STATIC_METHOD(Object_StaticMethod , Object, static_method, int);
LVD_ACCESS_STATIC_METHOD(Object_StaticMethod2, Object, static_method, int, int);

LVD_ACCESS_NORMAL_STABLE(Object_NormalStable , Object, normal_stable, int);
LVD_ACCESS_NORMAL_STABLE(Object_NormalStable2, Object, normal_stable, int, int);

// ----------

class AccessTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void normal_member() {
    Object object;
    QCOMPARE(access_normal_member<Object_NormalMember >(object)    , 42);
  }

  void normal_member_mutable() {
    Object object;
    QCOMPARE(access_normal_member<Object_NormalMember >(object)    , 42);

    access_normal_member<Object_NormalMember>(object) = 23;
    QCOMPARE(access_normal_member<Object_NormalMember >(object)    , 23);
  }

  void static_member() {
    QCOMPARE(access_static_member<Object_StaticMember >(      )    , 42);
  }

  void static_member_mutable() {
    QCOMPARE(access_static_member<Object_StaticMember >(      )    , 42);

    access_static_member<Object_StaticMember >(      ) = 23;
    QCOMPARE(access_static_member<Object_StaticMember >(      )    , 23);
  }

  // ----------

  void normal_method() {
    Object object;
    QCOMPARE(access_normal_method<Object_NormalMethod >(object)    , 42);
  }

  void normal_method2() {
    Object object;
    QCOMPARE(access_normal_method<Object_NormalMethod2>(object, 42), 42);
  }

  void static_method() {
    QCOMPARE(access_static_method<Object_StaticMethod >(      )    , 42);
  }

  void static_method2() {
    QCOMPARE(access_static_method<Object_StaticMethod2>(        42), 42);
  }

  // ----------

  void normal_stable() {
    Object object;
    QCOMPARE(access_normal_method<Object_NormalStable >(object)    , 42);
  }

  void normal_stable2() {
    Object object;
    QCOMPARE(access_normal_method<Object_NormalStable2>(object, 42), 42);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(AccessTest)
#include "access_test.moc"  // IWYU pragma: keep
