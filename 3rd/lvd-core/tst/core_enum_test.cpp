/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <array>

#include <QObject>
#include <QString>

#include "core_enum.hpp"

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

LVD_ENUM(MeraEnum, Mera       , Luna       );
LVD_ENUM(LunaEnum, Mera = 0x23, Luna = 0x42);

// ----------

class CoreEnumTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void simplex() {
    QCOMPARE(MeraEnum::Mera, 0x00);
    QCOMPARE(MeraEnum::Luna, 0x01);
  }

  void complex() {
    QCOMPARE(LunaEnum::Mera, 0x23);
    QCOMPARE(LunaEnum::Luna, 0x42);
  }

  void valid() {
    MeraEnum mera = MeraEnum::Mera;
    QVERIFY(mera.valid());

    MeraEnum luna = MeraEnum::Luna;
    QVERIFY(luna.valid());

    MeraEnum null = MeraEnum();
    QVERIFY(null.valid() != true);
  }

  // ----------

  void to_string() {
    QCOMPARE(MeraEnum(MeraEnum::Mera).to_string(), "Mera");
    QCOMPARE(MeraEnum(MeraEnum::Luna).to_string(), "Luna");

    QVERIFY_EXCEPTION_THROWN(MeraEnum(1337).to_string(),
                             std::runtime_error);
  }

  void from_string() {
    QCOMPARE(MeraEnum::From_String("Mera"), MeraEnum::Mera);
    QCOMPARE(MeraEnum::From_String("Luna"), MeraEnum::Luna);

    QVERIFY_EXCEPTION_THROWN(MeraEnum::From_String("1337"),
                             std::runtime_error);
  }

  // ----------

  void keys() {
    int i = 0;

    for (auto it  = MeraEnum::__keys_begin__();
              it != MeraEnum::__keys_end__  ();
              it++) {
      LVD_FINALLY { i++; };

      switch (i) {
        case MeraEnum::Mera:
          QCOMPARE((*it).data(), "Mera");
          break;

        case MeraEnum::Luna:
          QCOMPARE((*it).data(), "Luna");
          break;
      }
    }

    QCOMPARE(i, MeraEnum::__size__());
  }

  void vals() {
    int i = 0;

    for (auto it  = MeraEnum::__vals_begin__();
              it != MeraEnum::__vals_end__  ();
              it++) {
      LVD_FINALLY { i++; };

      switch (i) {
        case MeraEnum::Mera:
          QCOMPARE((*it), MeraEnum::Mera);
          break;

        case MeraEnum::Luna:
          QCOMPARE((*it), MeraEnum::Luna);
          break;
      }
    }

    QCOMPARE(i, MeraEnum::__size__());
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(CoreEnumTest)
#include "core_enum_test.moc"  // IWYU pragma: keep
