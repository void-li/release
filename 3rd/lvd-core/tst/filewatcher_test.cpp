/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <QDir>
#include <QFile>
#include <QIODevice>
#include <QMetaObject>
#include <QObject>
#include <QString>

#include "filewatcher.hpp"

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class FilewatcherTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void create_file() {
    Filewatcher filewatcher("meraluna.txt");
    filewatcher.set_interval(128);

    QSignalSpy spy(&filewatcher, &Filewatcher::changed);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(&filewatcher, [&] {
      QFile qfile("meraluna.txt");
      qfile.open(QIODevice::WriteOnly);
      qfile.write("Mera Luna!\n");
      qfile.close();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void change_file() {
    create_file();

    Filewatcher filewatcher("meraluna.txt");
    filewatcher.set_interval(128);

    QSignalSpy spy(&filewatcher, &Filewatcher::changed);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(&filewatcher, [&] {
      QFile qfile("meraluna.txt");
      qfile.open(QIODevice::WriteOnly);
      qfile.write("Mera Luna!\n");
      qfile.close();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void remove_file() {
    create_file();

    Filewatcher filewatcher("meraluna.txt");
    filewatcher.set_interval(128);

    QSignalSpy spy(&filewatcher, &Filewatcher::removed);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(&filewatcher, [&] {
      QFile::remove("meraluna.txt");
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  // ----------

  void inside_dirs() {
    QFETCH(bool, immediate);

    Filewatcher filewatcher("meraluna_tmp/meraluna.txt");
    filewatcher.set_interval(128);

    QSignalSpy spy(&filewatcher, &Filewatcher::changed);
    QVERIFY(spy.isValid());

    if (!immediate) {
      QVERIFY(!spy.wait(256));
      QCOMPARE(spy.size(), 0);
    }

    QMetaObject::invokeMethod(&filewatcher, [&] {
      QDir().mkdir("meraluna_tmp");
    });

    if (!immediate) {
      QVERIFY(!spy.wait(256));
      QCOMPARE(spy.size(), 0);
    }

    QMetaObject::invokeMethod(&filewatcher, [&] {
      QFile qfile("meraluna_tmp/meraluna.txt");
      qfile.open(QIODevice::WriteOnly);
      qfile.write("Mera Luna!\n");
      qfile.close();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(512));
    QCOMPARE(spy.size(), 1);
  }

  void inside_dirs_data() {
    QTest::addColumn<bool>("immediate");

    QTest::addRow("immediate") << true ;
    QTest::addRow("suspended") << false;
  }

  void inside_dirs_after_removal() {
    QFETCH(bool, immediate);

    Filewatcher filewatcher("meraluna_tmp/meraluna.txt");
    filewatcher.set_interval(128);

    inside_dirs();
    QDir("meraluna_tmp").removeRecursively();

    QSignalSpy spy(&filewatcher, &Filewatcher::changed);
    QVERIFY(spy.isValid());

    if (!immediate) {
      QVERIFY(!spy.wait(256));
      QCOMPARE(spy.size(), 0);
    }

    QMetaObject::invokeMethod(&filewatcher, [&] {
      QDir().mkdir("meraluna_tmp");
    });

    if (!immediate) {
      QVERIFY(!spy.wait(256));
      QCOMPARE(spy.size(), 0);
    }

    QMetaObject::invokeMethod(&filewatcher, [&] {
      QFile qfile("meraluna_tmp/meraluna.txt");
      qfile.open(QIODevice::WriteOnly);
      qfile.write("Mera Luna!\n");
      qfile.close();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(512));
    QCOMPARE(spy.size(),  1 + (immediate ? 1 : 0));
  }

  void inside_dirs_after_removal_data() {
    QTest::addColumn<bool>("immediate");

    QTest::addRow("immediate") << true ;
    QTest::addRow("suspended") << false;
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(FilewatcherTest)
#include "filewatcher_test.moc"  // IWYU pragma: keep
