/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <string>

#include <QObject>
#include <QString>

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class CoreTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void finally() {
    QString test = "Mera";

    {
      QCOMPARE(test, "Mera");

      LVD_FINALLY {
        test = "Luna";
      };

      QCOMPARE(test, "Mera");
    }

    QCOMPARE(test, "Luna");
  }

  // ----------

  void throwing() {
    QVERIFY_EXCEPTION_THROWN(LVD_THROW_LOGIC("Mera Luna"),
                             std::logic_error);

    QVERIFY_EXCEPTION_THROWN(LVD_THROW_ORANGE("Mera Luna"),
                             std::out_of_range);

    QVERIFY_EXCEPTION_THROWN(LVD_THROW_RUNTIME("Mera Luna"),
                             std::runtime_error);

    QVERIFY_EXCEPTION_THROWN(LVD_THROW_IMPOSSIBLE,
                             std::runtime_error);
  }

  void throwing2() {
    const std::string test_a = "Mera Luna";

    QVERIFY_EXCEPTION_THROWN(LVD_THROW_RUNTIME(test_a),
                             std::runtime_error);

    const QString     test_b = "Mera Luna";

    QVERIFY_EXCEPTION_THROWN(LVD_THROW_RUNTIME(test_b),
                             std::runtime_error);
  }

  // ----------

  void qstdout() {
    QTextStream* qStdOutPtr = lvd::__impl__::qStdOutPtr;
    LVD_FINALLY { lvd::__impl__::qStdOutPtr = qStdOutPtr; };

    lvd::__impl__::qStdOutPtr = nullptr;

    QTextStream qtextstream;

    QTextStream& s0 = qStdOut();
    QVERIFY(&s0 != &qtextstream);

    lvd::__impl__::qStdOutPtr = &qtextstream;
    LVD_FINALLY { lvd::__impl__::qStdOutPtr = nullptr; };

    QTextStream& s1 = qStdOut();
    QVERIFY(&s1 == &qtextstream);
  }

  void qstderr() {
    QTextStream* qStdErrPtr = lvd::__impl__::qStdErrPtr;
    LVD_FINALLY { lvd::__impl__::qStdErrPtr = qStdErrPtr; };

    lvd::__impl__::qStdErrPtr = nullptr;

    QTextStream qtextstream;

    QTextStream& s0 = qStdErr();
    QVERIFY(&s0 != &qtextstream);

    lvd::__impl__::qStdErrPtr = &qtextstream;
    LVD_FINALLY { lvd::__impl__::qStdErrPtr = nullptr; };

    QTextStream& s1 = qStdErr();
    QVERIFY(&s1 == &qtextstream);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(CoreTest)
#include "core_test.moc"  // IWYU pragma: keep
