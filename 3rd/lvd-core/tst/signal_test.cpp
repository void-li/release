/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <QMetaObject>
#include <QObject>
#include <QString>

#include "signal.hpp"

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class SignalTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void signal() {
    QFETCH(int, sigval);
    Signal signal(sigval);

    QSignalSpy spy(&signal, &Signal::activated);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(&signal, [sigval] {
      raise(sigval);
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void signal_data() {
    QTest::addColumn<int>("sigval");

    QTest::addRow("SIGINT" ) << SIGINT ;
    QTest::addRow("SIGTERM") << SIGTERM;

    QTest::addRow("SIGUSR1") << SIGUSR1;
    QTest::addRow("SIGUSR2") << SIGUSR2;
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(SignalTest)
#include "signal_test.moc"  // IWYU pragma: keep
