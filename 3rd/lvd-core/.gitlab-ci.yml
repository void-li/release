image: registry.gitlab.com/void-li/alpina/alpina-make:latest

stages:
  - build
  - tests
  - check
  - pages
  - deploy
  - release

variables:
  GIT_DEPTH: 7

# ----------

build:clang:
  stage: build

  before_script:
    - mkdir build

  script:
    - cd build
    - cmake -GNinja -DCMAKE_BUILD_TYPE=Coverage -DCMAKE_CXX_COMPILER=clang++ ..
    - ninja

  artifacts:
    paths:
      - build
    expire_in: 1w

build:gnucc:
  stage: build

  before_script:
    - mkdir build

  script:
    - cd build
    - cmake -GNinja -DCMAKE_BUILD_TYPE=Debug    -DCMAKE_CXX_COMPILER=g++     ..
    - ninja

  artifacts:
    paths:
      - build
    expire_in: 1w

  only:
    - master
    - /^v(\d+)\.(\d+)\.x$/
    - /^v(\d+)\.(\d+)\.(\d+)$/
    - merge_requests

# ----------

tests:clang:
  stage: tests

  dependencies:
    - build:clang

  script:
    - cd build
    - ninja test_coverage
    - echo test_coverage `xmllint --html --xpath "//td[text()='Lines:']/following-sibling::td[3]/text()" tst/test_coverage/index.html`

  after_script:
    - qtx2jux.sh .

  artifacts:
    paths:
      - build
    expire_in: 1w

    reports:
      junit:
        - build/*_test.ju.xml
        - build/*/*_test.ju.xml
        - build/*/*/*_test.ju.xml

    when: always

  coverage: '/test_coverage \d+\.\d+/'

# ----------

check:clang:
  stage: check
  image: registry.gitlab.com/void-li/reports:latest

  dependencies:
    - build:clang

  before_script:
    - rm -rf build

  script:
    - reports

  artifacts:
    paths:
      - report.json

    reports:
      codequality:
        - report.json

  only:
    - master
    - /^v(\d+)\.(\d+)\.x$/
    - /^v(\d+)\.(\d+)\.(\d+)$/
    - merge_requests

# ----------

pages:
  stage: pages
  image: registry.gitlab.com/void-li/alpina/allure:latest

  dependencies:
    - tests:clang

  before_script:
    - mkdir "${CI_PROJECT_DIR}/public"

  script:
    - cd build
    - mv tst/test_coverage  "$CI_PROJECT_DIR/public/cov" || echo
    - mkdir allure
    - find . -path ./allure -prune -o -name '*.ju.xml' -exec cp '{}' allure \;
    - allure generate -c -o "$CI_PROJECT_DIR/public/tst" allure
    - cp /index.html        "$CI_PROJECT_DIR/public/"

  artifacts:
    paths:
      - public

  only:
    - master

# ----------

.release:
  stage: release
  image: registry.gitlab.com/void-li/release:latest

  variables:
    GIT_DEPTH: 0

release:ini:
  extends: .release

  before_script:
    - gpg -v --import <(echo "$RELEASE_BOT_GNUPG")
    - gpg -v --import <(echo "$RELEASE_USR_GNUPG")
    - git checkout -B      ${CI_COMMIT_REF_NAME}
    - git branch -u origin/${CI_COMMIT_REF_NAME}
    - git remote set-url --push origin ${CI_PROJECT_URL/:\/\//:\/\/${GITLAB_USER_LOGIN}:${RELEASE_API_TOKEN}@}.git

  script:
    - release

  only:
    refs:
      - master
      - /^v(\d+)\.(\d+)\.x$/

    variables:
      - $CI_COMMIT_TITLE =~ /^\[(major-|minor-|patch-|)release\]$/

release:tag:
  extends: .release

  before_script:
    - gpg -v --import <(echo "$RELEASE_BOT_GNUPG")
    - gpg -v --import <(echo "$RELEASE_USR_GNUPG")

  script:
    - release .li.void.release.tag.conf

  only:
    - /^v(\d+)\.(\d+)\.(\d+)$/
