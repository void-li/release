/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QMetaObject>
#include <QObject>
#include <QSettings>
#include <QString>

#include "release.hpp"
#include "release_test_handlers.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;

#include "handlers/test_handler.hpp"

// ----------

class ReleaseTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Release release(config_);
  }

  // ----------

  void handlers(bool extended = false) {
    QFETCH(QString, handles);
    QFETCH(bool   , success);
    QFETCH(bool   , delayed);
    QFETCH(bool   , excepts);

    QSettings qsettings(config_, QSettings::IniFormat);
    qsettings.sync();

    QString handler = "test_%1_%2_handler";

    if (success) {
      handler = handler.arg("success");
    } else {
      handler = handler.arg("failure");
    }

    if (delayed) {
      handler = handler.arg("async");
    } else {
      handler = handler.arg( "sync");
    }

    if (excepts) {
      handler = "test_exception_handler";
    }

    {
      qsettings.beginGroup(handler);
      qsettings.setValue("handles", handles);
      qsettings.endGroup();
    }

    if (extended) {
      qsettings.beginGroup(handler + ":mera");
      qsettings.setValue("handles", handles);
      qsettings.endGroup();

      qsettings.beginGroup(handler + ":luna");
      qsettings.setValue("handles", handles);
      qsettings.endGroup();
    }

    qsettings.sync();

    ReleaseTestHandler::clear_iterations();
    QCOMPARE(ReleaseTestHandler::iterations(), 0);

    Release release(config_);
    QSignalSpy* spy;

    if (success) {
      spy = new QSignalSpy(&release, &Release::success);
      QVERIFY(spy->isValid());
    } else {
      spy = new QSignalSpy(&release, &Release::failure);
      QVERIFY(spy->isValid());
    }

    LVD_FINALLY {
      spy->deleteLater();
      spy = nullptr;
    };

    QMetaObject::invokeMethod(this, [&] {
      release.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy->wait(2048));
    QCOMPARE(spy->size(), 1);

    if (success && extended) {
      if (!excepts) {
        QCOMPARE(ReleaseTestHandler::iterations(), 3);
      }
    } else {
      if (!excepts) {
        QCOMPARE(ReleaseTestHandler::iterations(), 1);
      }
    }
  }

  void handlers_data() {
    QTest::addColumn<QString>("handles");
    QTest::addColumn<bool>   ("success");
    QTest::addColumn<bool>   ("delayed");
    QTest::addColumn<bool>   ("excepts");

    QTest::addRow("before success  sync") << "Before" << true  << false << false;
    QTest::addRow("before success async") << "Before" << true  << true  << false;
    QTest::addRow("before failure  sync") << "Before" << false << false << false;
    QTest::addRow("before failure async") << "Before" << false << true  << false;
    QTest::addRow("before exception"    ) << "Before" << false << false << true ;

    QTest::addRow("commit success  sync") << "Commit" << true  << false << false;
    QTest::addRow("commit success async") << "Commit" << true  << true  << false;
    QTest::addRow("commit failure  sync") << "Commit" << false << false << false;
    QTest::addRow("commit failure async") << "Commit" << false << true  << false;
    QTest::addRow("commit exception"    ) << "Commit" << false << false << true ;

    QTest::addRow("beyond success  sync") << "Beyond" << true  << false << false;
    QTest::addRow("beyond success async") << "Beyond" << true  << true  << false;
    QTest::addRow("beyond failure  sync") << "Beyond" << false << false << false;
    QTest::addRow("beyond failure async") << "Beyond" << false << true  << false;
    QTest::addRow("beyond exception"    ) << "Beyond" << false << false << true ;
  }

  void handlers_extended() {
    handlers(true);
  }

  void handlers_extended_data() {
    handlers_data();
  }

  void commits_since_dawn(bool dawn = true) {
    QFETCH(int, n);

    if (!dawn) {
      int ret;

      ret = execute("git", "tag", "-a", "v0.0.0", "-m", "Version 0.0.0");
      QCOMPARE(ret, 0);
    }

    for (int i = 0; i < n; i++) {
      int ret;

      if (!alter_file("data0"))
        return;

      ret = execute("git", "commit", "-m", "Another Commit", "-a");
      QCOMPARE(ret, 0);
    }

    QSettings qsettings(config_, QSettings::IniFormat);
    qsettings.sync();

    if (!dawn) {
      qsettings.beginGroup("prev_version_extractor");
      qsettings.setValue("handles", "Before");
      qsettings.endGroup();
    }

    {
      qsettings.beginGroup("test_success_sync_handler");
      qsettings.setValue("handles", "Commit");
      qsettings.endGroup();
    }

    qsettings.sync();

    ReleaseTestHandler::clear_iterations();
    QCOMPARE(ReleaseTestHandler::iterations(), 0);

    Release release(config_);

    QSignalSpy spy(&release, &Release::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      release.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    if (!dawn) {
      QCOMPARE(ReleaseTestHandler::iterations(), n + 0);
    } else {
      QCOMPARE(ReleaseTestHandler::iterations(), n + 1);
    }
  }

  void commits_since_dawn_data() {
    QTest::addColumn<int>("n");

    QTest::addRow( "0") <<  0;
    QTest::addRow( "1") <<  1;
    QTest::addRow( "2") <<  2;

    QTest::addRow("14") << 14;
    QTest::addRow("23") << 23;
    QTest::addRow("42") << 42;
  }

  void commits_since_last() {
    commits_since_dawn(false);
  }

  void commits_since_last_data() {
    QTest::addColumn<int>("n");

    QTest::addRow( "0") <<  0;
    QTest::addRow( "1") <<  1;
    QTest::addRow( "2") <<  2;

    QTest::addRow("14") << 14;
    QTest::addRow("23") << 23;
    QTest::addRow("42") << 42;
  }

  // ----------

  void git_repo_removed() {
    TestHandler::remove_git_repo();

    QSettings qsettings(config_, QSettings::IniFormat);
    qsettings.sync();

    { qsettings.beginGroup("test_timeout_handler");
      qsettings.setValue("handles", "Commit");
      qsettings.endGroup(); }

    qsettings.sync();

    Release release(config_);

    QSignalSpy spy(&release, &Release::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      release.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_removed() {
    TestHandler::git_util_removed();

    QSettings qsettings(config_, QSettings::IniFormat);
    qsettings.sync();

    { qsettings.beginGroup("test_timeout_handler");
      qsettings.setValue("handles", "Commit");
      qsettings.endGroup(); }

    qsettings.sync();

    Release release(config_);

    QSignalSpy spy(&release, &Release::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      release.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_timeout() {
    TestHandler::git_util_timeout();

    QSettings qsettings(config_, QSettings::IniFormat);
    qsettings.sync();

    QFETCH(QString, timeout);
    qsettings.setValue(timeout, 1);

    { qsettings.beginGroup("test_timeout_handler");
      qsettings.setValue("handles", "Commit");
      qsettings.endGroup(); }

    qsettings.sync();

    Release release(config_);

    QSignalSpy spy(&release, &Release::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      release.execute();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_timeout_data() {
    QTest::addColumn<QString>("timeout");

    QTest::addRow("hard_timeout") << "hard_timeout";
    QTest::addRow("soft_timeout") << "soft_timeout";
  }

  void git_util_lagging() {
    TestHandler::git_util_lagging();
    handlers();
  }

  void git_util_lagging_data() {
    QTest::addColumn<QString>("handles");
    QTest::addColumn<bool>   ("success");
    QTest::addColumn<bool>   ("delayed");
    QTest::addColumn<bool>   ("excepts");

    QTest::addRow("commit success async") << "Commit" << true  << true << false;
    QTest::addRow("commit failure async") << "Commit" << false << true << false;
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    create_git_repo();
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }

 private:
  QString config_ = "li.void.release.conf";
};

LVD_TEST_MAIN(ReleaseTest)
#include "release_test.moc"  // IWYU pragma: keep
