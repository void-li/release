/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "gitlab.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QtTest>

#include <QByteArray>
#include <QDateTime>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonValueRef>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QThread>
#include <QUrl>
#include <QVariant>

// ----------

namespace {
// clazy:excludeall=non-pod-global-static

const qint64  GitlabTimeLimit        = 10;
const qint64  GitlabTimePause        = 1;

const QString GitlabGatherProjectUrl = "%1/projects";
const QString GitlabCreateProjectUrl = "%1/projects";

const QString GitlabGatherGit_TagUrl = "%1/projects/%2/repository/tags/v7.8.9";
const QString GitlabCreateGit_TagUrl = "%1/projects/%2/repository/tags";

const QString GitlabGatherReleaseUrl = "%1/projects/%2/releases/v7.8.9";
const QString GitlabRemoveReleaseUrl = "%1/projects/%2/releases/v7.8.9";

}  // namespace

#ifndef MESSAGE
#define MESSAGE                                  \
  QString("Code: %1, Text: %2")                  \
      .arg(QString::number(code), QString(text)) \
      .toLocal8Bit().data()
#endif

namespace Gitlab {

bool await         (QNetworkAccessManager* manager,
                    const QString&         gitlab_api,
                    const QString&         gitlab_tok) {
  bool success = false;

  QDateTime beg = QDateTime::currentDateTime();
  QDateTime now;

  while (!success) {
    bool  trymore = false;

    [&] {
      QNetworkRequest qnetworkrequest;
      qnetworkrequest.setRawHeader("PRIVATE-TOKEN", gitlab_tok.toUtf8());
      qnetworkrequest.setRawHeader("Content-Type" , "application/json");

      QUrl qurl(GitlabGatherProjectUrl.arg(gitlab_api));
      qnetworkrequest.setUrl(qurl);

      QNetworkReply* reply = manager->get (qnetworkrequest);
      LVD_FINALLY { reply->deleteLater(); };

      QSignalSpy spy(reply, &QNetworkReply::finished);
      QVERIFY(spy.isValid());

      QVERIFY(spy.wait(16384));
      QCOMPARE(spy.size(), 1);

      if (   reply->error() == QNetworkReply::ConnectionRefusedError
          || reply->error() == QNetworkReply:: RemoteHostClosedError
          || reply->error() == QNetworkReply::          TimeoutError
          || reply->error() == QNetworkReply::    UnknownServerError) {
        trymore = true;
        return;
      }

      if (   reply->error() != QNetworkReply::               NoError) {
        trymore = false;
        return;
      }

      int      code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
      QVariant type = reply->header   (QNetworkRequest::ContentTypeHeader      );

      if (code != 200 || type != "application/json") {
        trymore = true;
        return;
      }

      success = true;
    }();

    if (!success) {
      if (!trymore) {
        break;
      }
    }

    if (!success) {
      now = QDateTime::currentDateTime();

      qint64 sec = beg.secsTo(now);
      if (sec > GitlabTimeLimit) {
        break;
      }
    }

    QThread::sleep(GitlabTimePause);
  }

  return success;
}

bool gather_project(QNetworkAccessManager* manager,
                    const QString&         gitlab_api,
                    const QString&         gitlab_tok,
                          QString*         gitlab_pid,
                          bool*            project_exists) {
  bool success = false;

  if (project_exists) {
    *project_exists = false;
  }

  [&] {
    QNetworkRequest qnetworkrequest;
    qnetworkrequest.setRawHeader("PRIVATE-TOKEN", gitlab_tok.toUtf8());
    qnetworkrequest.setRawHeader("Content-Type" , "application/json");

    QUrl qurl(GitlabGatherProjectUrl.arg(gitlab_api));
    qnetworkrequest.setUrl(qurl);

    QNetworkReply* reply = manager->get (qnetworkrequest);
    LVD_FINALLY { reply->deleteLater(); };

    QSignalSpy spy(reply, &QNetworkReply::finished);
    QVERIFY(spy.isValid());

    QVERIFY(spy.wait(16384));
    QCOMPARE(spy.size(), 1);

    int        code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QByteArray text = reply->readAll();

    QVERIFY2(code == 200,
             MESSAGE);

    auto qjd = QJsonDocument::fromJson(text);
    QVERIFY(qjd.isArray());

    QString name;
    QString aurl;

    for (auto qjv0 : qjd.array()) {  // clazy:exclude=range-loop
      QVERIFY(qjv0.isObject());
      auto qjo0 = qjv0.toObject();

      auto qjv_name = qjo0.value("name");
      QVERIFY(qjv_name.isString());
      name = qjv_name.toString();

      if (name.isEmpty() || name != "mera-luna") {
        continue;
      }

      auto qjv1 = qjo0.value("_links");
      QVERIFY(qjv1.isObject());
      auto qjo1 = qjv1.toObject();

      auto qjv_aurl = qjo1.value("self");
      QVERIFY(qjv_aurl.isString());
      aurl = qjv_aurl.toString();

      if (aurl.isEmpty()) {
        continue;
      }

      break;
    }

    bool exists = (!name.isEmpty() && !aurl.isEmpty());

    if (project_exists) {
      *project_exists = exists;
    }
    else {
      QVERIFY(!name.isEmpty() && name == "mera-luna");
      QVERIFY(!aurl.isEmpty());
    }

    if (exists) {
      QString url;
      QString pid;

      int url_pos = aurl.indexOf("/api/v4/projects/");
      QVERIFY( url_pos >= 0);

      url = aurl.left(url_pos);
      QVERIFY(!url.isEmpty());

      int pid_pos = aurl.indexOf("/api/v4/projects/") + QString("/api/v4/projects/").size();
      QVERIFY( pid_pos >= 0);

      pid = aurl.mid (pid_pos);
      QVERIFY(!pid.isEmpty());

      if (gitlab_pid) {
        *gitlab_pid = pid;
      }
    }

    success = true;
  }();

  return success;
}

bool create_project(QNetworkAccessManager* manager,
                    const QString&         gitlab_api,
                    const QString&         gitlab_tok) {
  bool success = false;

  [&] {
    QNetworkRequest qnetworkrequest;
    qnetworkrequest.setRawHeader("PRIVATE-TOKEN", gitlab_tok.toUtf8());
    qnetworkrequest.setRawHeader("Content-Type" , "application/json");

    QUrl qurl(GitlabCreateProjectUrl.arg(gitlab_api));
    qnetworkrequest.setUrl(qurl);

    QJsonObject qjsonobject;
    qjsonobject.insert("name"                  , QJsonValue("mera-luna"));
    qjsonobject.insert("initialize_with_readme", QJsonValue( true      ));

    QJsonDocument qjsondocument;
    qjsondocument.setObject(qjsonobject);

    QByteArray data = qjsondocument.toJson();

    QNetworkReply* reply = manager->post(qnetworkrequest, data);
    LVD_FINALLY { reply->deleteLater(); };

    QSignalSpy spy(reply, &QNetworkReply::finished);
    QVERIFY(spy.isValid());

    QVERIFY(spy.wait(16384));
    QCOMPARE(spy.size(), 1);

    int        code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QByteArray text = reply->readAll();

    QVERIFY2(code == 201,
             text);

    success = true;
  }();

  return success;
}

bool gather_git_tag(QNetworkAccessManager* manager,
                    const QString&         gitlab_api,
                    const QString&         gitlab_tok,
                    const QString&         gitlab_pid,
                          bool*            git_tag_exists) {
  bool success = false;

  if (git_tag_exists) {
    *git_tag_exists = false;
  }

  [&] {
    QNetworkRequest qnetworkrequest;
    qnetworkrequest.setRawHeader("PRIVATE-TOKEN", gitlab_tok.toUtf8());
    qnetworkrequest.setRawHeader("Content-Type" , "application/json");

    QUrl qurl(GitlabGatherGit_TagUrl.arg(gitlab_api, gitlab_pid));
    qnetworkrequest.setUrl(qurl);

    QNetworkReply* reply = manager->get (qnetworkrequest);
    LVD_FINALLY { reply->deleteLater(); };

    QSignalSpy spy(reply, &QNetworkReply::finished);
    QVERIFY(spy.isValid());

    QVERIFY(spy.wait(16384));
    QCOMPARE(spy.size(), 1);

    int        code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QByteArray text = reply->readAll();

    bool exists = (code == 200);

    if (git_tag_exists) {
      *git_tag_exists = exists;

      QVERIFY2(code == 200 || code == 404,
               MESSAGE);
    } else {
      QVERIFY2(code == 200,
               MESSAGE);
    }

    success = true;
  }();

  return success;
}

bool create_git_tag(QNetworkAccessManager* manager,
                    const QString&         gitlab_api,
                    const QString&         gitlab_tok,
                    const QString&         gitlab_pid) {
  bool success = false;

  [&] {
    QNetworkRequest qnetworkrequest;
    qnetworkrequest.setRawHeader("PRIVATE-TOKEN", gitlab_tok.toUtf8());
    qnetworkrequest.setRawHeader("Content-Type" , "application/json");

    QUrl qurl(GitlabCreateGit_TagUrl.arg(gitlab_api, gitlab_pid));
    qnetworkrequest.setUrl(qurl);

    QJsonObject qjsonobject;
    qjsonobject.insert("tag_name"              , QJsonValue("v7.8.9"));
    qjsonobject.insert("ref"                   , QJsonValue("master"));

    QJsonDocument qjsondocument;
    qjsondocument.setObject(qjsonobject);

    QByteArray data = qjsondocument.toJson();

    QNetworkReply* reply = manager->post(qnetworkrequest, data);
    LVD_FINALLY { reply->deleteLater(); };

    QSignalSpy spy(reply, &QNetworkReply::finished);
    QVERIFY(spy.isValid());

    QVERIFY(spy.wait(16384));
    QCOMPARE(spy.size(), 1);

    int        code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QByteArray text = reply->readAll();

    QVERIFY2(code == 201,
             MESSAGE);

    success = true;
  }();

  return success;
}

bool gather_release(QNetworkAccessManager* manager,
                    const QString&         gitlab_api,
                    const QString&         gitlab_tok,
                    const QString&         gitlab_pid,
                          bool*            release_exists,
                          QString*         release_name,
                          QString*         release_desc,
                          QString*         assets_name,
                          QString*         assets_link) {
  bool success = false;

  if (release_exists) {
    *release_exists = false;
  }

  [&] {
    QNetworkRequest qnetworkrequest;
    qnetworkrequest.setRawHeader("PRIVATE-TOKEN", gitlab_tok.toUtf8());
    qnetworkrequest.setRawHeader("Content-Type" , "application/json");

    QUrl qurl(GitlabGatherReleaseUrl.arg(gitlab_api, gitlab_pid));
    qnetworkrequest.setUrl(qurl);

    QNetworkReply* reply = manager->get (qnetworkrequest);
    LVD_FINALLY { reply->deleteLater(); };

    QSignalSpy spy(reply, &QNetworkReply::finished);
    QVERIFY(spy.isValid());

    QVERIFY(spy.wait(16384));
    QCOMPARE(spy.size(), 1);

    int        code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QByteArray text = reply->readAll();

    bool exists = (code == 200);

    if (release_exists) {
      *release_exists = exists;

      QVERIFY2(code == 200 || code == 403,
               MESSAGE);
    } else {
      QVERIFY2(code == 200,
               MESSAGE);
    }

    if (exists) {
      auto qjd = QJsonDocument::fromJson(text);
      QVERIFY(qjd.isObject());
      auto qjo = qjd.object();

      auto qjv_name = qjo.value("name");
      QVERIFY(qjv_name.isString());

      if (release_name) {
        *release_name = qjv_name.toString();
      }

      auto qjv_desc = qjo.value("description");
      QVERIFY(qjv_desc.isString());

      if (release_desc) {
        *release_desc = qjv_desc.toString();
      }

      auto qjv_ass = qjo.value("assets");
      if (qjv_ass.isObject()) {
        auto qjo_ass = qjv_ass.toObject();

        auto qjv_lnk = qjo_ass.value("links");
        if (qjv_lnk.isArray()) {

          auto qja_arr = qjv_lnk.toArray();
          if (qja_arr.size() > 0) {
            auto qjv_itm = qja_arr.first();

            if (qjv_itm.isObject()) {
              auto qjo_itm = qjv_itm.toObject();

              auto qjo_assets_name = qjo_itm.value("name");
              QVERIFY(qjo_assets_name.isString());

              if (assets_name) {
                *assets_name = qjo_assets_name.toString();
              }

              auto qjo_assets_link = qjo_itm.value("url" );
              QVERIFY(qjo_assets_link.isString());

              if (assets_link) {
                *assets_link = qjo_assets_link.toString();
              }
            }
          }
        }
      }
    }

    success = true;
  }();

  return success;
}

bool remove_release(QNetworkAccessManager* manager,
                    const QString&         gitlab_api,
                    const QString&         gitlab_tok,
                    const QString&         gitlab_pid) {
  bool success = false;

  [&] {
    QNetworkRequest qnetworkrequest;
    qnetworkrequest.setRawHeader("PRIVATE-TOKEN", gitlab_tok.toUtf8());
    qnetworkrequest.setRawHeader("Content-Type" , "application/json");

    QUrl qurl(GitlabRemoveReleaseUrl.arg(gitlab_api, gitlab_pid));
    qnetworkrequest.setUrl(qurl);

    QNetworkReply* reply = manager->deleteResource(qnetworkrequest);
    LVD_FINALLY { reply->deleteLater(); };

    QSignalSpy spy(reply, &QNetworkReply::finished);
    QVERIFY(spy.isValid());

    QVERIFY(spy.wait(16384));
    QCOMPARE(spy.size(), 1);

    int        code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QByteArray text = reply->readAll();

    QVERIFY2(code == 200,
             MESSAGE);

    success = true;
  }();

  return success;
}

}  // namespace Gitlab
