/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QObject>
#include <QString>

#include "commit.hpp"
#include "commit_test_data.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;

// ----------

class CommitTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Commit commit;
  }

  // ----------

  void parse_author_name() {
    Commit commit = create_commit();
    QCOMPARE(commit.author_name(), Author_Name);
  }

  void parse_author_mail() {
    Commit commit = create_commit();
    QCOMPARE(commit.author_mail(), Author_Mail);
  }

  void parse_commit_date() {
    Commit commit = create_commit();
    QCOMPARE(commit.commit_date(), Commit_Date);
  }

  void parse_subject() {
    Commit commit = create_commit();
    QCOMPARE(commit.subject(), Subject.trimmed());
  }

  void parse_subject_multiline() {
    Commit commit = create_commit_with_subject(Subject + Subject);
    QCOMPARE(commit.subject(), Subject.trimmed() + "\n" + Subject.trimmed());
  }

  void parse_message() {
    Commit commit = create_commit();
    QCOMPARE(commit.message(), Message.trimmed());
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(CommitTest)
#include "commit_test.moc"  // IWYU pragma: keep
