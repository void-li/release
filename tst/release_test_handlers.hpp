/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "lvd/core.hpp"

#include <QObject>
#include <QSettings>
#include <QString>
#include <QTimer>

#include "commit.hpp"
#include "handler.hpp"
#include "storage.hpp"

using namespace lvd;
using namespace release;

// ----------

class ReleaseTestHandler : public Handler {
  Q_OBJECT

 public:
  ReleaseTestHandler(const QString&    section,
                     const StoragePtr& storage,
                           QSettings&  settings,
                           bool        success,
                           bool        delayed,
                           QObject*    parent)
      : Handler(Handles(), section, storage, settings, parent),
        success_(success),
        delayed_(delayed) {}

 public:
  static quint64    iterations() {
    return iterations_;
  }
  static void clear_iterations() {
    iterations_ = 0;
  }

 private:
  bool handle_before_impl()              override {
    return handle();
  }

  bool handle_commit_impl(const Commit&) override {
    return handle();
  }

  bool handle_beyond_impl()              override {
    return handle();
  }

  bool handle() {
    if (delayed_) {
      if (success_) {
        QTimer::singleShot(1, this, [this] { emit success(); });
      } else {
        QTimer::singleShot(1, this, [this] { emit failure(); });
      }
    }
    else {
      if (success_) {
        emit success();
      } else {
        emit failure();
      }
    }

    iterations_ ++;
    return !delayed_;
  }

 private:
  bool success_;
  bool delayed_;

 private:
  static quint64 iterations_;
};

// ----------

class TestSuccessSyncHandler : public ReleaseTestHandler {
  Q_OBJECT

 public:
  TestSuccessSyncHandler        (const QString&    section,
                                 const StoragePtr& storage,
                                       QSettings&  settings,
                                       QObject*    parent = nullptr)
      : ReleaseTestHandler(section, storage, settings, true , false, parent) {}

  static
  TestSuccessSyncHandler* create(const QString&    section,
                                 const StoragePtr& storage,
                                       QSettings&  settings,
                                       QObject*    parent = nullptr) {
    return new TestSuccessSyncHandler(section, storage, settings, parent);
  }
};

DECLARE_HANDLER(TestSuccessSyncHandler, test_success_sync_handler);

// ----------

class TestSuccessASyncHandler : public ReleaseTestHandler {
  Q_OBJECT

 public:
  TestSuccessASyncHandler        (const QString&    section,
                                  const StoragePtr& storage,
                                        QSettings&  settings,
                                        QObject*    parent = nullptr)
      : ReleaseTestHandler(section, storage, settings, true , true , parent) {}

  static
  TestSuccessASyncHandler* create(const QString&    section,
                                  const StoragePtr& storage,
                                        QSettings&  settings,
                                        QObject*    parent = nullptr) {
    return new TestSuccessASyncHandler(section, storage, settings, parent);
  }
};

DECLARE_HANDLER(TestSuccessASyncHandler, test_success_async_handler);

// ----------

class TestFailureSyncHandler : public ReleaseTestHandler {
  Q_OBJECT

 public:
  TestFailureSyncHandler        (const QString&    section,
                                 const StoragePtr& storage,
                                       QSettings&  settings,
                                       QObject*    parent = nullptr)
      : ReleaseTestHandler(section, storage, settings, false, false, parent) {}

  static
  TestFailureSyncHandler* create(const QString&    section,
                                 const StoragePtr& storage,
                                       QSettings&  settings,
                                       QObject*    parent = nullptr) {
    return new TestFailureSyncHandler(section, storage, settings, parent);
  }
};

DECLARE_HANDLER(TestFailureSyncHandler, test_failure_sync_handler);

// ----------

class TestFailureASyncHandler : public ReleaseTestHandler {
  Q_OBJECT

 public:
  TestFailureASyncHandler        (const QString&    section,
                                  const StoragePtr& storage,
                                        QSettings&  settings,
                                        QObject*    parent = nullptr)
      : ReleaseTestHandler(section, storage, settings, false, true , parent) {}

  static
  TestFailureASyncHandler* create(const QString&    section,
                                  const StoragePtr& storage,
                                        QSettings&  settings,
                                        QObject*    parent = nullptr) {
    return new TestFailureASyncHandler(section, storage, settings, parent);
  }
};

DECLARE_HANDLER(TestFailureASyncHandler, test_failure_async_handler);

// ----------

class TestExceptionHandler : public Handler {
  Q_OBJECT

 public:
  TestExceptionHandler        (const QString&    section,
                               const StoragePtr& storage,
                                     QSettings&  settings,
                                     QObject*    parent = nullptr)
      : Handler(Handles(),
                section, storage, settings, parent) {}

  static
  TestExceptionHandler* create(const QString&    section,
                               const StoragePtr& storage,
                                     QSettings&  settings,
                                     QObject*    parent = nullptr) {
    return new TestExceptionHandler(section, storage, settings, parent);
  }

 private:
  bool handle_before_impl()              override {
    LVD_THROW_RUNTIME("Dummy");
  }

  bool handle_commit_impl(const Commit&) override {
    LVD_THROW_RUNTIME("Dummy");
  }

  bool handle_beyond_impl()              override {
    LVD_THROW_RUNTIME("Dummy");
  }
};

DECLARE_HANDLER(TestExceptionHandler, test_exception_handler);

// ----------

class TestTimeoutHandler : public Handler {
  Q_OBJECT

 public:
  TestTimeoutHandler        (const QString&    section,
                             const StoragePtr& storage,
                                   QSettings&  settings,
                                   QObject*    parent = nullptr)
      : Handler(Handles::Commit,
                section, storage, settings, parent) {}

  static
  TestTimeoutHandler* create(const QString&    section,
                             const StoragePtr& storage,
                                   QSettings&  settings,
                                   QObject*    parent = nullptr) {
    return new TestTimeoutHandler(section, storage, settings, parent);
  }

 private:
  bool handle_commit_impl(const Commit&) override {
    return false;
  }
};

DECLARE_HANDLER(TestTimeoutHandler, test_timeout_handler);
