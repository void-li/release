/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QMetaObject>
#include <QObject>
#include <QSettings>
#include <QSharedPointer>
#include <QString>

#include "handler.hpp"
#include "handlers/tagger.hpp"
#include "storage.hpp"
#include "version.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class TaggerTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Tagger        (section_, storage_, *settings_);
    Tagger::create(section_, storage_, *settings_, this)->deleteLater();
  }

  // ----------

  void tagging_version() {
    QFETCH(quint32, major);
    QFETCH(quint32, minor);
    QFETCH(quint32, patch);
    QFETCH(QString, descr);

    const Version next_version = Version(major, minor, patch, Version::default_format);
    storage_->set_next_version(next_version);

    Tagger tagger(section_, storage_, *settings_);

    QSignalSpy spy(&tagger, &Tagger::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      tagger.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    int ret;

    ret = execute("git", "describe");
    QCOMPARE(ret, 0);

    QCOMPARE(execute_stdout_.trimmed(),
             descr);
  }

  void tagging_version_data() {
    QTest::addColumn<quint32>("major");
    QTest::addColumn<quint32>("minor");
    QTest::addColumn<quint32>("patch");
    QTest::addColumn<QString>("descr");

    QTest::newRow("v0.0.0") << 0u << 0u << 0u << "v0.0.0";
    QTest::newRow("v1.2.3") << 1u << 2u << 3u << "v1.2.3";
    QTest::newRow("v6.6.6") << 6u << 6u << 6u << "v6.6.6";
  }

  void message_from_next_version() {
    //    storage_->set_curr_version(Version()); NO!

    Tagger tagger(section_, storage_, *settings_);

    QSignalSpy spy(&tagger, &Tagger::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      tagger.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    int ret;

    ret = execute("git", "tag", "-n");
    QCOMPARE(ret, 0);

    int pos = execute_stdout_.indexOf(' ');
    QCOMPARE(execute_stdout_.mid(pos).trimmed(),
             storage_->next_version().to_string());
  }

  void message_from_curr_version() {
    storage_->set_next_version(Version());

    Tagger tagger(section_, storage_, *settings_);

    QSignalSpy spy(&tagger, &Tagger::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      tagger.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    int ret;

    ret = execute("git", "tag", "-n");
    QCOMPARE(ret, 0);

    int pos = execute_stdout_.indexOf(' ');
    QCOMPARE(execute_stdout_.mid(pos).trimmed(),
             storage_->curr_version().to_string());
  }

  void message_from_user_message() {
    QFETCH(QString, message);
    QFETCH(QString, outcome);

    settings_->setValue("options", message);

    Tagger tagger(section_, storage_, *settings_);

    QSignalSpy spy(&tagger, &Tagger::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      tagger.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    int ret;

    ret = execute("git", "tag", "-n");
    QCOMPARE(ret, 0);

    int pos = execute_stdout_.indexOf(' ');
    QCOMPARE(execute_stdout_.mid(pos).trimmed(),
             outcome);
  }

  void message_from_user_message_data() {
    QTest::addColumn<QString>("message");
    QTest::addColumn<QString>("outcome");

    QTest::addRow("Mera Luna") << "\"" "Mera Luna"            "\"" << "Mera Luna";
    QTest::addRow("Version")   << "\"" "Mera Luna %{version}" "\"" << "Mera Luna v7.8.9";
    QTest::addRow("Major")     << "\"" "Mera Luna %{major""}" "\"" << "Mera Luna 7";
    QTest::addRow("Minor")     << "\"" "Mera Luna %{minor""}" "\"" << "Mera Luna 8";
    QTest::addRow("Patch")     << "\"" "Mera Luna %{patch""}" "\"" << "Mera Luna 9";
  }

  void missing_version() {
    storage_->set_curr_version(Version());
    storage_->set_next_version(Version());

    Tagger tagger(section_, storage_, *settings_);

    QSignalSpy spy(&tagger, &Tagger::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      tagger.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  // ----------

  void git_repo_removed() {
    TestHandler::remove_git_repo();

    Tagger tagger(section_, storage_, *settings_);

    QSignalSpy spy(&tagger, &Tagger::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      tagger.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_removed() {
    TestHandler::git_util_removed();

    Tagger tagger(section_, storage_, *settings_);

    QSignalSpy spy(&tagger, &Tagger::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      tagger.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_timeout() {
    TestHandler::git_util_timeout();

    QFETCH(QString, timeout);
    settings_->setValue(timeout, 1);

    Tagger tagger(section_, storage_, *settings_);

    QSignalSpy spy(&tagger, &Tagger::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      tagger.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_timeout_data() {
    QTest::addColumn<QString>("timeout");

    QTest::addRow("hard_timeout") << "hard_timeout";
    QTest::addRow("soft_timeout") << "soft_timeout";
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    create_git_repo();

    const Version prev_version = Version(1, 2, 3, Version::default_format);
    storage_->set_prev_version(prev_version);

    const Version curr_version = Version(4, 5, 6, Version::default_format);
    storage_->set_curr_version(curr_version);

    const Version next_version = Version(7, 8, 9, Version::default_format);
    storage_->set_next_version(next_version);
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(TaggerTest)
#include "tagger_test.moc"  // IWYU pragma: keep
