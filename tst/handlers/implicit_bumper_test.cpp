/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QObject>
#include <QSettings>
#include <QSharedPointer>
#include <QString>

#include "commit.hpp"
#include "commit_test_data.hpp"
#include "handlers/implicit_bumper.hpp"
#include "implicit_bump.hpp"
#include "storage.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class ImplicitBumperTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    ImplicitBumper        (section_, storage_, *settings_);
    ImplicitBumper::create(section_, storage_, *settings_, this)->deleteLater();
  }

  void construction2() {
    QFETCH(QString, key);
    QFETCH(QString, val);

    if (val.isEmpty()) {
      settings_->remove  (key);
    } else {
      settings_->setValue(key, val);
    }

    ImplicitBumper(section_, storage_, *settings_);
  }

  void construction2_data() {
    QTest::addColumn<QString>("key");
    QTest::addColumn<QString>("val");

    QTest::addRow("major") << "implicit_bump"
                           << "Major";
    QTest::addRow("minor") << "implicit_bump"
                           << "Minor";
    QTest::addRow("patch") << "implicit_bump"
                           << "Patch";
  }

  void construction_failure() {
    settings_->remove("match_subject");
    settings_->remove("match_message");

    QVERIFY_EXCEPTION_THROWN(ImplicitBumper(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure2() {
    QFETCH(QString, key);
    QFETCH(QString, val);

    if (val.isEmpty()) {
      settings_->remove  (key);
    } else {
      settings_->setValue(key, val);
    }

    QVERIFY_EXCEPTION_THROWN(ImplicitBumper(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure2_data() {
    QTest::addColumn<QString>("key");
    QTest::addColumn<QString>("val");

    QTest::addRow("implicit_bump missing") << "implicit_bump" << QString();
    QTest::addRow("implicit_bump invalid") << "implicit_bump" << "invalid";

    QTest::addRow("match_subject invalid") << "match_subject" << "([a-z]+";
    QTest::addRow("match_message invalid") << "match_message" << "([a-z]+";
  }

  // ----------

  void matching_commit_subject() {
    QFETCH(QString, settings_implicit_bump);
    QFETCH(QString, settings_match_subject);
    QFETCH(QString,                subject);
    QFETCH(ImplicitBump,     implicit_bump);

    settings_->setValue("implicit_bump", settings_implicit_bump);
    settings_->setValue("match_subject", settings_match_subject);

    ImplicitBumper implicit_bumper(section_, storage_, *settings_);

    Commit commit = create_commit_with_subject(subject);
    implicit_bumper.handle_commit(commit);

    QVERIFY (storage_->implicit_bump().valid());
    QCOMPARE(storage_->implicit_bump(), implicit_bump);
  }

  void matching_commit_subject_data() {
    QTest::addColumn<QString>("settings_implicit_bump");
    QTest::addColumn<QString>("settings_match_subject");
    QTest::addColumn<QString>(               "subject");
    QTest::addColumn<ImplicitBump>    ("implicit_bump");

    QTest::addRow("major") << "Major"
                           << "^some_subject:"
                           << "some_subject: Mera Luna"
                           << ImplicitBump(ImplicitBump::Major);

    QTest::addRow("minor") << "Minor"
                           << "^some_subject:"
                           << "some_subject: Mera Luna"
                           << ImplicitBump(ImplicitBump::Minor);

    QTest::addRow("patch") << "Patch"
                           << "^some_subject:"
                           << "some_subject: Mera Luna"
                           << ImplicitBump(ImplicitBump::Patch);
  }

  void matching_commit_subject_in_history() {
    QString settings_implicit_bump = "Major";
    QString settings_match_subject = "^some_subject";
    QString                subject = "some_subject: Mera Luna";
    ImplicitBump     implicit_bump = ImplicitBump::Major;

    settings_->setValue("implicit_bump", settings_implicit_bump);
    settings_->setValue("match_subject", settings_match_subject);

    ImplicitBumper implicit_bumper(section_, storage_, *settings_);

    Commit commit0 = create_commit_with_subject("empty" + subject);
    implicit_bumper.handle_commit(commit0);

    Commit commit1 = create_commit_with_subject(subject);
    implicit_bumper.handle_commit(commit1);

    QVERIFY (storage_->implicit_bump().valid());
    QCOMPARE(storage_->implicit_bump(), implicit_bump);
  }

  void nonsense_commit_subject() {
    QFETCH(QString, settings_implicit_bump);
    QFETCH(QString, settings_match_subject);
    QFETCH(QString,                subject);
    QFETCH(ImplicitBump,     implicit_bump);

    settings_->setValue("implicit_bump", settings_implicit_bump);
    settings_->setValue("match_subject", settings_match_subject);

    ImplicitBumper implicit_bumper(section_, storage_, *settings_);

    Commit commit = create_commit_with_subject(subject);
    implicit_bumper.handle_commit(commit);

    QVERIFY(!storage_->implicit_bump().valid());
    Q_UNUSED(implicit_bump)
  }

  void nonsense_commit_subject_data() {
    QTest::addColumn<QString>("settings_implicit_bump");
    QTest::addColumn<QString>("settings_match_subject");
    QTest::addColumn<QString>(               "subject");
    QTest::addColumn<ImplicitBump>    ("implicit_bump");

    QTest::addRow("major") << "Major"
                           << "^some_subject:"
                           << "null_subject: Mera Luna"
                           << ImplicitBump(ImplicitBump::Major);

    QTest::addRow("minor") << "Minor"
                           << "^some_subject:"
                           << "null_subject: Mera Luna"
                           << ImplicitBump(ImplicitBump::Minor);

    QTest::addRow("patch") << "Patch"
                           << "^some_subject:"
                           << "null_subject: Mera Luna"
                           << ImplicitBump(ImplicitBump::Patch);
  }

  void nonsense_commit_subject_in_history() {
    QString settings_implicit_bump = "Major";
    QString settings_match_subject = "^some_subject";
    QString                subject = "some_subject: Mera Luna";
    ImplicitBump     implicit_bump = ImplicitBump::Major;

    settings_->setValue("implicit_bump", settings_implicit_bump);
    settings_->setValue("match_subject", settings_match_subject);

    ImplicitBumper implicit_bumper(section_, storage_, *settings_);

    Commit commit0 = create_commit_with_subject("empty" + subject);
    implicit_bumper.handle_commit(commit0);

    Commit commit1 = create_commit_with_subject("empty" + subject);
    implicit_bumper.handle_commit(commit1);

    QVERIFY(!storage_->implicit_bump().valid());
    Q_UNUSED(implicit_bump)
  }

  void matching_commit_message() {
    QFETCH(QString, settings_implicit_bump);
    QFETCH(QString, settings_match_message);
    QFETCH(QString,                message);
    QFETCH(ImplicitBump,     implicit_bump);

    settings_->setValue("implicit_bump", settings_implicit_bump);
    settings_->setValue("match_message", settings_match_message);

    ImplicitBumper implicit_bumper(section_, storage_, *settings_);

    Commit commit = create_commit_with_message(message);
    implicit_bumper.handle_commit(commit);

    QVERIFY (storage_->implicit_bump().valid());
    QCOMPARE(storage_->implicit_bump(), implicit_bump);
  }

  void matching_commit_message_data() {
    QTest::addColumn<QString>("settings_implicit_bump");
    QTest::addColumn<QString>("settings_match_message");
    QTest::addColumn<QString>(               "message");
    QTest::addColumn<ImplicitBump>    ("implicit_bump");

    QTest::addRow("major") << "Major"
                           << "^some_message:"
                           << "some_message: Mera Luna"
                           << ImplicitBump(ImplicitBump::Major);

    QTest::addRow("minor") << "Minor"
                           << "^some_message:"
                           << "some_message: Mera Luna"
                           << ImplicitBump(ImplicitBump::Minor);

    QTest::addRow("patch") << "Patch"
                           << "^some_message:"
                           << "some_message: Mera Luna"
                           << ImplicitBump(ImplicitBump::Patch);
  }

  void matching_commit_message_in_history() {
    QString settings_implicit_bump = "Major";
    QString settings_match_message = "^some_message";
    QString                message = "some_message: Mera Luna";
    ImplicitBump     implicit_bump = ImplicitBump::Major;

    settings_->setValue("implicit_bump", settings_implicit_bump);
    settings_->setValue("match_message", settings_match_message);

    ImplicitBumper implicit_bumper(section_, storage_, *settings_);

    Commit commit0 = create_commit_with_message("empty" + message);
    implicit_bumper.handle_commit(commit0);

    Commit commit1 = create_commit_with_message(message);
    implicit_bumper.handle_commit(commit1);

    QVERIFY (storage_->implicit_bump().valid());
    QCOMPARE(storage_->implicit_bump(), implicit_bump);
  }

  void nonsense_commit_message() {
    QFETCH(QString, settings_implicit_bump);
    QFETCH(QString, settings_match_message);
    QFETCH(QString, message);
    QFETCH(ImplicitBump, implicit_bump);

    settings_->setValue("implicit_bump", settings_implicit_bump);
    settings_->setValue("match_message", settings_match_message);

    ImplicitBumper implicit_bumper(section_, storage_, *settings_);

    Commit commit = create_commit_with_message(message);
    implicit_bumper.handle_commit(commit);

    QVERIFY(!storage_->implicit_bump().valid());
    Q_UNUSED(implicit_bump)
  }

  void nonsense_commit_message_data() {
    QTest::addColumn<QString>("settings_implicit_bump");
    QTest::addColumn<QString>("settings_match_message");
    QTest::addColumn<QString>(               "message");
    QTest::addColumn<ImplicitBump>    ("implicit_bump");

    QTest::addRow("major") << "Major"
                           << "^some_message:"
                           << "null_message: Mera Luna"
                           << ImplicitBump(ImplicitBump::Major);

    QTest::addRow("minor") << "Minor"
                           << "^some_message:"
                           << "null_message: Mera Luna"
                           << ImplicitBump(ImplicitBump::Minor);

    QTest::addRow("patch") << "Patch"
                           << "^some_message:"
                           << "null_message: Mera Luna"
                           << ImplicitBump(ImplicitBump::Patch);
  }

  void nonsense_commit_message_in_history() {
    QString settings_implicit_bump = "Major";
    QString settings_match_message = "^some_message";
    QString                message = "some_message: Mera Luna";
    ImplicitBump     implicit_bump = ImplicitBump::Major;

    settings_->setValue("implicit_bump", settings_implicit_bump);
    settings_->setValue("match_message", settings_match_message);

    ImplicitBumper implicit_bumper(section_, storage_, *settings_);

    Commit commit0 = create_commit_with_message("empty" + message);
    implicit_bumper.handle_commit(commit0);

    Commit commit1 = create_commit_with_message("empty" + message);
    implicit_bumper.handle_commit(commit1);

    QVERIFY(!storage_->implicit_bump().valid());
    Q_UNUSED(implicit_bump)
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    settings_->setValue("implicit_bump", QString("Major"));
    settings_->setValue("match_subject", QString("Dummy"));
    settings_->setValue("match_message", QString("Dummy"));
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ImplicitBumperTest)
#include "implicit_bumper_test.moc"  // IWYU pragma: keep
