/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QObject>
#include <QSharedPointer>
#include <QString>

#include "explicit_bump.hpp"
#include "handlers/next_version_generator.hpp"
#include "implicit_bump.hpp"
#include "storage.hpp"
#include "version.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class NextVersionGeneratorTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    NextVersionGenerator        (section_, storage_, *settings_);
    NextVersionGenerator::create(section_, storage_, *settings_, this)->deleteLater();
  }

  // ----------

  void with_curr_version() {
    storage_->set_curr_version(Version(0, 0, 0, Version::default_format));

    NextVersionGenerator next_version_generator(section_, storage_, *settings_);
    next_version_generator.handle_beyond();

    QVERIFY(!storage_->next_version().valid());
  }

  void with_prev_version() {
    storage_->set_prev_version(Version(0, 0, 0, Version::default_format));

    NextVersionGenerator next_version_generator(section_, storage_, *settings_);
    next_version_generator.handle_beyond();

    QVERIFY (storage_->next_version().valid());
    QCOMPARE(storage_->next_version(), Version(0, 0, 1, Version::default_format));
  }

  void without_version() {
    storage_->set_curr_version(Version());
    storage_->set_next_version(Version());

    NextVersionGenerator next_version_generator(section_, storage_, *settings_);
    next_version_generator.handle_beyond();

    QVERIFY (storage_->next_version().valid());
    QCOMPARE(storage_->next_version(), Version(0, 0, 1, Version::default_format));
  }

  void explicit_bump() {
    QFETCH(Version,       prev_version);
    QFETCH(ExplicitBump, explicit_bump);
    QFETCH(Version,       next_version);

    storage_->set_prev_version(prev_version);
    storage_->set_explicit_bump(explicit_bump);

    NextVersionGenerator next_version_generator(section_, storage_, *settings_);
    next_version_generator.handle_beyond();

    QVERIFY (storage_->next_version().valid());
    QCOMPARE(storage_->next_version(), next_version);
  }

  void explicit_bump_data() {
    QTest::addColumn<Version>     ( "prev_version");
    QTest::addColumn<ExplicitBump>("explicit_bump");
    QTest::addColumn<Version>     ( "next_version");

    QTest::newRow("major") << Version(0, 0, 0, Version::default_format)
                           << ExplicitBump(ExplicitBump::Major)
                           << Version(1, 0, 0, Version::default_format);

    QTest::newRow("minor") << Version(0, 0, 0, Version::default_format)
                           << ExplicitBump(ExplicitBump::Minor)
                           << Version(0, 1, 0, Version::default_format);

    QTest::newRow("patch") << Version(0, 0, 0, Version::default_format)
                           << ExplicitBump(ExplicitBump::Patch)
                           << Version(0, 0, 1, Version::default_format);
  }

  void explicit_bump_beyond_100() {
    QFETCH(Version,       prev_version);
    QFETCH(ExplicitBump, explicit_bump);
    QFETCH(Version,       next_version);

    storage_->set_prev_version(prev_version);
    storage_->set_explicit_bump(explicit_bump);

    NextVersionGenerator next_version_generator(section_, storage_, *settings_);
    next_version_generator.handle_beyond();

    QVERIFY (storage_->next_version().valid());
    QCOMPARE(storage_->next_version(), next_version);
  }

  void explicit_bump_beyond_100_data() {
    QTest::addColumn<Version>     ( "prev_version");
    QTest::addColumn<ExplicitBump>("explicit_bump");
    QTest::addColumn<Version>     ( "next_version");

    QTest::newRow("major") << Version(1, 0, 0, Version::default_format)
                           << ExplicitBump(ExplicitBump::Major)
                           << Version(2, 0, 0, Version::default_format);

    QTest::newRow("minor") << Version(1, 0, 0, Version::default_format)
                           << ExplicitBump(ExplicitBump::Minor)
                           << Version(1, 1, 0, Version::default_format);

    QTest::newRow("patch") << Version(1, 0, 0, Version::default_format)
                           << ExplicitBump(ExplicitBump::Patch)
                           << Version(1, 0, 1, Version::default_format);
  }

  void implicit_bump() {
    QFETCH(Version,       prev_version);
    QFETCH(ImplicitBump, implicit_bump);
    QFETCH(Version,       next_version);

    storage_->set_prev_version(prev_version);
    storage_->set_implicit_bump(implicit_bump);

    NextVersionGenerator next_version_generator(section_, storage_, *settings_);
    next_version_generator.handle_beyond();

    QVERIFY (storage_->next_version().valid());
    QCOMPARE(storage_->next_version(), next_version);
  }

  void implicit_bump_data() {
    QTest::addColumn<Version>     ( "prev_version");
    QTest::addColumn<ImplicitBump>("implicit_bump");
    QTest::addColumn<Version>     ( "next_version");

    QTest::newRow("major") << Version(0, 0, 0, Version::default_format)
                           << ImplicitBump(ImplicitBump::Major)
                           << Version(0, 0, 1, Version::default_format);

    QTest::newRow("minor") << Version(0, 0, 0, Version::default_format)
                           << ImplicitBump(ImplicitBump::Minor)
                           << Version(0, 0, 1, Version::default_format);

    QTest::newRow("patch") << Version(0, 0, 0, Version::default_format)
                           << ImplicitBump(ImplicitBump::Patch)
                           << Version(0, 0, 1, Version::default_format);
  }

  void implicit_bump_beyond_100() {
    QFETCH(Version,       prev_version);
    QFETCH(ImplicitBump, implicit_bump);
    QFETCH(Version,       next_version);

    storage_->set_prev_version(prev_version);
    storage_->set_implicit_bump(implicit_bump);

    NextVersionGenerator next_version_generator(section_, storage_, *settings_);
    next_version_generator.handle_beyond();

    QVERIFY (storage_->next_version().valid());
    QCOMPARE(storage_->next_version(), next_version);
  }

  void implicit_bump_beyond_100_data() {
    QTest::addColumn<Version>     ( "prev_version");
    QTest::addColumn<ImplicitBump>("implicit_bump");
    QTest::addColumn<Version>     ( "next_version");

    QTest::newRow("major") << Version(1, 0, 0, Version::default_format)
                           << ImplicitBump(ImplicitBump::Major)
                           << Version(2, 0, 0, Version::default_format);

    QTest::newRow("minor") << Version(1, 0, 0, Version::default_format)
                           << ImplicitBump(ImplicitBump::Minor)
                           << Version(1, 1, 0, Version::default_format);

    QTest::newRow("patch") << Version(1, 0, 0, Version::default_format)
                           << ImplicitBump(ImplicitBump::Patch)
                           << Version(1, 0, 1, Version::default_format);
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(NextVersionGeneratorTest)
#include "next_version_generator_test.moc"  // IWYU pragma: keep
