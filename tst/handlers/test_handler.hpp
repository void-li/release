/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QByteArray>
#include <QObject>
#include <QSettings>
#include <QString>

#include "storage.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;

// ----------

class TestHandler : public Test {
  Q_OBJECT

 protected:
  void create_git_repo();
  void remove_git_repo();

  void git_util_removed();
  void git_util_timeout();
  void git_util_lagging();

  bool alter_file(const QString& path);

 protected slots:
  void init();
  void cleanup();

 protected:
  QString    section_  = "dummy";
  StoragePtr storage_;
  QSettings* settings_ = nullptr;

  QByteArray path_;
};
