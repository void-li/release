/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QMetaObject>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QStringList>

#include "commit.hpp"
#include "commit_test_data.hpp"
#include "handler.hpp"
#include "handlers/script.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class ScriptTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Script        (section_, storage_, *settings_);
    Script::create(section_, storage_, *settings_, this)->deleteLater();
  }

  void construction_handles() {
    QFETCH(QString, handles);
    QFETCH(bool   , handles_before);
    QFETCH(bool   , handles_commit);
    QFETCH(bool   , handles_beyond);

    if (!handles.contains(",")) {
      settings_->setValue("handles", handles);
    } else {
      settings_->setValue("handles", handles.split(","));
    }

    Script script(section_, storage_, *settings_);

    QCOMPARE(script.handles_before(), handles_before);
    if (handles_before) {
      QSignalSpy spy(&script, &Script::success);


      QMetaObject::invokeMethod(this, [&] {
        script.handle_before();
      }, Qt::QueuedConnection);

      QVERIFY (spy.wait(256));
      QCOMPARE(spy.size(), 1);
    }

    QCOMPARE(script.handles_commit(), handles_commit);
    if (handles_commit) {
      QSignalSpy spy(&script, &Script::success);
      QVERIFY(spy.isValid());

      QMetaObject::invokeMethod(this, [&] {
        Commit commit = create_commit();
        script.handle_commit(commit);
      }, Qt::QueuedConnection);

      QVERIFY (spy.wait(256));
      QCOMPARE(spy.size(), 1);
    }

    QCOMPARE(script.handles_beyond(), handles_beyond);
    if (handles_beyond) {
      QSignalSpy spy(&script, &Script::success);
      QVERIFY(spy.isValid());

      QMetaObject::invokeMethod(this, [&] {
        script.handle_beyond();
      }, Qt::QueuedConnection);

      QVERIFY (spy.wait(256));
      QCOMPARE(spy.size(), 1);
    }
  }

  void construction_handles_data() {
    QTest::addColumn<QString>("handles");
    QTest::addColumn<bool>   ("handles_before");
    QTest::addColumn<bool>   ("handles_commit");
    QTest::addColumn<bool>   ("handles_beyond");

    QTest::newRow("before") << "Before"               << true  << false << false;
    QTest::newRow("commit") <<        "Commit"        << false << true  << false;
    QTest::newRow("beyond") <<               "Beyond" << false << false << true ;
    QTest::newRow("plenty") << "Before,Commit,Beyond" << true  << true  << true ;
  }

  void construction_failure() {
    QFETCH(QString, key);
    QFETCH(QString, val);

    if (val.isEmpty()) {
      settings_->remove  (key);
    } else {
      settings_->setValue(key, val);
    }

    QVERIFY_EXCEPTION_THROWN(Script(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure_data() {
    QTest::addColumn<QString>("key");
    QTest::addColumn<QString>("val");

    QTest::addRow("handles missing") << "handles" << QString();
    QTest::addRow("handles invalid") << "handles" << "invalid";

    QTest::addRow("command missing") << "command" << QString();
  }

  // ----------

  void success() {
    settings_->setValue("handles", "Before");
    settings_->setValue("command", "true");

    Script script(section_, storage_, *settings_);

    QSignalSpy spy(&script, &Script::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      script.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void failure() {
    settings_->setValue("handles", "Before");
    settings_->setValue("command", "false");

    Script script(section_, storage_, *settings_);

    QSignalSpy spy(&script, &Script::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      script.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void missing() {
    settings_->setValue("handles", "Before");
    settings_->setValue("command", "pneumonoultramicroscopicsilicovolcanoconiosis");

    Script script(section_, storage_, *settings_);

    QSignalSpy spy(&script, &Script::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      script.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  // ----------

  void git_util_timeout() {
    TestHandler::git_util_timeout();

    settings_->setValue("handles", "Before");
    settings_->setValue("command", "git");

    QFETCH(QString, timeout);
    settings_->setValue(timeout, 1);

    Script script(section_, storage_, *settings_);

    QSignalSpy spy(&script, &Script::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      script.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_timeout_data() {
    QTest::addColumn<QString>("timeout");

    QTest::addRow("hard_timeout") << "hard_timeout";
    QTest::addRow("soft_timeout") << "soft_timeout";
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    settings_->setValue("handles", "Before");
    settings_->setValue("command", "true");
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ScriptTest)
#include "script_test.moc"  // IWYU pragma: keep
