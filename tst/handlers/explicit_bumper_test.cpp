/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QObject>
#include <QSettings>
#include <QSharedPointer>
#include <QString>

#include "commit.hpp"
#include "commit_test_data.hpp"
#include "explicit_bump.hpp"
#include "handlers/explicit_bumper.hpp"
#include "storage.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class ExplicitBumperTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    ExplicitBumper        (section_, storage_, *settings_);
    ExplicitBumper::create(section_, storage_, *settings_, this)->deleteLater();
  }

  void construction2() {
    QFETCH(QString, key);
    QFETCH(QString, val);

    if (val.isEmpty()) {
      settings_->remove  (key);
    } else {
      settings_->setValue(key, val);
    }

    ExplicitBumper(section_, storage_, *settings_);
  }

  void construction2_data() {
    QTest::addColumn<QString>("key");
    QTest::addColumn<QString>("val");

    QTest::addRow("major") << "explicit_bump"
                           << "Major";
    QTest::addRow("minor") << "explicit_bump"
                           << "Minor";
    QTest::addRow("patch") << "explicit_bump"
                           << "Patch";
  }

  void construction_failure() {
    settings_->remove("match_subject");
    settings_->remove("match_message");

    QVERIFY_EXCEPTION_THROWN(ExplicitBumper(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure2() {
    QFETCH(QString, key);
    QFETCH(QString, val);

    if (val.isEmpty()) {
      settings_->remove  (key);
    } else {
      settings_->setValue(key, val);
    }

    QVERIFY_EXCEPTION_THROWN(ExplicitBumper(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure2_data() {
    QTest::addColumn<QString>("key");
    QTest::addColumn<QString>("val");

    QTest::addRow("explicit_bump missing") << "explicit_bump" << QString();
    QTest::addRow("explicit_bump invalid") << "explicit_bump" << "invalid";

    QTest::addRow("match_subject invalid") << "match_subject" << "([a-z]+";
    QTest::addRow("match_message invalid") << "match_message" << "([a-z]+";
  }

  // ----------

  void matching_commit_subject() {
    QFETCH(QString, settings_explicit_bump);
    QFETCH(QString, settings_match_subject);
    QFETCH(QString,                subject);
    QFETCH(ExplicitBump,     explicit_bump);

    settings_->setValue("explicit_bump", settings_explicit_bump);
    settings_->setValue("match_subject", settings_match_subject);

    ExplicitBumper explicit_bumper(section_, storage_, *settings_);

    Commit commit = create_commit_with_subject(subject);
    explicit_bumper.handle_commit(commit);

    QVERIFY (storage_->explicit_bump().valid());
    QCOMPARE(storage_->explicit_bump(), explicit_bump);
  }

  void matching_commit_subject_data() {
    QTest::addColumn<QString>("settings_explicit_bump");
    QTest::addColumn<QString>("settings_match_subject");
    QTest::addColumn<QString>(               "subject");
    QTest::addColumn<ExplicitBump>    ("explicit_bump");

    QTest::addRow("major") << "Major"
                           << "^some_subject:"
                           << "some_subject: Mera Luna"
                           << ExplicitBump(ExplicitBump::Major);

    QTest::addRow("minor") << "Minor"
                           << "^some_subject:"
                           << "some_subject: Mera Luna"
                           << ExplicitBump(ExplicitBump::Minor);

    QTest::addRow("patch") << "Patch"
                           << "^some_subject:"
                           << "some_subject: Mera Luna"
                           << ExplicitBump(ExplicitBump::Patch);
  }

  void matching_commit_subject_in_history() {
    QString settings_explicit_bump = "Major";
    QString settings_match_subject = "^some_subject";
    QString                subject = "some_subject: Mera Luna";
    ExplicitBump     explicit_bump = ExplicitBump::Major;

    settings_->setValue("explicit_bump", settings_explicit_bump);
    settings_->setValue("match_subject", settings_match_subject);

    ExplicitBumper explicit_bumper(section_, storage_, *settings_);

    Commit commit0 = create_commit_with_subject("empty" + subject);
    explicit_bumper.handle_commit(commit0);

    Commit commit1 = create_commit_with_subject(subject);
    explicit_bumper.handle_commit(commit1);

    QVERIFY(!storage_->explicit_bump().valid());
    Q_UNUSED(explicit_bump)
  }

  void nonsense_commit_subject() {
    QFETCH(QString, settings_explicit_bump);
    QFETCH(QString, settings_match_subject);
    QFETCH(QString,                subject);
    QFETCH(ExplicitBump,     explicit_bump);

    settings_->setValue("explicit_bump", settings_explicit_bump);
    settings_->setValue("match_subject", settings_match_subject);

    ExplicitBumper explicit_bumper(section_, storage_, *settings_);

    Commit commit = create_commit_with_subject(subject);
    explicit_bumper.handle_commit(commit);

    QVERIFY(!storage_->explicit_bump().valid());
    Q_UNUSED(explicit_bump)
  }

  void nonsense_commit_subject_data() {
    QTest::addColumn<QString>("settings_explicit_bump");
    QTest::addColumn<QString>("settings_match_subject");
    QTest::addColumn<QString>(               "subject");
    QTest::addColumn<ExplicitBump>    ("explicit_bump");

    QTest::addRow("major") << "Major"
                           << "^some_subject:"
                           << "null_subject: Mera Luna"
                           << ExplicitBump(ExplicitBump::Major);

    QTest::addRow("minor") << "Minor"
                           << "^some_subject:"
                           << "null_subject: Mera Luna"
                           << ExplicitBump(ExplicitBump::Minor);

    QTest::addRow("patch") << "Patch"
                           << "^some_subject:"
                           << "null_subject: Mera Luna"
                           << ExplicitBump(ExplicitBump::Patch);
  }

  void nonsense_commit_subject_in_history() {
    QString settings_explicit_bump = "Major";
    QString settings_match_subject = "^some_subject";
    QString                subject = "some_subject: Mera Luna";
    ExplicitBump     explicit_bump = ExplicitBump::Major;

    settings_->setValue("explicit_bump", settings_explicit_bump);
    settings_->setValue("match_subject", settings_match_subject);

    ExplicitBumper explicit_bumper(section_, storage_, *settings_);

    Commit commit0 = create_commit_with_subject("empty" + subject);
    explicit_bumper.handle_commit(commit0);

    Commit commit1 = create_commit_with_subject("empty" + subject);
    explicit_bumper.handle_commit(commit1);

    QVERIFY(!storage_->explicit_bump().valid());
    Q_UNUSED(explicit_bump)
  }

  void matching_commit_message() {
    QFETCH(QString, settings_explicit_bump);
    QFETCH(QString, settings_match_message);
    QFETCH(QString,                message);
    QFETCH(ExplicitBump,     explicit_bump);

    settings_->setValue("explicit_bump", settings_explicit_bump);
    settings_->setValue("match_message", settings_match_message);

    ExplicitBumper explicit_bumper(section_, storage_, *settings_);

    Commit commit = create_commit_with_message(message);
    explicit_bumper.handle_commit(commit);

    QVERIFY (storage_->explicit_bump().valid());
    QCOMPARE(storage_->explicit_bump(), explicit_bump);
  }

  void matching_commit_message_data() {
    QTest::addColumn<QString>("settings_explicit_bump");
    QTest::addColumn<QString>("settings_match_message");
    QTest::addColumn<QString>(               "message");
    QTest::addColumn<ExplicitBump>    ("explicit_bump");

    QTest::addRow("major") << "Major"
                           << "^some_message:"
                           << "some_message: Mera Luna"
                           << ExplicitBump(ExplicitBump::Major);

    QTest::addRow("minor") << "Minor"
                           << "^some_message:"
                           << "some_message: Mera Luna"
                           << ExplicitBump(ExplicitBump::Minor);

    QTest::addRow("patch") << "Patch"
                           << "^some_message:"
                           << "some_message: Mera Luna"
                           << ExplicitBump(ExplicitBump::Patch);
  }

  void matching_commit_message_in_history() {
    QString settings_explicit_bump = "Major";
    QString settings_match_message = "^some_message";
    QString                message = "some_message: Mera Luna";
    ExplicitBump     explicit_bump = ExplicitBump::Major;

    settings_->setValue("explicit_bump", settings_explicit_bump);
    settings_->setValue("match_message", settings_match_message);

    ExplicitBumper explicit_bumper(section_, storage_, *settings_);

    Commit commit0 = create_commit_with_message("empty" + message);
    explicit_bumper.handle_commit(commit0);

    Commit commit1 = create_commit_with_message(message);
    explicit_bumper.handle_commit(commit1);

    QVERIFY(!storage_->explicit_bump().valid());
    Q_UNUSED(explicit_bump)
  }

  void nonsense_commit_message() {
    QFETCH(QString, settings_explicit_bump);
    QFETCH(QString, settings_match_message);
    QFETCH(QString, message);
    QFETCH(ExplicitBump, explicit_bump);

    settings_->setValue("explicit_bump", settings_explicit_bump);
    settings_->setValue("match_message", settings_match_message);

    ExplicitBumper explicit_bumper(section_, storage_, *settings_);

    Commit commit = create_commit_with_message(message);
    explicit_bumper.handle_commit(commit);

    QVERIFY(!storage_->explicit_bump().valid());
    Q_UNUSED(explicit_bump)
  }

  void nonsense_commit_message_data() {
    QTest::addColumn<QString>("settings_explicit_bump");
    QTest::addColumn<QString>("settings_match_message");
    QTest::addColumn<QString>(               "message");
    QTest::addColumn<ExplicitBump>    ("explicit_bump");

    QTest::addRow("major") << "Major"
                           << "^some_message:"
                           << "null_message: Mera Luna"
                           << ExplicitBump(ExplicitBump::Major);

    QTest::addRow("minor") << "Minor"
                           << "^some_message:"
                           << "null_message: Mera Luna"
                           << ExplicitBump(ExplicitBump::Minor);

    QTest::addRow("patch") << "Patch"
                           << "^some_message:"
                           << "null_message: Mera Luna"
                           << ExplicitBump(ExplicitBump::Patch);
  }

  void nonsense_commit_message_in_history() {
    QString settings_explicit_bump = "Major";
    QString settings_match_message = "^some_message";
    QString                message = "some_message: Mera Luna";
    ExplicitBump     explicit_bump = ExplicitBump::Major;

    settings_->setValue("explicit_bump", settings_explicit_bump);
    settings_->setValue("match_message", settings_match_message);

    ExplicitBumper explicit_bumper(section_, storage_, *settings_);

    Commit commit0 = create_commit_with_message("empty" + message);
    explicit_bumper.handle_commit(commit0);

    Commit commit1 = create_commit_with_message("empty" + message);
    explicit_bumper.handle_commit(commit1);

    QVERIFY(!storage_->explicit_bump().valid());
    Q_UNUSED(explicit_bump)
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    settings_->setValue("explicit_bump", QString("Major"));
    settings_->setValue("match_subject", QString("Dummy"));
    settings_->setValue("match_message", QString("Dummy"));
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ExplicitBumperTest)
#include "explicit_bumper_test.moc"  // IWYU pragma: keep
