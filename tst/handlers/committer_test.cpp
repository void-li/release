/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QMetaObject>
#include <QObject>
#include <QSettings>
#include <QSharedPointer>
#include <QString>

#include "handler.hpp"
#include "handlers/committer.hpp"
#include "storage.hpp"
#include "version.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class CommitterTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Committer        (section_, storage_, *settings_);
    Committer::create(section_, storage_, *settings_, this)->deleteLater();
  }

  // ----------

  void commit_by_world() {
    if (!alter_file("data0"))
      return;

    int ret;

    ret = execute("git", "add", "data0");
    QCOMPARE(ret, 0);

    ret = execute("git", "status");
    QCOMPARE(ret, 0);

    QVERIFY(execute_stdout_.contains("data0"));

    Committer committer(section_, storage_, *settings_);

    QSignalSpy spy(&committer, &Committer::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      committer.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    ret = execute("git", "status");
    QCOMPARE(ret, 0);

    QVERIFY(!execute_stdout_.contains("data0"));
  }

  void commit_modified() {
    { if (!alter_file("data1"))
        return;

      int ret;

      ret = execute("git", "add", "data1");
      QCOMPARE(ret, 0);

      ret = execute("git", "commit", "-m", "Mera Luna");
      QCOMPARE(ret, 0);
    }

    if (!alter_file("data0"))
      return;

    if (!alter_file("data1"))
      return;

    int ret;

    ret = execute("git", "status");
    QCOMPARE(ret, 0);

    QVERIFY (execute_stdout_.contains("data0"));
    QVERIFY (execute_stdout_.contains("data1"));

    settings_->setValue("options", "\"%{version}\" -a");

    Committer committer(section_, storage_, *settings_);

    QSignalSpy spy(&committer, &Committer::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      committer.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    ret = execute("git", "status");
    QCOMPARE(ret, 0);

    QVERIFY(!execute_stdout_.contains("data0"));
    QVERIFY(!execute_stdout_.contains("data1"));
  }

  void commit_explicit() {
    { if (!alter_file("data1"))
        return;

      int ret;

      ret = execute("git", "add", "data1");
      QCOMPARE(ret, 0);

      ret = execute("git", "commit", "-m", "Mera Luna");
      QCOMPARE(ret, 0);
    }

    if (!alter_file("data0"))
      return;

    if (!alter_file("data1"))
      return;

    int ret;

    ret = execute("git", "status");
    QCOMPARE(ret, 0);

    QVERIFY (execute_stdout_.contains("data0"));
    QVERIFY (execute_stdout_.contains("data1"));

    settings_->setValue("options", "\"%{version}\" data0");

    Committer committer(section_, storage_, *settings_);

    QSignalSpy spy(&committer, &Committer::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      committer.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    ret = execute("git", "status");
    QCOMPARE(ret, 0);

    QVERIFY(!execute_stdout_.contains("data0"));
    QVERIFY( execute_stdout_.contains("data1"));
  }

  void message_from_next_version() {
    if (!alter_file("data0"))
      return;

    settings_->setValue("options", "\"%{version}\" data0");
//    storage_->set_curr_version(Version()); NO!

    Committer committer(section_, storage_, *settings_);

    QSignalSpy spy(&committer, &Committer::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      committer.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    int ret;

    ret = execute("git", "log", "-1", "--pretty=%B");
    QCOMPARE(ret, 0);

    QCOMPARE(execute_stdout_.trimmed(),
             storage_->next_version().to_string());
  }

  void message_from_curr_version() {
    if (!alter_file("data0"))
      return;

    settings_->setValue("options", "\"%{version}\" data0");
    storage_->set_next_version(Version());

    Committer committer(section_, storage_, *settings_);

    QSignalSpy spy(&committer, &Committer::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      committer.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    int ret;

    ret = execute("git", "log", "-1", "--pretty=%B");
    QCOMPARE(ret, 0);

    QCOMPARE(execute_stdout_.trimmed(),
             storage_->curr_version().to_string());
  }

  void message_from_user_message() {
    QFETCH(QString, message);
    QFETCH(QString, outcome);

    if (!alter_file("data0"))
      return;

    settings_->setValue("options", message + " data0");

    Committer committer(section_, storage_, *settings_);

    QSignalSpy spy(&committer, &Committer::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      committer.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    int ret;

    ret = execute("git", "log", "-1", "--pretty=%B");
    QCOMPARE(ret, 0);

    QCOMPARE(execute_stdout_.trimmed(),
             outcome);
  }

  void message_from_user_message_data() {
    QTest::addColumn<QString>("message");
    QTest::addColumn<QString>("outcome");

    QTest::addRow("Mera Luna") << "\"" "Mera Luna"            "\"" << "Mera Luna";
    QTest::addRow("Version")   << "\"" "Mera Luna %{version}" "\"" << "Mera Luna v7.8.9";
    QTest::addRow("Major")     << "\"" "Mera Luna %{major""}" "\"" << "Mera Luna 7";
    QTest::addRow("Minor")     << "\"" "Mera Luna %{minor""}" "\"" << "Mera Luna 8";
    QTest::addRow("Patch")     << "\"" "Mera Luna %{patch""}" "\"" << "Mera Luna 9";
  }

  void missing_version() {
    storage_->set_curr_version(Version());
    storage_->set_next_version(Version());

    Committer committer(section_, storage_, *settings_);

    QSignalSpy spy(&committer, &Committer::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      committer.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  // ----------

  void git_repo_removed() {
    TestHandler::remove_git_repo();

    Committer committer(section_, storage_, *settings_);

    QSignalSpy spy(&committer, &Committer::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      committer.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_removed() {
    TestHandler::git_util_removed();

    Committer committer(section_, storage_, *settings_);

    QSignalSpy spy(&committer, &Committer::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      committer.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_timeout() {
    TestHandler::git_util_timeout();

    QFETCH(QString, timeout);
    settings_->setValue(timeout, 1);

    Committer committer(section_, storage_, *settings_);

    QSignalSpy spy(&committer, &Committer::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      committer.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_timeout_data() {
    QTest::addColumn<QString>("timeout");

    QTest::addRow("hard_timeout") << "hard_timeout";
    QTest::addRow("soft_timeout") << "soft_timeout";
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    create_git_repo();

    const Version prev_version = Version(1, 2, 3, Version::default_format);
    storage_->set_prev_version(prev_version);

    const Version curr_version = Version(4, 5, 6, Version::default_format);
    storage_->set_curr_version(curr_version);

    const Version next_version = Version(7, 8, 9, Version::default_format);
    storage_->set_next_version(next_version);
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(CommitterTest)
#include "committer_test.moc"  // IWYU pragma: keep
