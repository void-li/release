/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test_handler.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QtTest>

#include <QDir>
#include <QFile>
#include <QIODevice>
#include <QSharedPointer>

#include "version.hpp"

#include "test_config.hpp"

// ----------

void TestHandler::create_git_repo() {
  int ret;

  ret = execute("git", "init");
  QCOMPARE(ret, 0);

  ret = execute("git", "config", "user.name" , "Derp Derpington");
  QCOMPARE(ret, 0);

  ret = execute("git", "config", "user.email", "derp.derpington@example.org");
  QCOMPARE(ret, 0);

  QFile qfile("data0");
  qfile.open(QFile::WriteOnly);

  QVERIFY(qfile.isOpen());

  qfile.write("Mera Luna");
  qfile.close();

  ret = execute("git", "add", qfile.fileName());
  QCOMPARE(ret, 0);

  ret = execute("git", "commit", "-m", "Initial Commit");
  QCOMPARE(ret, 0);
}

void TestHandler::remove_git_repo() {
  bool ret;

  QDir  qdir(".git");
  ret = qdir.removeRecursively();

  QVERIFY(ret);
}

void TestHandler::git_util_removed() {
  if (path_.isEmpty()) {
    path_ = qgetenv("PATH");
  }

  bool ret = qputenv("PATH", Git_Util_Removed_Path().toLocal8Bit());
  QVERIFY(ret);
}

void TestHandler::git_util_timeout() {
  if (path_.isEmpty()) {
    path_ = qgetenv("PATH");
  }

  bool ret = qputenv("PATH", Git_Util_Timeout_Path().toLocal8Bit());
  QVERIFY(ret);
}

void TestHandler::git_util_lagging() {
  if (path_.isEmpty()) {
    path_ = qgetenv("PATH");
  }

  bool ret = qputenv("PATH", Git_Util_Lagging_Path().toLocal8Bit());
  QVERIFY(ret);
}

bool TestHandler::alter_file(const QString &path) {
  bool success = false;

  [&] {
    QFile qfile(path);
    qfile.open(QFile::Append);

    QVERIFY(qfile.isOpen());

    qfile.write("Mera Luna");
    qfile.close();

    success = true;
  }();

  return success;
}

void TestHandler::init() {
  Test::init();

  settings_ = new QSettings("li.void.release.conf", QSettings::IniFormat, this);
  settings_->sync();

  storage_  = Storage::create();
  storage_ ->set_version_format(Version::default_format);
}

void TestHandler::cleanup() {
  LVD_FINALLY {
    Test::cleanup();
  };

  if (!path_.isEmpty() ) {
    bool ret = qputenv("PATH", path_);
    if (!ret) {
      QWARN("PATH restoration failed");
    }

    path_.clear();
  }

  if (storage_ ) {
    storage_ ->deleteLater();
    storage_  = nullptr;
  }

  if (settings_) {
    settings_->sync();

    settings_->deleteLater();
    settings_ = nullptr;
  }
}
