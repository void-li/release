/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QByteArray>
#include <QMetaObject>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QStringList>

#include "commit.hpp"
#include "commit_test_data.hpp"
#include "handler.hpp"
#include "handlers/gitconfig.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class GitconfigTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Gitconfig        (section_, storage_, *settings_);
    Gitconfig::create(section_, storage_, *settings_, this)->deleteLater();
  }

  void construction_handles() {
    QFETCH(QString, handles);
    QFETCH(bool   , handles_before);
    QFETCH(bool   , handles_commit);
    QFETCH(bool   , handles_beyond);

    if (!handles.contains(",")) {
      settings_->setValue("handles", handles);
    } else {
      settings_->setValue("handles", handles.split(","));
    }

    Gitconfig gitconfig(section_, storage_, *settings_);

    QCOMPARE(gitconfig.handles_before(), handles_before);
    if (handles_before) {
      QSignalSpy spy(&gitconfig, &Gitconfig::success);
      QVERIFY(spy.isValid());

      QMetaObject::invokeMethod(this, [&] {
        gitconfig.handle_before();
      }, Qt::QueuedConnection);

      QVERIFY (spy.wait(256));
      QCOMPARE(spy.size(), 1);
    }

    QCOMPARE(gitconfig.handles_commit(), handles_commit);
    if (handles_commit) {
      QSignalSpy spy(&gitconfig, &Gitconfig::success);
      QVERIFY(spy.isValid());

      QMetaObject::invokeMethod(this, [&] {
        Commit commit = create_commit();
        gitconfig.handle_commit(commit);
      }, Qt::QueuedConnection);

      QVERIFY (spy.wait(256));
      QCOMPARE(spy.size(), 1);
    }

    QCOMPARE(gitconfig.handles_beyond(), handles_beyond);
    if (handles_beyond) {
      QSignalSpy spy(&gitconfig, &Gitconfig::success);
      QVERIFY(spy.isValid());

      QMetaObject::invokeMethod(this, [&] {
        gitconfig.handle_beyond();
      }, Qt::QueuedConnection);

      QVERIFY (spy.wait(256));
      QCOMPARE(spy.size(), 1);
    }
  }

  void construction_handles_data() {
    QTest::addColumn<QString>("handles");
    QTest::addColumn<bool>   ("handles_before");
    QTest::addColumn<bool>   ("handles_commit");
    QTest::addColumn<bool>   ("handles_beyond");

    QTest::newRow("before") << "Before"               << true  << false << false;
    QTest::newRow("commit") <<        "Commit"        << false << true  << false;
    QTest::newRow("beyond") <<               "Beyond" << false << false << true ;
    QTest::newRow("plenty") << "Before,Commit,Beyond" << true  << true  << true ;
  }

  void construction_failure() {
    settings_->remove("user.name" );
    settings_->remove("user.email");

    QVERIFY_EXCEPTION_THROWN(Gitconfig(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure2() {
    QFETCH(QString, key);
    QFETCH(QString, val);

    if (val.isEmpty()) {
      settings_->remove  (key);
    } else {
      settings_->setValue(key, val);
    }

    QVERIFY_EXCEPTION_THROWN(Gitconfig(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure2_data() {
    QTest::addColumn<QString>("key");
    QTest::addColumn<QString>("val");

    QTest::addRow("handles missing") << "handles" << QString();
    QTest::addRow("handles invalid") << "handles" << "invalid";
  }

  // ----------

  void configure_name() {
    settings_->setValue("handles", "Before");
    settings_->setValue("user.name" , Author_Name);

    Gitconfig gitconfig(section_, storage_, *settings_);

    QSignalSpy spy(&gitconfig, &Gitconfig::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      gitconfig.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    int ret;

    ret = execute("git", "config", "--local", "--get", "user.name");
    QCOMPARE(ret, 0);

    QCOMPARE(execute_stdout_.trimmed(),
             Author_Name);
  }

  void configure_mail() {
    settings_->setValue("handles", "Beyond");
    settings_->setValue("user.email", Author_Mail);

    Gitconfig gitconfig(section_, storage_, *settings_);

    QSignalSpy spy(&gitconfig, &Gitconfig::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      gitconfig.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    int ret;

    ret = execute("git", "config", "--local", "--get", "user.email");
    QCOMPARE(ret, 0);

    QCOMPARE(execute_stdout_.trimmed(),
             Author_Mail);
  }

  void configure_both() {
    settings_->setValue("handles", "Commit");
    settings_->setValue("user.name" , Author_Name);
    settings_->setValue("user.email", Author_Mail);

    Gitconfig gitconfig(section_, storage_, *settings_);

    QSignalSpy spy(&gitconfig, &Gitconfig::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      Commit commit = create_commit();
      gitconfig.handle_commit(commit);
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    int ret;

    ret = execute("git", "config", "--local", "--get", "user.name");
    QCOMPARE(ret, 0);

    QCOMPARE(execute_stdout_.trimmed(),
             Author_Name);

    ret = execute("git", "config", "--local", "--get", "user.email");
    QCOMPARE(ret, 0);

    QCOMPARE(execute_stdout_.trimmed(),
             Author_Mail);
  }

  // ----------

  void git_repo_removed() {
    TestHandler::remove_git_repo();

    Gitconfig gitconfig(section_, storage_, *settings_);

    QSignalSpy spy(&gitconfig, &Gitconfig::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      gitconfig.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_removed() {
    TestHandler::git_util_removed();

    Gitconfig gitconfig(section_, storage_, *settings_);

    QSignalSpy spy(&gitconfig, &Gitconfig::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      gitconfig.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_timeout() {
    TestHandler::git_util_timeout();

    QFETCH(QString, timeout);
    settings_->setValue(timeout, 1);

    Gitconfig gitconfig(section_, storage_, *settings_);

    QSignalSpy spy(&gitconfig, &Gitconfig::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      gitconfig.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_timeout_data() {
    QTest::addColumn<QString>("timeout");

    QTest::addRow("hard_timeout") << "hard_timeout";
    QTest::addRow("soft_timeout") << "soft_timeout";
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    create_git_repo();

    settings_->setValue("handles", "Before");

    settings_->setValue("user.name" , Author_Name);
    settings_->setValue("user.email", Author_Mail);
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(GitconfigTest)
#include "gitconfig_test.moc"  // IWYU pragma: keep
