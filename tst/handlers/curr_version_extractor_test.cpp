/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QMetaObject>
#include <QObject>
#include <QSettings>
#include <QSharedPointer>
#include <QString>

#include "handler.hpp"
#include "handlers/curr_version_extractor.hpp"
#include "storage.hpp"
#include "version.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class CurrVersionExtractorTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    CurrVersionExtractor        (section_, storage_, *settings_);
    CurrVersionExtractor::create(section_, storage_, *settings_, this)->deleteLater();
  }

  // ----------

  void extract_version() {
    CurrVersionExtractor curr_version_extractor(section_, storage_, *settings_);

    QSignalSpy spy(&curr_version_extractor, &CurrVersionExtractor::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      curr_version_extractor.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QVERIFY (storage_->curr_version().valid());
    QCOMPARE(storage_->curr_version().major(), 4);
    QCOMPARE(storage_->curr_version().minor(), 5);
    QCOMPARE(storage_->curr_version().patch(), 6);
  }

  void missing_version() {
    int ret;

    ret = execute("git", "tag", "-d", "v4.5.6");
    QCOMPARE(ret, 0);

    CurrVersionExtractor curr_version_extractor(section_, storage_, *settings_);

    QSignalSpy spy(&curr_version_extractor, &CurrVersionExtractor::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      curr_version_extractor.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QVERIFY(!storage_->curr_version().valid());
  }

  void missing_version_format() {
    storage_->set_version_format(QString());

    CurrVersionExtractor curr_version_extractor(section_, storage_, *settings_);

    QSignalSpy spy(&curr_version_extractor, &CurrVersionExtractor::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      curr_version_extractor.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QVERIFY(!storage_->prev_version().valid());
  }

  void invalid_version() {
    int ret;

    ret = execute("git", "tag", "-d", "v4.5.6");
    QCOMPARE(ret, 0);

    ret = execute("git", "tag", "-a", "V4-5-6", "-m", "Version v4.5.6");
    QCOMPARE(ret, 0);

    CurrVersionExtractor curr_version_extractor(section_, storage_, *settings_);

    QSignalSpy spy(&curr_version_extractor, &CurrVersionExtractor::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      curr_version_extractor.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QVERIFY(!storage_->curr_version().valid());
  }

  // ----------

  void git_repo_removed() {
    TestHandler::remove_git_repo();

    CurrVersionExtractor curr_version_extractor(section_, storage_, *settings_);

    QSignalSpy spy(&curr_version_extractor, &CurrVersionExtractor::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      curr_version_extractor.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QVERIFY(!storage_->curr_version().valid());
  }

  void git_util_removed() {
    TestHandler::git_util_removed();

    CurrVersionExtractor curr_version_extractor(section_, storage_, *settings_);

    QSignalSpy spy(&curr_version_extractor, &CurrVersionExtractor::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      curr_version_extractor.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QVERIFY(!storage_->curr_version().valid());
  }

  void git_util_timeout() {
    TestHandler::git_util_timeout();

    QFETCH(QString, timeout);
    settings_->setValue(timeout, 1);

    CurrVersionExtractor curr_version_extractor(section_, storage_, *settings_);

    QSignalSpy spy(&curr_version_extractor, &CurrVersionExtractor::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      curr_version_extractor.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QVERIFY(!storage_->curr_version().valid());
  }

  void git_util_timeout_data() {
    QTest::addColumn<QString>("timeout");

    QTest::addRow("hard_timeout") << "hard_timeout";
    QTest::addRow("soft_timeout") << "soft_timeout";
  }

  // ----------

 private:
  void config_git_repo() {
    int ret;

    ret = execute("git", "tag", "-a", "v1.2.3", "-m", "Version 1.2.3");
    QCOMPARE(ret, 0);

    if (!alter_file("data0"))
      return;

    ret = execute("git", "commit", "-m", "Another Commit", "-a");
    QCOMPARE(ret, 0);

    ret = execute("git", "tag", "-a", "v4.5.6", "-m", "Version 4.5.6");
    QCOMPARE(ret, 0);
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    create_git_repo();
    config_git_repo();
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(CurrVersionExtractorTest)
#include "curr_version_extractor_test.moc"  // IWYU pragma: keep
