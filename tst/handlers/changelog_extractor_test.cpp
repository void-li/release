/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QFile>
#include <QIODevice>
#include <QObject>
#include <QSettings>
#include <QSharedPointer>
#include <QString>

#include "handlers/changelog_extractor.hpp"
#include "storage.hpp"
#include "version.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class ChangelogExtractorTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    ChangelogExtractor        (section_, storage_, *settings_);
    ChangelogExtractor::create(section_, storage_, *settings_, this)->deleteLater();
  }

  void construction_failure() {
    QFETCH(QString, key);
    QFETCH(QString, val);

    if (val.isEmpty()) {
      settings_->remove  (key);
    } else {
      settings_->setValue(key, val);
    }

    QVERIFY_EXCEPTION_THROWN(ChangelogExtractor(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure_data() {
    QTest::addColumn<QString>("key");
    QTest::addColumn<QString>("val");

    QTest::addRow("changelog missing")    << "changelog"    << QString();

    QTest::addRow("title_starts missing") << "title_starts" << QString();
    QTest::addRow("title_starts invalid") << "title_starts" << "([a-z]+";
    QTest::addRow("title_inside invalid") << "title_inside" << "([a-z]+";
    QTest::addRow("title_parser invalid") << "title_parser" << "([a-z]+";

    QTest::addRow("group_starts missing") << "group_starts" << QString();
    QTest::addRow("group_starts invalid") << "group_starts" << "([a-z]+";
    QTest::addRow("group_inside invalid") << "group_inside" << "([a-z]+";
    QTest::addRow("group_parser invalid") << "group_parser" << "([a-z]+";

    QTest::addRow("entry_starts missing") << "entry_starts" << QString();
    QTest::addRow("entry_starts invalid") << "entry_starts" << "([a-z]+";
    QTest::addRow("entry_inside invalid") << "entry_inside" << "([a-z]+";
  }

  // ----------

  void simple_changelog() {
    const QString changelog =
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
;

    save_changelog(changelog);

    ChangelogExtractor changelog_extractor(section_, storage_, *settings_);
    changelog_extractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 1);

    QCOMPARE(storage_->changelog_group(0), "Major Changes");
    QCOMPARE(storage_->changelog_group_size(0), 1);

    QCOMPARE(storage_->changelog_entry_subject(0, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy");
    QCOMPARE(storage_->changelog_entry_message(0, 0), "");
  }

  void complex_changelog() {
      const QString changelog =
  "## Changes in 1.0.0"                                                       "\n"
  ""                                                                          "\n"
  "### Major Changes"                                                         "\n"
  ""                                                                          "\n"
  "- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  ""                                                                          "\n"
  "- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  "  eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
  "  voluptua."                                                               "\n"
  ""                                                                          "\n"
  "- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  "  eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
  "  voluptua."                                                               "\n"
  ""                                                                          "\n"
  "  At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
  "  gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
  "  ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy."     "\n"
  ""                                                                          "\n"
  "### Minor Changes"                                                         "\n"
  ""                                                                          "\n"
  "- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  "  eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
  "  voluptua."                                                               "\n"
  ""                                                                          "\n"
  "  At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
  "  gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
  "  ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy."     "\n"
  ""                                                                          "\n"
  "- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  "  eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
  "  voluptua."                                                               "\n"
  ""                                                                          "\n"
  "  At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
  "  gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
  "  ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy."     "\n"
  ""                                                                          "\n"
  "### Patches"                                                               "\n"
  ""                                                                          "\n"
  "- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  ""                                                                          "\n"
  "- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  ""                                                                          "\n"
  "- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  ;

    save_changelog(changelog);

    ChangelogExtractor changelog_extractor(section_, storage_, *settings_);
    changelog_extractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 3);

    QCOMPARE(storage_->changelog_group(0), "Major Changes");
    QCOMPARE(storage_->changelog_group_size(0), 3);

    QCOMPARE(storage_->changelog_entry_subject(0, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy");
    QCOMPARE(storage_->changelog_entry_message(0, 0), "");

    QCOMPARE(storage_->changelog_entry_subject(0, 1),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
"voluptua.");
    QCOMPARE(storage_->changelog_entry_message(0, 1), "");

    QCOMPARE(storage_->changelog_entry_subject(0, 2),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
"voluptua.");

    QCOMPARE(storage_->changelog_entry_message(0, 2),
"At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
"gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
"ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy.");

    QCOMPARE(storage_->changelog_group(1), "Minor Changes");
    QCOMPARE(storage_->changelog_group_size(1), 2);

    QCOMPARE(storage_->changelog_entry_subject(1, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
"voluptua.");

    QCOMPARE(storage_->changelog_entry_message(1, 0),
"At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
"gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
"ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy.");

    QCOMPARE(storage_->changelog_entry_subject(1, 1),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
"voluptua.");

    QCOMPARE(storage_->changelog_entry_message(1, 1),
"At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
"gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
"ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy.");

    QCOMPARE(storage_->changelog_group(2), "Patches");
    QCOMPARE(storage_->changelog_group_size(2), 3);

    QCOMPARE(storage_->changelog_entry_subject(2, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy");
    QCOMPARE(storage_->changelog_entry_message(2, 0), "");

    QCOMPARE(storage_->changelog_entry_subject(2, 1),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy");
    QCOMPARE(storage_->changelog_entry_message(2, 1), "");

    QCOMPARE(storage_->changelog_entry_subject(2, 2),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy");
    QCOMPARE(storage_->changelog_entry_message(2, 2), "");
  }

  void extract_changelog_after() {
    const QString changelog =
"## Changes in 2.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non #1""\n"
""                                                                          "\n"
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non #2""\n"
;

    save_changelog(changelog);

    ChangelogExtractor changelogExtractor(section_, storage_, *settings_);
    changelogExtractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 1);

    QCOMPARE(storage_->changelog_group(0), "Major Changes");
    QCOMPARE(storage_->changelog_group_size(0), 1);

    QCOMPARE(storage_->changelog_entry_subject(0, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non #2");
    QCOMPARE(storage_->changelog_entry_message(0, 0), "");
  }

  void extract_changelog_before() {
    const QString changelog =
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non #2""\n"
""                                                                          "\n"
"## Changes in 0.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non #3""\n"
;

    save_changelog(changelog);

    ChangelogExtractor changelogExtractor(section_, storage_, *settings_);
    changelogExtractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 1);

    QCOMPARE(storage_->changelog_group(0), "Major Changes");
    QCOMPARE(storage_->changelog_group_size(0), 1);

    QCOMPARE(storage_->changelog_entry_subject(0, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non #2");
    QCOMPARE(storage_->changelog_entry_message(0, 0), "");
  }

  void extract_changelog_between() {
    const QString changelog =
"## Changes in 2.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non #1""\n"
""                                                                          "\n"
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non #2""\n"
""                                                                          "\n"
"## Changes in 0.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non #3""\n"
;

    save_changelog(changelog);

    ChangelogExtractor changelogExtractor(section_, storage_, *settings_);
    changelogExtractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 1);

    QCOMPARE(storage_->changelog_group(0), "Major Changes");
    QCOMPARE(storage_->changelog_group_size(0), 1);

    QCOMPARE(storage_->changelog_entry_subject(0, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non #2");
    QCOMPARE(storage_->changelog_entry_message(0, 0), "");
  }

  void extract_changelog_between_nonsense() {
    const QString changelog =
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy"  "\n"
"eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam"  "\n"
"voluptua. At vero eos et accusam et justo duo dolores et ea rebum."        "\n"
""                                                                          "\n"
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"# Lorem ipsum"                                                             "\n"
""                                                                          "\n"
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy"  "\n"
"eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam"  "\n"
"voluptua. At vero eos et accusam et justo duo dolores et ea rebum."        "\n"
;

    save_changelog(changelog);

    ChangelogExtractor changelogExtractor(section_, storage_, *settings_);
    changelogExtractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 1);

    QCOMPARE(storage_->changelog_group(0), "Major Changes");
    QCOMPARE(storage_->changelog_group_size(0), 1);

    QCOMPARE(storage_->changelog_entry_subject(0, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy");
    QCOMPARE(storage_->changelog_entry_message(0, 0), "");
  }

  void multiline_title() {
    const QString changelog =
"## Changes"                                                                "\n"
"## in 1.0.0"                                                               "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
;

    save_changelog(changelog);

    settings_->setValue("title_inside", "^## (.+)");
    settings_->setValue("title_parser", "(?<major>[0-9]+).(?<minor>[0-9]+).(?<patch>[0-9]+)");

    ChangelogExtractor changelogExtractor(section_, storage_, *settings_);
    changelogExtractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 1);

    QCOMPARE(storage_->changelog_group(0), "Major Changes");
    QCOMPARE(storage_->changelog_group_size(0), 1);

    QCOMPARE(storage_->changelog_entry_subject(0, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy");
    QCOMPARE(storage_->changelog_entry_message(0, 0), "");
  }

  void multiline_group() {
    const QString changelog =
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes and"                                                     "\n"
"### Minor Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
;

    save_changelog(changelog);

    settings_->setValue("group_inside", "^### (.+)");
    settings_->setValue("group_format", "^### (?<group>[^$]+)");

    ChangelogExtractor changelogExtractor(section_, storage_, *settings_);
    changelogExtractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 1);

    QCOMPARE(storage_->changelog_group(0), "Major Changes and\nMinor Changes");
    QCOMPARE(storage_->changelog_group_size(0), 1);

    QCOMPARE(storage_->changelog_entry_subject(0, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy");
    QCOMPARE(storage_->changelog_entry_message(0, 0), "");
  }

  void next_version_based() {
    const QString changelog =
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
;

    save_changelog(changelog);

    storage_->set_curr_version(Version());
    storage_->set_next_version(Version(1, 0, 0,
                                       Version::default_format));

    ChangelogExtractor changelogExtractor(section_, storage_, *settings_);
    changelogExtractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 1);

    QCOMPARE(storage_->changelog_group(0), "Major Changes");
    QCOMPARE(storage_->changelog_group_size(0), 1);

    QCOMPARE(storage_->changelog_entry_subject(0, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy");
    QCOMPARE(storage_->changelog_entry_message(0, 0), "");
  }

  void curr_version_based() {
    const QString changelog =
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
;

    save_changelog(changelog);

    storage_->set_next_version(Version());
    storage_->set_curr_version(Version(1, 0, 0,
                                       Version::default_format));

    ChangelogExtractor changelogExtractor(section_, storage_, *settings_);
    changelogExtractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 1);

    QCOMPARE(storage_->changelog_group(0), "Major Changes");
    QCOMPARE(storage_->changelog_group_size(0), 1);

    QCOMPARE(storage_->changelog_entry_subject(0, 0),
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy");
    QCOMPARE(storage_->changelog_entry_message(0, 0), "");
  }

  void no_changelog_file() {
    ChangelogExtractor changelogExtractor(section_, storage_, *settings_);
    changelogExtractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 0);
  }

  void no_changelog_datA() {
    const QString changelog =
"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy"  "\n"
"eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam"  "\n"
"voluptua. At vero eos et accusam et justo duo dolores et ea rebum."        "\n"
;

    save_changelog(changelog);

    ChangelogExtractor changelogExtractor(section_, storage_, *settings_);
    changelogExtractor.handle_beyond();

    QCOMPARE(storage_->changelog_size(), 0);
  }

  // ----------

 private:
  auto load_changelog() -> QString {
    QFile qfile("CHANGELOG.md");
    qfile.open(QFile::ReadOnly);

    QTEST_ASSERT(qfile.isOpen());
    return QString(qfile.readAll());
  }

  void save_changelog(const QString& changelog) {
    QFile qfile("CHANGELOG.md");
    qfile.open(QFile::WriteOnly);

    QTEST_ASSERT(qfile.isOpen());
    qfile.write(changelog.toUtf8());
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    settings_->setValue("changelog", "CHANGELOG.md");

    settings_->setValue("title_starts", "^## ");
    settings_->setValue("title_parser", "^## Changes in (?<major>[0-9]+).(?<minor>[0-9]+).(?<patch>[0-9]+)");

    settings_->setValue("group_starts", "^### ");
    settings_->setValue("group_parser", "^### (?<group>[^$]+)");

    settings_->setValue("entry_starts", "^- (?<entry>.+)");
    settings_->setValue("entry_inside", "^(  (?<entry>.+)|)$");

    storage_ ->set_next_version(Version(1, 0, 0,
                                        Version::default_format));
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ChangelogExtractorTest)
#include "changelog_extractor_test.moc"  // IWYU pragma: keep
