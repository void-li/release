/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QByteArray>
#include <QMetaObject>
#include <QNetworkAccessManager>
#include <QObject>
#include <QSettings>
#include <QSharedPointer>
#include <QString>

#include "changelog_formatter.hpp"
#include "gitlab.hpp"
#include "handler.hpp"
#include "handlers/changelog_publisher_gitlab.hpp"
#include "storage.hpp"
#include "version.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class ChangelogPublisherGitlabTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    ChangelogPublisherGitlab        (section_, storage_, *settings_);
    ChangelogPublisherGitlab::create(section_, storage_, *settings_, this)->deleteLater();
  }

  void construction_failure() {
    QFETCH(QString, key);
    QFETCH(QString, val);

    if (val.isEmpty()) {
      settings_->remove  (key);
    } else {
      settings_->setValue(key, val);
    }

    QVERIFY_EXCEPTION_THROWN(ChangelogPublisherGitlab(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure_data() {
    QTest::addColumn<QString>("key");
    QTest::addColumn<QString>("val");

    QTest::addRow("gitlab_api missing")   << "gitlab_api"   << QString();
    QTest::addRow("gitlab_pid missing")   << "gitlab_pid"   << QString();
    QTest::addRow("gitlab_tok missing")   << "gitlab_tok"   << QString();

    QTest::addRow("title_format missing") << "title_format" << QString();
    QTest::addRow("group_format missing") << "group_format" << QString();
    QTest::addRow("entry_format missing") << "entry_format" << QString();
  }

  // ----------

  void create_release() {
    ChangelogPublisherGitlab changelog_publisher_gitlab(section_, storage_, *settings_);

    QSignalSpy spy(&changelog_publisher_gitlab, &ChangelogPublisherGitlab::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      changelog_publisher_gitlab.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY(spy.wait(16384));
    QCOMPARE(spy.size(), 1);

    QString release_name;
    QString release_desc;

    bool ok;

    { ok = Gitlab::gather_release(manager_, gitlab_api_, gitlab_tok_,  gitlab_pid_, nullptr, &release_name, &release_desc);
      if (!ok) { return; } }

    Version version;
    if (!version.valid()) {
      version = storage_->next_version();
    }
    if (!version.valid()) {
      version = storage_->curr_version();
    }

    QString changelog = changelog_formatter(storage_.get(),
                                            version,
                                            title_format_,
                                            group_format_,
                                            entry_format_);

    QString testing_name = ChangelogPublisherGitlab::create_release_subject(subject_, version);
    QString testing_desc = ChangelogPublisherGitlab::create_release_message(message_, version, changelog);

    QCOMPARE(release_name, testing_name);
    QCOMPARE(release_desc, testing_desc);
  }

  void update_release() {
    create_release();

    ChangelogPublisherGitlab changelog_publisher_gitlab(section_, storage_, *settings_);

    QSignalSpy spy(&changelog_publisher_gitlab, &ChangelogPublisherGitlab::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      changelog_publisher_gitlab.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY(spy.wait(16384));
    QCOMPARE(spy.size(), 1);

    QString release_name;
    QString release_desc;

    bool ok;

    { ok = Gitlab::gather_release(manager_, gitlab_api_, gitlab_tok_,  gitlab_pid_, nullptr, &release_name, &release_desc);
      if (!ok) { return; } }

    Version version;
    if (!version.valid()) {
      version = storage_->next_version();
    }
    if (!version.valid()) {
      version = storage_->curr_version();
    }

    QString changelog = changelog_formatter(storage_.get(),
                                            version,
                                            title_format_,
                                            group_format_,
                                            entry_format_);

    QString testing_name = ChangelogPublisherGitlab::create_release_subject(subject_, version);
    QString testing_desc = ChangelogPublisherGitlab::create_release_message(message_, version, changelog);

    QCOMPARE(release_name, testing_name);
    QCOMPARE(release_desc, testing_desc);
  }

  // ----------

  void assets() {
    settings_->setValue("assets_name",                   "Release"       );
    settings_->setValue("assets_link", "https://localhost/release.tar.gz");

    ChangelogPublisherGitlab changelog_publisher_gitlab(section_, storage_, *settings_);

    QSignalSpy spy(&changelog_publisher_gitlab, &ChangelogPublisherGitlab::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      changelog_publisher_gitlab.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY(spy.wait(16384));
    QCOMPARE(spy.size(), 1);

    QString release_name;
    QString release_desc;

    QString assets_name;
    QString assets_link;

    bool ok;

    { ok = Gitlab::gather_release(manager_, gitlab_api_, gitlab_tok_,  gitlab_pid_, nullptr, &release_name, &release_desc, &assets_name, &assets_link);
      if (!ok) { return; } }

    QCOMPARE(assets_name,
                               "Release"       );

    QCOMPARE(assets_link,
             "https://localhost/release.tar.gz");
  }

  // ----------

  void networks_timeout() {
    QFETCH(QString, timeout);
    settings_->setValue(timeout, 1);

    ChangelogPublisherGitlab changelog_publisher_gitlab(section_, storage_, *settings_);

    QSignalSpy spy(&changelog_publisher_gitlab, &ChangelogPublisherGitlab::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      changelog_publisher_gitlab.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void networks_timeout_data() {
    QTest::addColumn<QString>("timeout");

    QTest::addRow("hard_timeout") << "hard_timeout";
    QTest::addRow("soft_timeout") << "soft_timeout";
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    settings_->setValue("gitlab_api", gitlab_api_);
    settings_->setValue("gitlab_pid", gitlab_pid_);
    settings_->setValue("gitlab_tok", gitlab_tok_);

    settings_->setValue("title_format", title_format_);
    settings_->setValue("group_format", group_format_);
    settings_->setValue("entry_format", entry_format_);

    const Version prev_version = Version(1, 2, 3, Version::default_format);
    storage_->set_prev_version(prev_version);

    const Version curr_version = Version(4, 5, 6, Version::default_format);
    storage_->set_curr_version(curr_version);

    const Version next_version = Version(7, 8, 9, Version::default_format);
    storage_->set_next_version(next_version);

    storage_->add_changelog_group("Major Changes");
    storage_->add_changelog_entry("Major Changes", "Mera Luna", "Moon Dust");

    storage_->add_changelog_group("Minor Changes");
    storage_->add_changelog_entry("Minor Changes", "Moon Dust", "Mera Luna");

    bool ok;

    bool release_exists = false;
    { ok = Gitlab::gather_release(manager_, gitlab_api_, gitlab_tok_,  gitlab_pid_, &release_exists);
      if (!ok) { QVERIFY(false); } }

    if ( release_exists) {
      ok = Gitlab::remove_release(manager_, gitlab_api_, gitlab_tok_,  gitlab_pid_);
      if (!ok) { QVERIFY(false); }
    }
  }

  void initTestCase() {
    TestHandler::initTestCase();

    QByteArray gitlab_api = qgetenv("RELEASE_TEST_GITLAB_API");
    gitlab_api_ = !gitlab_api.isEmpty() ? gitlab_api : "http://localhost:10080/api/v4";

    QByteArray gitlab_tok = qgetenv("RELEASE_TEST_GITLAB_TOK");
    gitlab_tok_ = !gitlab_tok.isEmpty() ? gitlab_tok : "test";

    manager_ = new QNetworkAccessManager(this);

    bool ok;

    { ok = Gitlab::await         (manager_, gitlab_api_, gitlab_tok_);
      if (!ok) { QVERIFY(false); } }

    bool project_exists = false;
    { ok = Gitlab::gather_project(manager_, gitlab_api_, gitlab_tok_, &gitlab_pid_, &project_exists);
      if (!ok) { QVERIFY(false); } }

    if (!project_exists) {
      ok = Gitlab::create_project(manager_, gitlab_api_, gitlab_tok_);
      if (!ok) { QVERIFY(false); }

      ok = Gitlab::gather_project(manager_, gitlab_api_, gitlab_tok_, &gitlab_pid_);
      if (!ok) { QVERIFY(false); }
    }

    bool git_tag_exists = false;
    { ok = Gitlab::gather_git_tag(manager_, gitlab_api_, gitlab_tok_,  gitlab_pid_, &git_tag_exists);
      if (!ok) { QVERIFY(false); } }

    if (!git_tag_exists) {
      ok = Gitlab::create_git_tag(manager_, gitlab_api_, gitlab_tok_,  gitlab_pid_);
      if (!ok) { QVERIFY(false); }

      ok = Gitlab::gather_git_tag(manager_, gitlab_api_, gitlab_tok_,  gitlab_pid_);
      if (!ok) { QVERIFY(false); }
    }

    QVERIFY(true);
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }

 private:
  QString gitlab_api_;
  QString gitlab_pid_;
  QString gitlab_tok_;

  QString title_format_ =             "";
  QString group_format_ = "### %{group}";
  QString entry_format_ =   "- %{entry}";

  QString subject_ =  "%{version}" ;
  QString message_ = "%{changelog}";

 private:
  QNetworkAccessManager* manager_ = nullptr;
};

LVD_TEST_MAIN(ChangelogPublisherGitlabTest)
#include "changelog_publisher_gitlab_test.moc"  // IWYU pragma: keep
