/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QObject>
#include <QSettings>
#include <QSharedPointer>
#include <QString>

#include "formatter.hpp"
#include "handlers/changelog_converter.hpp"
#include "storage.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class ChangelogConverterTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    ChangelogConverter        (section_, storage_, *settings_);
    ChangelogConverter::create(section_, storage_, *settings_, this)->deleteLater();
  }

  void construction_failure() {
    settings_->remove("match_subject");
    settings_->remove("match_message");

    QVERIFY_EXCEPTION_THROWN(ChangelogConverter(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure2() {
    QFETCH(QString, key);
    QFETCH(QString, val);

    if (val.isEmpty()) {
      settings_->remove  (key);
    } else {
      settings_->setValue(key, val);
    }

    QVERIFY_EXCEPTION_THROWN(ChangelogConverter(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure2_data() {
    QTest::addColumn<QString>("key");
    QTest::addColumn<QString>("val");

    QTest::addRow("match_subject invalid") << "match_subject" << "([a-z]+";
    QTest::addRow("match_message invalid") << "match_message" << "([a-z]+";
  }

  // ----------

  void subject() {
    QFETCH(QString, match_subject);
    QFETCH(QString, match_subject_format);

    QFETCH(QString, subject);
    QFETCH(QString, outcome);

    settings_->setValue("match_subject"       , match_subject);
    settings_->setValue("match_subject_format", match_subject_format);

    storage_->add_changelog_entry("Mera", subject, "Hail Rain");
    storage_->add_changelog_entry("Mera", subject, "Hail Rain");

    storage_->add_changelog_entry("Luna", subject, "Hail Rain");
    storage_->add_changelog_entry("Luna", subject, "Hail Rain");

    ChangelogConverter changelog_converter(section_, storage_, *settings_);
    changelog_converter.handle_beyond();

    QCOMPARE(storage_->changelog_entry_subject(0, 0), outcome);
    QCOMPARE(storage_->changelog_entry_subject(0, 1), outcome);

    QCOMPARE(storage_->changelog_entry_subject(1, 0), outcome);
    QCOMPARE(storage_->changelog_entry_subject(1, 1), outcome);
  }

  void subject_data() {
    QTest::addColumn<QString>("match_subject");
    QTest::addColumn<QString>("match_subject_format");

    QTest::addColumn<QString>("subject");
    QTest::addColumn<QString>("outcome");

    QTest::addRow("single") << "Luna"
                            << "Moon"
                            << "Mera Luna"
                            << "Mera Moon";

    QTest::addRow("simple") << "Mera ([a-zA-Z]+)"
                            << "Mera Moon"
                            << "Mera Luna"
                            << "Mera Moon";

    QTest::addRow("invert") << "(?<mera>[a-zA-Z]+) (?<luna>[a-zA-Z]+)"
                            << "%{luna} %{mera}"
                            << "Mera Luna"
                            << "Luna Mera";

    QTest::addRow("immure") << "(?<word>[a-zA-Z]+)"
                            << "[%{word}]"
                            << "Mera Luna"
                            << "[Mera] [Luna]";

    QTest::addRow("immure multiline")         << "(?<word>[a-zA-Z]+)"
                                              << "[%{word}]"
                                              << "Mera Luna\nMoon Star"
                                              << "[Mera] [Luna]\n[Moon] [Star]";

    QTest::addRow("immure multiline initial") << "^(?<word>[a-zA-Z]+)"
                                              << "[%{word}]"
                                              << "Mera Luna\nMoon Star"
                                              << "[Mera] Luna\nMoon Star";

    QTest::addRow("immure multiline newline") << "\n(?<word>[a-zA-Z]+)"
                                              << "\n[%{word}]"
                                              << "Mera Luna\nMoon Star"
                                              << "Mera Luna\n[Moon] Star";
  }

  void message() {
    QFETCH(QString, match_message);
    QFETCH(QString, match_message_format);

    QFETCH(QString, message);
    QFETCH(QString, outcome);

    settings_->setValue("match_message"       , match_message);
    settings_->setValue("match_message_format", match_message_format);

    storage_->add_changelog_entry("Mera", "Hail Rain", message);
    storage_->add_changelog_entry("Mera", "Hail Rain", message);

    storage_->add_changelog_entry("Luna", "Hail Rain", message);
    storage_->add_changelog_entry("Luna", "Hail Rain", message);

    ChangelogConverter changelog_converter(section_, storage_, *settings_);
    changelog_converter.handle_beyond();

    QCOMPARE(storage_->changelog_entry_message(0, 0), outcome);
    QCOMPARE(storage_->changelog_entry_message(0, 1), outcome);

    QCOMPARE(storage_->changelog_entry_message(1, 0), outcome);
    QCOMPARE(storage_->changelog_entry_message(1, 1), outcome);
  }

  void message_data() {
    QTest::addColumn<QString>("match_message");
    QTest::addColumn<QString>("match_message_format");

    QTest::addColumn<QString>("message");
    QTest::addColumn<QString>("outcome");

    QTest::addRow("single") << "Luna"
                            << "Moon"
                            << "Mera Luna"
                            << "Mera Moon";

    QTest::addRow("simple") << "Mera ([a-zA-Z]+)"
                            << "Mera Moon"
                            << "Mera Luna"
                            << "Mera Moon";

    QTest::addRow("invert") << "(?<mera>[a-zA-Z]+) (?<luna>[a-zA-Z]+)"
                            << "%{luna} %{mera}"
                            << "Mera Luna"
                            << "Luna Mera";

    QTest::addRow("immure") << "(?<word>[a-zA-Z]+)"
                            << "[%{word}]"
                            << "Mera Luna"
                            << "[Mera] [Luna]";

    QTest::addRow("immure multiline")         << "(?<word>[a-zA-Z]+)"
                                              << "[%{word}]"
                                              << "Mera Luna\nMoon Star"
                                              << "[Mera] [Luna]\n[Moon] [Star]";

    QTest::addRow("immure multiline initial") << "^(?<word>[a-zA-Z]+)"
                                              << "[%{word}]"
                                              << "Mera Luna\nMoon Star"
                                              << "[Mera] Luna\nMoon Star";

    QTest::addRow("immure multiline newline") << "\n(?<word>[a-zA-Z]+)"
                                              << "\n[%{word}]"
                                              << "Mera Luna\nMoon Star"
                                              << "Mera Luna\n[Moon] Star";
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    settings_->setValue("match_subject",        QString("^major: (?<subject>.+)"));
    settings_->setValue("match_subject_format", QString(         "%{subject}"   ));
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ChangelogConverterTest)
#include "changelog_converter_test.moc"  // IWYU pragma: keep
