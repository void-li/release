#!/bin/sh
set -e -x

exec docker run --detach --hostname gitlab --publish 10080:80 --publish 10022:22 --name release-gitlab registry.gitlab.com/void-li/release-gitlab/release-gitlab:latest
