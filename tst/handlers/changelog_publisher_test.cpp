/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QFile>
#include <QIODevice>
#include <QObject>
#include <QSettings>
#include <QSharedPointer>
#include <QString>

#include "handlers/changelog_publisher.hpp"
#include "storage.hpp"
#include "version.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class ChangelogPublisherTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    ChangelogPublisher        (section_, storage_, *settings_);
    ChangelogPublisher::create(section_, storage_, *settings_, this)->deleteLater();
  }

  void construction_failure() {
    QFETCH(QString, key);
    QFETCH(QString, val);

    if (val.isEmpty()) {
      settings_->remove  (key);
    } else {
      settings_->setValue(key, val);
    }

    QVERIFY_EXCEPTION_THROWN(ChangelogPublisher(section_, storage_, *settings_),
                             std::exception);
  }

  void construction_failure_data() {
    QTest::addColumn<QString>("key");
    QTest::addColumn<QString>("val");

    QTest::addRow("changelog missing")    << "changelog"    << QString();

    QTest::addRow("title_starts missing") << "title_starts" << QString();
    QTest::addRow("title_starts invalid") << "title_starts" << "([a-z]+";
    QTest::addRow("title_inside invalid") << "title_inside" << "([a-z]+";
    QTest::addRow("title_parser invalid") << "title_parser" << "([a-z]+";

    QTest::addRow("title_format missing") << "title_format" << QString();
    QTest::addRow("group_format missing") << "group_format" << QString();
    QTest::addRow("entry_format missing") << "entry_format" << QString();
  }

  // ----------

  void simple_changelog() {
    const QString changelog =
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"\n"
;

    storage_->add_changelog_group("Major Changes");
    storage_->add_changelog_entry("Major Changes",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
  "");

    ChangelogPublisher changelog_publisher(section_, storage_, *settings_);
    changelog_publisher.handle_beyond();

    QString changedat = load_changelog();
    QCOMPARE(changedat, changelog);
  }

  void complex_changelog() {
    const QString changelog =
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"  eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
"  voluptua."                                                               "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"  eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
"  voluptua."                                                               "\n"
""                                                                          "\n"
"  At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
"  gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
"  ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy."     "\n"
""                                                                          "\n"
"### Minor Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"  eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
"  voluptua."                                                               "\n"
""                                                                          "\n"
"  At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
"  gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
"  ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy."     "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"  eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
"  voluptua."                                                               "\n"
""                                                                          "\n"
"  At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
"  gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
"  ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy."     "\n"
""                                                                          "\n"
"### Patches"                                                               "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"\n"
;

    storage_->add_changelog_group("Major Changes");
    storage_->add_changelog_entry("Major Changes",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
  "");

    storage_->add_changelog_entry("Major Changes",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  "eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
  "voluptua.",
  "");

    storage_->add_changelog_entry("Major Changes",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  "eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
  "voluptua.",
  "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
  "gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
  "ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy.");

    storage_->add_changelog_group("Minor Changes");
    storage_->add_changelog_entry("Minor Changes",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  "eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
  "voluptua.",
  "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
  "gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
  "ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy.");

    storage_->add_changelog_entry("Minor Changes",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
  "eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam""\n"
  "voluptua.",
  "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd""\n"
  "gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem""\n"
  "ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy.");

    storage_->add_changelog_group("Patches");
    storage_->add_changelog_entry("Patches",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
  "");

    storage_->add_changelog_entry("Patches",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
  "");

    storage_->add_changelog_entry("Patches",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
  "");

    ChangelogPublisher changelog_publisher(section_, storage_, *settings_);
    changelog_publisher.handle_beyond();

    QString changedat = load_changelog();
    QCOMPARE(changedat, changelog);
  }

  void insert_changelog_after() {
    const QString changelog =
"## Changes in 2.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"\n"
;

    storage_->add_changelog_group("Major Changes");
    storage_->add_changelog_entry("Major Changes",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
  "");

    const QString changetpl =
"## Changes in 2.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"\n"
;

    save_changelog(changetpl);

    ChangelogPublisher changelog_publisher(section_, storage_, *settings_);
    changelog_publisher.handle_beyond();

    QString changedat = load_changelog();
    QCOMPARE(changedat, changelog);
  }

  void insert_changelog_before() {
    const QString changelog =
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"## Changes in 0.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"\n"
;

    storage_->add_changelog_group("Major Changes");
    storage_->add_changelog_entry("Major Changes",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
  "");

    const QString changetpl =
"## Changes in 0.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"\n"
;

    save_changelog(changetpl);

    ChangelogPublisher changelog_publisher(section_, storage_, *settings_);
    changelog_publisher.handle_beyond();

    QString changedat = load_changelog();
    QCOMPARE(changedat, changelog);
  }

  void insert_changelog_between() {
    const QString changelog =
"## Changes in 2.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"## Changes in 0.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"\n"
;

    storage_->add_changelog_group("Major Changes");
    storage_->add_changelog_entry("Major Changes",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
  "");

    const QString changetpl =
"## Changes in 2.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"## Changes in 0.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"\n"
;

    save_changelog(changetpl);

    ChangelogPublisher changelog_publisher(section_, storage_, *settings_);
    changelog_publisher.handle_beyond();

    QString changedat = load_changelog();
    QCOMPARE(changedat, changelog);
  }

  void insert_changelog_override() {
    const QString changelog =
"## Changes in 2.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"## Changes in 0.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"\n"
;

    storage_->add_changelog_group("Major Changes");
    storage_->add_changelog_entry("Major Changes",
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
  "");

    const QString changetpl =
"## Changes in 2.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
""                                                                          "\n"
"## Changes in 1.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam xdummy""\n"
""                                                                          "\n"
"## Changes in 0.0.0"                                                       "\n"
""                                                                          "\n"
"### Major Changes"                                                         "\n"
""                                                                          "\n"
"- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy""\n"
"\n"
;

    save_changelog(changetpl);

    ChangelogPublisher changelog_publisher(section_, storage_, *settings_);
    changelog_publisher.handle_beyond();

    QString changedat = load_changelog();
    QCOMPARE(changedat, changelog);
  }

  // ----------

 private:
  auto load_changelog() -> QString {
    QFile qfile("CHANGELOG.md");
    qfile.open(QFile::ReadOnly);

    QTEST_ASSERT(qfile.isOpen());
    return QString(qfile.readAll());
  }

  void save_changelog(const QString& changelog) {
    QFile qfile("CHANGELOG.md");
    qfile.open(QFile::WriteOnly);

    QTEST_ASSERT(qfile.isOpen());
    qfile.write(changelog.toUtf8());
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    settings_->setValue("changelog", "CHANGELOG.md");

    settings_->setValue("title_starts", "^## ");
    settings_->setValue("title_parser", "^## Changes in (?<major>[0-9]+).(?<minor>[0-9]+).(?<patch>[0-9]+)");

    settings_->setValue("title_format", "## Changes in %{major}.%{minor}.%{patch}");
    settings_->setValue("group_format", "### %{group}");
    settings_->setValue("entry_format", "- %{entry}");

    storage_ ->set_next_version(Version(1, 0, 0,
                                        Version::default_format));
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ChangelogPublisherTest)
#include "changelog_publisher_test.moc"  // IWYU pragma: keep
