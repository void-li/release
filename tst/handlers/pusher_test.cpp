/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QList>
#include <QMetaObject>
#include <QObject>
#include <QSettings>
#include <QSharedPointer>
#include <QString>
#include <QStringList>

#include "handler.hpp"
#include "handlers/pusher.hpp"
#include "storage.hpp"
#include "version.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class PusherTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Pusher        (section_, storage_, *settings_);
    Pusher::create(section_, storage_, *settings_, this)->deleteLater();
  }

  // ----------

  void push() {
    QFETCH(QString, repository);
    QFETCH(QString, references);

    QFETCH(bool, master);
    QFETCH(bool, branch);
    QFETCH(bool, tagged);

    int ret;

    ret = execute("mkdir", "origin");
    QCOMPARE(ret, 0);

    ret = execute("git", "-C", "origin", "init", "--bare");
    QCOMPARE(ret, 0);

    ret = execute("git", "remote", "add", "origin", "origin");
    QCOMPARE(ret, 0);

    ret = execute("git", "push", "-u", "origin", "master");
    QCOMPARE(ret, 0);

    ret = execute("git", "branch", "branch");
    QCOMPARE(ret, 0);

    ret = execute("git", "tag",    "-a", "-m", "Mera Luna", "tagged");
    QCOMPARE(ret, 0);

    if (!alter_file("data0"))
      return;

    ret = execute("git", "commit", "-a", "-m", "Mera Luna");
    QCOMPARE(ret, 0);

    QStringList options;

    if (!repository.isEmpty()) {
      options.append(repository);
    }
    if (!references.isEmpty()) {
      options.append(references);
    }

    settings_->setValue("options", options.join(" "));

    Pusher pusher(section_, storage_, *settings_);

    QSignalSpy spy(&pusher, &Pusher::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      pusher.handle_beyond();
    });

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    if (!compare_master(master)) {
      return;
    }
    if (!compare_branch(branch)) {
      return;
    }
    if (!compare_tagged(tagged)) {
      return;
    }
  }

  void push_data() {
    QTest::addColumn<QString>("repository");
    QTest::addColumn<QString>("references");

    QTest::addColumn<bool>("master");
    QTest::addColumn<bool>("branch");
    QTest::addColumn<bool>("tagged");

    QTest::addRow("default")  << ""       << ""       << true  << false << false;
    QTest::addRow("remotes")  << "origin" << ""       << true  << false << false;

    QTest::addRow("branch1")  << "origin" << "branch" << false << true  << false;
    QTest::addRow("branches") << "origin" << "--all"  << true  << true  << false;

    QTest::addRow("one tag")  << "origin" << "tagged" << false << false << true ;
    QTest::addRow("all tags") << "origin" << "--tags" << false << false << true ;
  }

  void push_failure() {
    int ret;

    ret = execute("git", "remote", "add", "origin", "ssh://localhost:1/repository.git");
    QCOMPARE(ret, 0);

    QStringList options;

    options.append("origin");
    options.append("master");

    settings_->setValue("options", options.join(" "));

    Pusher pusher(section_, storage_, *settings_);

    QSignalSpy spy(&pusher, &Pusher::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      pusher.handle_beyond();
    });

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  // ----------

  void git_repo_removed() {
    TestHandler::remove_git_repo();

    Pusher pusher(section_, storage_, *settings_);

    QSignalSpy spy(&pusher, &Pusher::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      pusher.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_removed() {
    TestHandler::git_util_removed();

    Pusher pusher(section_, storage_, *settings_);

    QSignalSpy spy(&pusher, &Pusher::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      pusher.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_timeout() {
    TestHandler::git_util_timeout();

    QFETCH(QString, timeout);
    settings_->setValue(timeout, 1);

    Pusher pusher(section_, storage_, *settings_);

    QSignalSpy spy(&pusher, &Pusher::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      pusher.handle_beyond();
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void git_util_timeout_data() {
    QTest::addColumn<QString>("timeout");

    QTest::addRow("hard_timeout") << "hard_timeout";
    QTest::addRow("soft_timeout") << "soft_timeout";
  }

  // ----------

 private:
  bool compare_master(bool master) {
    bool success = false;

    [&] {
      if (master) {
        int ret;

        ret = execute("git", "-C", "origin", "show-ref", "heads/master");
        QCOMPARE(ret, 0);

        QVERIFY              (execute_stdout_.split(' ').size() > 0);
        QString remote_hash = execute_stdout_.split(' ')         [0];

        ret = execute("git",                 "show-ref", "heads/master");
        QCOMPARE(ret, 0);

        QVERIFY              (execute_stdout_.split(' ').size() > 0);
        QString  local_hash = execute_stdout_.split(' ')         [0];

        QVERIFY( local_hash == remote_hash );
      } else {
        int ret;

        ret = execute("git", "-C", "origin", "show-ref", "heads/master");
        QCOMPARE(ret, 0);

        QVERIFY              (execute_stdout_.split(' ').size() > 0);
        QString remote_hash = execute_stdout_.split(' ')         [0];

        ret = execute("git",                 "show-ref", "heads/master");
        QCOMPARE(ret, 0);

        QVERIFY              (execute_stdout_.split(' ').size() > 0);
        QString  local_hash = execute_stdout_.split(' ')         [0];

        QVERIFY (local_hash != remote_hash);
      }

      success = true;
    }();

    return success;
  }

  bool compare_branch(bool branch) {
    bool success = false;

    [&] {
      if (branch) {
        int ret;

        ret = execute("git", "-C", "origin", "show-ref", "heads/branch");
        QCOMPARE(ret, 0);

        QVERIFY              (execute_stdout_.split(' ').size() > 0);
        QString remote_hash = execute_stdout_.split(' ')         [0];

        ret = execute("git",                 "show-ref", "heads/branch");
        QCOMPARE(ret, 0);

        QVERIFY              (execute_stdout_.split(' ').size() > 0);
        QString  local_hash = execute_stdout_.split(' ')         [0];

        QVERIFY (local_hash == remote_hash);
      } else {
        int ret;

        ret = execute("git", "-C", "origin", "show-ref", "heads/branch");
        QCOMPARE(ret, 1);
      }

      success = true;
    }();

    return success;
  }

  bool compare_tagged(bool tagged) {
    bool success = false;

    [&] {
      if (tagged) {
        int ret;

        ret = execute("git", "-C", "origin", "show-ref", "-d", "tags/tagged");
        QCOMPARE(ret, 0);

        QVERIFY              (execute_stdout_.split(' ').size() > 2);
        QString remote_hash = execute_stdout_.split(' ')         [2];

        ret = execute("git",                 "show-ref", "-d", "tags/tagged");
        QCOMPARE(ret, 0);

        QVERIFY              (execute_stdout_.split(' ').size() > 2);
        QString  local_hash = execute_stdout_.split(' ')         [2];

        QVERIFY (local_hash == remote_hash);
      } else {
        int ret;

        ret = execute("git", "-C", "origin", "show-ref", "-d", "tags/tagged");
        QCOMPARE(ret, 1);
      }

      success = true;
    }();

    return success;
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    create_git_repo();

    const Version prev_version = Version(1, 2, 3, Version::default_format);
    storage_->set_prev_version(prev_version);

    const Version curr_version = Version(4, 5, 6, Version::default_format);
    storage_->set_curr_version(curr_version);

    const Version next_version = Version(7, 8, 9, Version::default_format);
    storage_->set_next_version(next_version);
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(PusherTest)
#include "pusher_test.moc"  // IWYU pragma: keep
