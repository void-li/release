/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QDir>
#include <QFile>
#include <QFileDevice>
#include <QFlags>
#include <QMetaObject>
#include <QObject>
#include <QProcess>
#include <QString>

#include "handler.hpp"
#include "handlers/verifier.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;
using namespace lvd::release::handlers;

#include "test_handler.hpp"

// ----------

class VerifierTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Verifier        (section_, storage_, *settings_);
    Verifier::create(section_, storage_, *settings_, this)->deleteLater();
  }

  // ----------

  void success_signature() {
    if (!alter_file("data0"))
      return;

    int ret = execute("git", "commit", "-a", "-m", "Mera Luna", "-S");
    QCOMPARE(ret, 0);

    Verifier verifier(section_, storage_, *settings_);

    QSignalSpy spy(&verifier, &Verifier::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      verifier.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY(spy.wait(8192));
    QCOMPARE(spy.size(), 1);
  }

  void failure_signature() {
    if (!alter_file("data0"))
      return;

    int ret = execute("git", "commit", "-a", "-m", "Mera Luna", "-S");
    QCOMPARE(ret, 0);

    QDir qdir("gpg"); qdir.removeRecursively();

    if (!setup_perms(qdir)) {
      return;
    }

    qputenv("GNUPGHOME", qdir.path().toLocal8Bit());

    if (!setup_gnupg()) {
      return;
    }

    Verifier verifier(section_, storage_, *settings_);

    QSignalSpy spy(&verifier, &Verifier::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      verifier.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY(spy.wait(8192));
    QCOMPARE(spy.size(), 1);
  }

  void missing_signature() {
    if (!alter_file("data0"))
      return;

    int ret = execute("git", "commit", "-a", "-m", "Mera Luna");
    QCOMPARE(ret, 0);

    Verifier verifier(section_, storage_, *settings_);

    QSignalSpy spy(&verifier, &Verifier::failure);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      verifier.handle_before();
    }, Qt::QueuedConnection);

    QVERIFY(spy.wait(8192));
    QCOMPARE(spy.size(), 1);
  }

  // ----------

 private:
  bool setup_gnupg() {
    bool success = false;

    [&] {
      QProcess qprocess;
      qprocess.start("gpg", { "--gen-key", "--batch" });

      bool ok = true;

      if (ok) {
        ok = qprocess.waitForStarted();
      }

      if (ok) {
        qprocess.write("%no-protection");
        qprocess.write("\n");

        qprocess.write("Key-Type: default");
        qprocess.write("\n");

        qprocess.write("Key-Length: 1024");
        qprocess.write("\n");

        qprocess.write("Subkey-Type: default");
        qprocess.write("\n");

        qprocess.write("Subkey-Length: 1024");
        qprocess.write("\n");

        qprocess.write("Name-Real: Derp Derpington");
        qprocess.write("\n");

        qprocess.write("Name-Email: derp.derpington@example.org");
        qprocess.write("\n");

        qprocess.write("Expire-Date: 0");
        qprocess.write("\n");

        qprocess.closeWriteChannel();
      }

      if (ok) {
        ok = qprocess.waitForFinished();
      }

      if (QByteArray execute_stdout = qprocess.readAllStandardOutput();
                    !execute_stdout.isEmpty()) {
        if (execute_stdout.endsWith("\n")) {
          execute_stdout.chop(1);
        }

        lvd::qStdOut() << execute_stdout << endl;
      }

      if (QByteArray execute_stderr = qprocess.readAllStandardError ();
                    !execute_stderr.isEmpty()) {
        if (execute_stderr.endsWith("\n")) {
          execute_stderr.chop(1);
        }

        lvd::qStdErr() << execute_stderr << endl;
      }

      QVERIFY(ok);

      success = true;
    }();

    return success;
  }

  bool setup_perms(const QDir& qdir) {
    bool success = false;

    [&] {
      qdir.mkpath(".");
      QVERIFY(qdir.exists());

      QFile qfile(qdir.path());
      qfile.setPermissions(QFileDevice:: ReadOwner
                         | QFileDevice::WriteOwner
                         | QFileDevice::  ExeOwner);

      success = true;
    }();

    return success;
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();

    create_git_repo();

    QDir qdir("gpg");

    if (!setup_perms(qdir)) {
      return;
    }

    qputenv("GNUPGHOME", qdir.path().toLocal8Bit());

    if (!setup_gnupg()) {
      return;
    }
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };

    qunsetenv("GNUPGHOME");
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(VerifierTest)
#include "verifier_test.moc"  // IWYU pragma: keep
