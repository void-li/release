/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>
#include <limits>

#include <QObject>
#include <QString>

#include "version.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;

// ----------

class VersionTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Version version;
    QVERIFY(!version.valid());
  }

  void construction_by_value() {
    QFETCH(quint32, major);
    QFETCH(quint32, minor);
    QFETCH(quint32, patch);
    QFETCH(QString, descr);
    QFETCH(QString, version_format);

    Version version(major, minor, patch,
                    version_format);

    QVERIFY (version.valid());
    QCOMPARE(version.major(), major);
    QCOMPARE(version.minor(), minor);
    QCOMPARE(version.patch(), patch);

    QString  version_descr = version.to_string();
    QCOMPARE(version_descr  , descr);
  }

  void construction_by_value_data() {
    QTest::addColumn<quint32>("major");
    QTest::addColumn<quint32>("minor");
    QTest::addColumn<quint32>("patch");
    QTest::addColumn<QString>("descr");
    QTest::addColumn<QString>("version_format");

    QTest::newRow("v0.0.0") << 0u << 0u << 0u << "v0.0.0" << "v%{major}.%{minor}.%{patch}";
    QTest::newRow("v1.2.3") << 1u << 2u << 3u << "v1.2.3" << "v%{major}.%{minor}.%{patch}";
    QTest::newRow("V3-2-1") << 3u << 2u << 1u << "V3-2-1" << "V%{major}-%{minor}-%{patch}";
  }

  void construction_by_descr() {
    QFETCH(quint32, major);
    QFETCH(quint32, minor);
    QFETCH(quint32, patch);
    QFETCH(QString, descr);
    QFETCH(QString, version_format);

    Version version(descr,
                    version_format);

    QVERIFY (version.valid());
    QCOMPARE(version.major(), major);
    QCOMPARE(version.minor(), minor);
    QCOMPARE(version.patch(), patch);

    QString  version_descr = version.to_string();
    QCOMPARE(version_descr  , descr);
  }

  void construction_by_descr_data() {
    QTest::addColumn<quint32>("major");
    QTest::addColumn<quint32>("minor");
    QTest::addColumn<quint32>("patch");
    QTest::addColumn<QString>("descr");
    QTest::addColumn<QString>("version_format");

    QTest::newRow("v0.0.0") << 0u << 0u << 0u << "v0.0.0" << "v%{major}.%{minor}.%{patch}";
    QTest::newRow("v1.2.3") << 1u << 2u << 3u << "v1.2.3" << "v%{major}.%{minor}.%{patch}";
    QTest::newRow("V3-2-1") << 3u << 2u << 1u << "V3-2-1" << "V%{major}-%{minor}-%{patch}";
  }

  // ----------

  void bump_version() {
    QFETCH(quint32, a);  // major
    QFETCH(quint32, b);  // minor
    QFETCH(quint32, c);  // patch

    Version version0(0, 0, 0,
                     Version::default_format);

    for (quint32 i = 0; i < a; i++) version0.bump_major();
    for (quint32 i = 0; i < b; i++) version0.bump_minor();
    for (quint32 i = 0; i < c; i++) version0.bump_patch();

    Version version1(a, b, c,
                     Version::default_format);

    QCOMPARE(version0            , version1            );
    QCOMPARE(version0.to_string(), version1.to_string());
  }

  void bump_version_data() {
    QTest::addColumn<quint32>("a");
    QTest::addColumn<quint32>("b");
    QTest::addColumn<quint32>("c");

    QTest::addRow("major") << 1u << 0u << 0u;
    QTest::addRow("minor") << 0u << 1u << 0u;
    QTest::addRow("patch") << 0u << 0u << 1u;
  }

  void bump_version_zeroises() {
    QFETCH(quint32, a);  // major
    QFETCH(quint32, b);  // minor
    QFETCH(quint32, c);  // patch

    QFETCH(quint32, x);  // major
    QFETCH(quint32, y);  // minor
    QFETCH(quint32, z);  // patch

    Version version0(1, 1, 1,
                     Version::default_format);

    for (quint32 i = 0; i < a; i++) version0.bump_major();
    for (quint32 i = 0; i < b; i++) version0.bump_minor();
    for (quint32 i = 0; i < c; i++) version0.bump_patch();

    Version version1(x, y, z,
                     Version::default_format);

    QCOMPARE(version0            , version1            );
    QCOMPARE(version0.to_string(), version1.to_string());
  }

  void bump_version_zeroises_data() {
    QTest::addColumn<quint32>("a");
    QTest::addColumn<quint32>("b");
    QTest::addColumn<quint32>("c");

    QTest::addColumn<quint32>("x");
    QTest::addColumn<quint32>("y");
    QTest::addColumn<quint32>("z");

    QTest::addRow("major") << 1u << 0u << 0u << 2u << 0u << 0u;
    QTest::addRow("minor") << 0u << 1u << 0u << 1u << 2u << 0u;
    QTest::addRow("patch") << 0u << 0u << 1u << 1u << 1u << 2u;
  }

  void bump_version_overflow() {
    Version version0(std::numeric_limits<quint32>::max(), 0, 0,
                     Version::default_format);

    QVERIFY_EXCEPTION_THROWN(version0.bump_major(),
                             std::exception);

    Version version1(0, std::numeric_limits<quint32>::max(), 0,
                     Version::default_format);

    QVERIFY_EXCEPTION_THROWN(version1.bump_minor(),
                             std::exception);

    Version version2(0, 0, std::numeric_limits<quint32>::max(),
                     Version::default_format);

    QVERIFY_EXCEPTION_THROWN(version2.bump_patch(),
                             std::exception);
  }

  // ----------

  void equal() {
    Version version0(0, 0, 0,
                     Version::default_format);
    QVERIFY(version0 == version0);

    Version version1(1, 0, 0,
                     Version::default_format);
    QVERIFY(version1 != version0);

    Version version2(0, 1, 0,
                     Version::default_format);
    QVERIFY(version2 != version0);

    Version version3(0, 0, 1,
                     Version::default_format);
    QVERIFY(version3 != version0);
  }

  void lower() {
    Version version0(0, 0, 0,
                     Version::default_format);

    Version version1(1, 0, 0,
                     Version::default_format);

    QVERIFY(version1 >  version0);
    QVERIFY(version1 >= version0);

    Version version2(0, 1, 0,
                     Version::default_format);

    QVERIFY(version2 >  version0);
    QVERIFY(version2 >= version0);

    QVERIFY(version2 <  version1);
    QVERIFY(version2 <= version1);

    Version version3(0, 0, 1,
                     Version::default_format);

    QVERIFY(version3 >  version0);
    QVERIFY(version3 >= version0);

    QVERIFY(version3 <  version2);
    QVERIFY(version3 <= version2);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(VersionTest)
#include "version_test.moc"  // IWYU pragma: keep
