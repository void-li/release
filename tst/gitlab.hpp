/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QNetworkAccessManager>
#include <QString>

// ----------

namespace Gitlab {

bool await         (QNetworkAccessManager* manager,
                    const QString&         gitlab_url,
                    const QString&         gitlab_tok);

bool gather_project(QNetworkAccessManager* manager,
                    const QString&         gitlab_url,
                    const QString&         gitlab_tok,
                          QString*         gitlab_pid     = nullptr,
                          bool*            project_exists = nullptr);

bool create_project(QNetworkAccessManager* manager,
                    const QString&         gitlab_url,
                    const QString&         gitlab_tok);

bool gather_git_tag(QNetworkAccessManager* manager,
                    const QString&         gitlab_url,
                    const QString&         gitlab_tok,
                    const QString&         gitlab_pid,
                          bool*            git_tag_exists = nullptr);

bool create_git_tag(QNetworkAccessManager* manager,
                    const QString&         gitlab_url,
                    const QString&         gitlab_tok,
                    const QString&         gitlab_pid);

bool gather_release(QNetworkAccessManager* manager,
                    const QString&         gitlab_url,
                    const QString&         gitlab_tok,
                    const QString&         gitlab_pid,
                          bool*            release_exists = nullptr,
                          QString*         release_name   = nullptr,
                          QString*         release_desc   = nullptr,
                          QString*         assets_name    = nullptr,
                          QString*         assets_link    = nullptr);

bool remove_release(QNetworkAccessManager* manager,
                    const QString&         gitlab_url,
                    const QString&         gitlab_tok,
                    const QString&         gitlab_pid);

}  // namespace Gitlab
