/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QString>

#include "commit.hpp"

using namespace lvd;
using namespace lvd::release;

// ----------

namespace {
// clazy:excludeall=non-pod-global-static

const QString Commit_Hash = "0a4d55a8d778e5022fab701977c5d840bbc486d0";

const QString Author_Name = "Derp Derpington";
const QString Author_Mail = "derp.derpington@example.org";

const QString Commit_Date = "Mon Jan 7 19:50:33 2019 +0100";

const QString Subject     = "Lorem ipsum dolor sit amet, consetetur"    "\n";

const QString Message     = "sadipscing elitr, sed diam nonumy eirmod"  "\n"
                            "tempor invidunt ut labore et dolore magna" "\n"
                            "aliquyam erat, sed diam voluptua. At vero" "\n"
                                                                        "\n"
                            "eos et accusam et justo duo dolores et ea" "\n"
                            "rebum. Stet clita kasd gubergren, no sea"  "\n"
                            "takimata sanctus est Lorem ipsum dolor"    "\n"
                            "sit amet."                                 "\n";

const QString Pattern     = "commit %1"                                 "\n"
                            "Author: %2 <%3>"                           "\n"
                            "Date:   %4"                                "\n"
                                                                        "\n"
                            "%5"                                        "\n"
                                                                        "\n"
                            "%6"                                        "\n";

}  // namespace

Commit create_commit() {
  QString text = Pattern.arg(Commit_Hash,
                             Author_Name,
                             Author_Mail,
                             Commit_Date,
                    "    " + Subject.trimmed().replace("\n", "\n    "),
                    "    " + Message.trimmed().replace("\n", "\n    "));

  Commit commit(text);
  return commit;
}

Commit create_commit_with_subject(const QString& subject) {
  QString text = Pattern.arg(Commit_Hash,
                             Author_Name,
                             Author_Mail,
                             Commit_Date,
                    "    " + subject.trimmed().replace("\n", "\n    "),
                    "    " + Message.trimmed().replace("\n", "\n    "));

  Commit commit(text);
  return commit;
}

Commit create_commit_with_message(const QString& message) {
  QString text = Pattern.arg(Commit_Hash,
                             Author_Name,
                             Author_Mail,
                             Commit_Date,
                    "    " + Subject.trimmed().replace("\n", "\n    "),
                    "    " + message.trimmed().replace("\n", "\n    "));

  Commit commit(text);
  return commit;
}
