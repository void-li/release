/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QObject>
#include <QString>

#include "formatter.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;

// ----------

class FormatterTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Formatter formatter;
  }

  // ----------

  void mapping() {
    Formatter formatter;
    formatter.append("mera", "luna");
//    formatter.append("moon", "star"); NO!

    QString source = QString("Hello %1 World").arg("%{mera}");
    QString target = QString("Hello %1 World").arg(  "luna" );

    source = formatter.format(source);
    QCOMPARE(source, target);
  }

  void mappings() {
    Formatter formatter;
    formatter.append("mera", "luna");
    formatter.append("moon", "star");

    QString source = QString("Hello %1 World %2").arg("%{mera}", "%{moon}");
    QString target = QString("Hello %1 World %2").arg(  "luna" ,   "star" );

    source = formatter.format(source);
    QCOMPARE(source, target);
  }

  void without_mapping() {
    Formatter formatter;
    formatter.append("mera", "luna");
//    formatter.append("moon", "star"); NO!

    QString source = QString("Hello %1 World %2").arg("%{mera}", "%{moon}");
    QString target = QString("Hello %1 World %2").arg(  "luna" , "%{moon}");

    source = formatter.format(source);
    QCOMPARE(source, target);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(FormatterTest)
#include "formatter_test.moc"  // IWYU pragma: keep
