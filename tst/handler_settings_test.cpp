/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QObject>
#include <QSettings>
#include <QString>

#include "handler.hpp"
#include "handler_settings.hpp"
#include "version.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::release;

#include "handlers/test_handler.hpp"

// ----------

class HandlerSettingsTest : public TestHandler {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void construction() {
    Handler::Settings handler_settings(*settings_);
  }

  // ----------

  void optional_value() {
    settings_->setValue("some_value", Some_Str);

    Handler::Settings handler_settings(*settings_);
    auto some_str = handler_settings.optional<QString>("some_value");

    QCOMPARE(some_str,
             Some_Str);
  }

  void optional_value_with_default_argument() {
//    settings_->setValue("some_value", Some_Str); NO!

    Handler::Settings handler_settings(*settings_);
    auto some_str = handler_settings.optional<QString>("some_value", Some_Str);

    QCOMPARE(some_str,
             Some_Str);
  }

  void optional_value_with_invalid_type() {
    settings_->setValue("some_value", Some_Str);

    Handler::Settings handler_settings(*settings_);

    QVERIFY_EXCEPTION_THROWN(
      handler_settings.optional<Version>("some_value");,
      std::exception);
  }

  void optional_value_missing() {
//    settings_->setValue("some_value", Some_Str); NO!

    Handler::Settings handler_settings(*settings_);
    auto some_str = handler_settings.optional<QString>("some_value");

    QCOMPARE(some_str,
             QString());
  }

  void optional_value_in_environment() {
    QFETCH(QString, source);
    QFETCH(QString, target);

    qputenv("SOME_STR", Some_Str.toLocal8Bit());
    settings_->setValue("some_value", source);

    Handler::Settings handler_settings(*settings_);
    auto some_str = handler_settings.optional<QString>("some_value");

    QCOMPARE(some_str,
             target);
  }

  void optional_value_in_environment_data() {
    QTest::addColumn<QString>("source");
    QTest::addColumn<QString>("target");

    QTest::addRow("simplex") <<  "${SOME_STR}"  << QString( "%1" ).arg(Some_Str);
    QTest::addRow("complex") << "(${SOME_STR})" << QString("(%1)").arg(Some_Str);
  }

  // ----------

  void required_value() {
    settings_->setValue("some_value", Some_Str);

    Handler::Settings handler_settings(*settings_);
    auto some_str = handler_settings.required<QString>("some_value");

    QCOMPARE(some_str,
             Some_Str);
  }

  void required_value_with_default_argument() {
//    settings_->setValue("some_value", Some_Str); NO!

    Handler::Settings handler_settings(*settings_);
    auto some_str = handler_settings.required<QString>("some_value", Some_Str);

    QCOMPARE(some_str,
             Some_Str);
  }

  void required_value_with_invalid_type() {
    settings_->setValue("some_value", Some_Str);

    Handler::Settings handler_settings(*settings_);

    QVERIFY_EXCEPTION_THROWN(
      handler_settings.required<Version>("some_value");,
      std::exception);
  }

  void required_value_missing() {
//    settings_->setValue("some_value", Some_Str); NO!

    Handler::Settings handler_settings(*settings_);

    QVERIFY_EXCEPTION_THROWN(
      handler_settings.required<QString>("some_value");,
      std::exception);
  }

  void required_value_in_environment() {
    QFETCH(QString, source);
    QFETCH(QString, target);

    qputenv("SOME_STR", Some_Str.toLocal8Bit());
    settings_->setValue("some_value", source);

    Handler::Settings handler_settings(*settings_);
    auto some_str = handler_settings.required<QString>("some_value");

    QCOMPARE(some_str,
             target);
  }

  void required_value_in_environment_data() {
    QTest::addColumn<QString>("source");
    QTest::addColumn<QString>("target");

    QTest::addRow("simplex") <<  "${SOME_STR}"  << QString( "%1" ).arg(Some_Str);
    QTest::addRow("complex") << "(${SOME_STR})" << QString("(%1)").arg(Some_Str);
  }

  // ----------

  void excess_values() {
    settings_->setValue("some_value", Some_Str);
    settings_->setValue("othr_value", Some_Int);

    Handler::Settings handler_settings(*settings_);
    auto some_str = handler_settings.optional<QString>("some_value");

    QCOMPARE(some_str,
             Some_Str);
  }

  // ----------

 private slots:
  void init() {
    TestHandler::init();
  }

  void initTestCase() {
    TestHandler::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      TestHandler::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      TestHandler::cleanupTestCase();
    };
  }

 private:
  const QString Some_Str = "Some String";
  const int     Some_Int = 0;
};

LVD_TEST_MAIN(HandlerSettingsTest)
#include "handler_settings_test.moc"  // IWYU pragma: keep
