#!/bin/sh
set -e

# config

origins=${1:-$(realpath $(realpath $(dirname "$0")/..))}
[ -n "$origins" ]; origins=$(realpath "$origins")

project=${2:-$(basename $(realpath $(dirname "$0")/..))}
[ -n "$project" ]

# source

pkgver=

if [ -z "$pkgver" ]; then
  if [ -f "$origins/pkg/PKGBUILD" ]; then
    pkgver=$(grep -oP '^pkgver=(\K.*)' "$origins/pkg/PKGBUILD")
  fi
fi

if [ -z "$pkgver" ]; then
  if [ -d "$origins/.git" ] || [ -d "$origins/../.git" ]; then
    pkgver=$(git -C "$origins" describe --long        2>/dev/null       \
              | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g'                 \
          || git -C "$origins" describe --long --tags 2>/dev/null       \
              | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g'                 \
          || printf "0.0.0.r%s.g%s\n"                                   \
              "$(git -C "$origins" rev-list  --count HEAD 2>/dev/null)" \
              "$(git -C "$origins" rev-parse --short HEAD 2>/dev/null)")
  fi
fi

if [ -z "$pkgver" ]; then
  echo "missing pkgver"
  exit 1
fi

# ----------

tarbase="$project"
tarname="$project-$pkgver.tar.gz"

find "$origins" -mindepth 1 \( -type f -o -type l \) \
    \( -path "$origins/.git/*"  \
    -o -path "$origins/build/*" \
    -o -path "$(pwd)/*"         \
       -prune                   \
    -o -print0 \)               \
  | tar -acf "$tarname" --null -P --transform "s,^${origins//,/\\,},${tarbase//,/\\,}," -T -

# ----------

for f in "PKGBUILD" "PKGBUILD.install"; do
  if [ -f "$origins/pkg/$f" ]; then
    cp "$origins/pkg/$f" .
  fi
done

if [ -f "PKGBUILD" ]; then
  sed -E -i   "s,^pkgver=$,pkgver=${pkgver//,/\\,}," "PKGBUILD"
  sed -E -i "s,^_pkgver=$,_pkgver=${pkgver//,/\\,}," "PKGBUILD"

  updpkgsums
fi
