/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "lvd/core_enum.hpp"

#include <QMetaType>

// ----------

namespace lvd::release {

LVD_ENUM(ExplicitBump, Major, Minor, Patch);

}  // namespace lvd::release

Q_DECLARE_METATYPE(lvd::release::ExplicitBump);
