/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QString>

// ----------

namespace lvd::release {

class Commit {
 public:
  Commit();
  Commit(const QString& data);

  QString author_name() const { return author_name_; }
  QString author_mail() const { return author_mail_; }

  QString commit_date() const { return commit_date_; }

  QString subject() const { return subject_; }
  QString message() const { return message_; }

 private:
  QString author_name_;
  QString author_mail_;

  QString commit_date_;

  QString subject_;
  QString message_;
};

}  // namespace lvd::release
