/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "handler_settings.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <QStringList>

// ----------

namespace {

QString integrate_environment(const QString& qstring) {
  QString outcome = qstring;

  QRegularExpression expression("\\${([a-zA-Z_]+[a-zA-Z0-9_]*)}");
  Q_ASSERT(expression.isValid());

  auto   matches = expression.globalMatch(qstring);
  while (matches.hasNext()) {
    auto match   = matches.next();

    QString match_all = match.captured(0);
    QString match_key = match.captured(1);

    QString replace = qEnvironmentVariable(match_key.toLocal8Bit());
    outcome.replace(match_all, replace);
  }

  return outcome;
}

}  // namespace

// ----------

namespace lvd::release {

Handler::Settings::Settings(QSettings& qsettings)
    : qsettings_(qsettings) {}

Handler::Settings::~Settings() {
  for (QString& key : qsettings_.childKeys()) {
    if (!used_keys_.contains(key)) {
      LVD_LOG_W() << "unused key" << key;
    }
  }
}

// ----------

QVariant Handler::Settings::optional_impl(const QString&  key,
                                          const QVariant& val,
                                          int             tid) {
  QVariant value = qsettings_.value(key, val);

  QString vastr = value.toString();
  if (vastr.contains('$')) {
    value = integrate_environment(vastr);
  }

  if (value.isValid()) {
    if (!value.canConvert(tid)) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << "invalid value for key"
                          << key << value.toString();

      LVD_THROW_RUNTIME(message);
    }
  }

  used_keys_.insert(key);
  return value;
}

QVariant Handler::Settings::required_impl(const QString&  key,
                                          const QVariant& val,
                                          int             tid) {
  QVariant value = qsettings_.value(key, val);

  QString vastr = value.toString();
  if (vastr.contains('$')) {
    value = integrate_environment(vastr);
  }

  if (value.isValid()) {
    if (!value.canConvert(tid)) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << "invalid value for key"
                          << key << value.toString();

      LVD_THROW_RUNTIME(message);
    }
  }
  else {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "missing value for key"
                        << key;

    LVD_THROW_RUNTIME(message);
  }

  used_keys_.insert(key);
  return value;
}

}  // namespace lvd::release
