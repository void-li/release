/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QHash>
#include <QString>

// ----------

namespace lvd::release {

class Formatter {
 public:
  void    append(const QString& key,
                 const QString& val);

  QString format(const QString& str) const;

 private:
  QHash<QString, QString> mappings_;
};

}  // namespace lvd::release
