﻿/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "release.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include "lvd/core_task.hxx"

#include <QByteArray>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QMetaObject>
#include <QSettings>
#include <QSharedPointer>
#include <QStringList>
#include <QStringRef>
#include <QUrl>
#include <QVariant>

#include "lvd/shield.hpp"
#include "lvd/string.hpp"

#include "commit.hpp"
#include "config.hpp"
#include "release_commit_executor.hpp"
#include "release_simple_executor.hpp"
#include "version.hpp"

// ----------

namespace lvd::release {

Release::Release(const QString& config,
                 QObject*       parent)
    : QObject(parent),
      config_(config) {
  LVD_LOG_T();

  LVD_LOG_D() << "config:"
              << config_;
}

Release::~Release() {
  LVD_LOG_T();
}

// ----------

void Release::execute() {
  LVD_LOG_T();
  LVD_SHIELD;

  push_state(&Release::state_finished);
  push_state(&Release::state_beyond);
  push_state(&Release::state_commit);
  push_state(&Release::state_before);
  push_state(&Release::state_settings);

  exec_state();

  LVD_SHIELD_FUN(&Release::failure);
}

// ----------

bool Release::state_settings() {
  LVD_LOG_T();

  QSettings* qsettings = nullptr;

  LVD_FINALLY {
    if (qsettings) {
      qsettings->deleteLater();
      qsettings = nullptr;
    }
  };

  if (QFileInfo::exists(config_)) {
    qsettings = new QSettings(config_, QSettings::IniFormat, this);
    qsettings->sync();
  } else {
    qsettings = new QSettings(                               this);
    qsettings->sync();
  }

  qsettings->setFallbacksEnabled(false);

  bool ok;

  hard_timeout_ = qsettings->value("hard_timeout", config::Release_Hard_Timeout()).toInt(&ok);
  LVD_LOG_D() << "hard_timeout:"
              << hard_timeout_;

  if (hard_timeout_ < 0 || !ok) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "invalid hard timeout"
                        << hard_timeout_;

    on_failure(message);
    return true;
  }

  soft_timeout_ = qsettings->value("soft_timeout", config::Release_Soft_Timeout()).toInt(&ok);
  LVD_LOG_D() << "soft_timeout:"
              << soft_timeout_;

  if (soft_timeout_ < 0 || !ok) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "invalid soft timeout"
                        << soft_timeout_;

    on_failure(message);
    return true;
  }

  QString version_format =  qsettings->value(
      "version_format", Version::default_format).toString();

  LVD_LOG_D() << "version_format"
              << version_format;

  if (version_format.isEmpty()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "missing version format";

    LVD_THROW_RUNTIME(message);
  }

  storage_ = Storage::create();
  storage_->set_version_format(version_format);

  QFile qfile(config_);
  qfile.open(QIODevice::ReadOnly);

  if (!qfile.isOpen()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "cannot read configuration"
                        << config_;

    on_failure(message);
    return true;
  }

  QStringList sections;

  while (!qfile.atEnd()) {
    QByteArray line = qfile.readLine();
    line = strip(line);

    if (   line.startsWith('[')
        && line.  endsWith(']')) {
      line = strip(line, "[]");

      if (line == "lvd_log") {
        continue;
      }

      sections << QUrl::fromPercentEncoding(line);
    }
  }

  for (const QString& section : qAsConst(sections)) {
    QString species;

    int specpos = section.indexOf(':');
    if (specpos >= 0) {
      species = section.left(specpos);
    } else {
      species = section;
    }

    LVD_LOG_D() << "handler"
                << "section" << section
                << "species" << species;

                  qsettings->beginGroup(section);
    LVD_FINALLY { qsettings->  endGroup(); };

    Handler* handler = Handler::create(species,
                                       section,
                                       storage_,
                                       *qsettings,
                                       this);

    if (handler->handles_before()) {
      before_handlers_.append(handler);
    }

    if (handler->handles_commit()) {
      commit_handlers_.append(handler);
    }

    if (handler->handles_beyond()) {
      beyond_handlers_.append(handler);
    }
  }

  return true;
}

bool Release::state_before() {
  LVD_LOG_T();

  if (!before_handlers_.isEmpty()) {
    LVD_LOG_D() << "execute before handlers";

    SimpleExecutor* simple_executor = new SimpleExecutor(
        before_handlers_,
        [] (Handler* handler) {
          return handler->handle_before();
        }, this
    );

    connect(simple_executor, &SimpleExecutor::success,
            this, &Release::on_success);

    connect(simple_executor, &SimpleExecutor::failure,
            this, &Release::on_failure);

    return simple_executor->execute();
  }
  else {
    LVD_LOG_D() << "skip before handlers";
  }

  return true;
}

bool Release::state_commit() {
  LVD_LOG_T();

  if (!commit_handlers_.isEmpty()) {
    LVD_LOG_D() << "execute commit handlers";

    CommitExecutor* commit_executor = new CommitExecutor(
        commit_handlers_,
        [] (Handler* handler, const Commit& commit) {
          return handler->handle_commit(commit);
        }, this
    );

    connect(commit_executor, &CommitExecutor::success,
            this, &Release::on_success);

    connect(commit_executor, &CommitExecutor::failure,
            this, &Release::on_failure);

    if (hard_timeout_ > 0) {
      commit_executor->set_hard_timeout(hard_timeout_);
    }

    if (soft_timeout_ > 0) {
      commit_executor->set_soft_timeout(soft_timeout_);
    }

    return commit_executor->execute(storage_);
  }
  else {
    LVD_LOG_D() << "skip commit handlers";
  }

  return true;
}

bool Release::state_beyond() {
  LVD_LOG_T();

  if (!beyond_handlers_.isEmpty()) {
    LVD_LOG_D() << "execute beyond handlers";

    SimpleExecutor* simple_executor = new SimpleExecutor(
        beyond_handlers_,
        [] (Handler* handler) {
          return handler->handle_beyond();
        }, this
    );

    connect(simple_executor, &SimpleExecutor::success,
            this, &Release::on_success);

    connect(simple_executor, &SimpleExecutor::failure,
            this, &Release::on_failure);

    return simple_executor->execute();
  }
  else {
    LVD_LOG_D() << "skip beyond handlers";
  }

  return true;
}

bool Release::state_finished() {
  LVD_LOG_T();

  emit success();
  return true;
}

// ----------

void Release::on_success(const QString& message) {
  LVD_LOG_T() << LVD_LOGLINE(message);
  LVD_SHIELD;

  QMetaObject::invokeMethod(this, [&] {
    LVD_LOG_T();
    LVD_SHIELD;

//    clear_states(); NO!
    exec_state();

    LVD_SHIELD_FUN(&Release::failure);
  }, Qt::QueuedConnection);

  LVD_SHIELD_FUN(&Release::failure);
}

void Release::on_failure(const QString& message) {
  LVD_LOG_T() << LVD_LOGLINE(message);
  LVD_SHIELD;

  clear_states();
  emit failure(message);

  LVD_SHIELD_FUN(&Release::failure);
}

}  // namespace lvd::release
