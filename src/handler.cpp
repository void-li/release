/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "handler.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QMetaObject>
#include <QStringList>
#include <QVariant>

#include "lvd/shield.hpp"

#include "config.hpp"

// ----------

namespace lvd::release {

Handler::Declarations& Handler::declarations() {
  static Declarations declarations;
  return declarations;
}

// ----------

Handler::Handler        (      Handles     handles,
                         const QString&    section,
                         const StoragePtr& storage,
                               QSettings&  settings,
                               QObject*    parent)
    : QObject(parent),
      handles_(handles),
      section_(section),
      storage_(storage) {
  LVD_LOG_T() << section;

  if (!handles_.valid()) {
    Handles::__type__ handles = 0;

    const QStringList values = settings.value("handles").toStringList();
    for (const QString& value : values) {
      LVD_LOG_D() << section << "handles:" << value;

      handles |= Handles::From_String(value.toLocal8Bit().data());
    }

    if (handles) {
      handles_ = Handles(handles);
    }
  }

  if (!handles_.valid()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section
                        << "invalid handles declaration";

    LVD_THROW_RUNTIME(message);
  }

  bool ok;

  hard_timeout_ = settings.value("hard_timeout", config::Handler_Hard_Timeout()).toInt(&ok);
  LVD_LOG_D() << section << "hard_timeout:" << hard_timeout_;

  if (hard_timeout_ < 0 || !ok) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section
                        << "invalid hard timeout"
                        << hard_timeout_;

    LVD_THROW_RUNTIME(message);
  }

  soft_timeout_ = settings.value("soft_timeout", config::Handler_Soft_Timeout()).toInt(&ok);
  LVD_LOG_D() << section << "soft_timeout:" << soft_timeout_;

  if (soft_timeout_ < 0 || !ok) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section
                        << "invalid soft timeout"
                        << soft_timeout_;

    LVD_THROW_RUNTIME(message);
  }
}

Handler::~Handler() {
  LVD_LOG_T() << section();

  remove_manager();
  remove_process();
}

// ----------

Handler* Handler::create(const QString&    species,
                         const QString&    section,
                         const StoragePtr& storage,
                               QSettings&  settings,
                               QObject*    parent) {
  LVD_LOG_T();

  const Declarations& declarations = Handler::declarations();
  if (!declarations.contains(species)) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "unknown handler"
                        << species;

    LVD_THROW_RUNTIME(message);
  }

  return declarations[species](section, storage, settings, parent);
}

// ----------

Version Handler::make_version() const {
  Version version;

  if (!version.valid()) {
    version = storage()->next_version();
  }
  if (!version.valid()) {
    version = storage()->curr_version();
  }

  return version;
}

// ----------

void Handler::create_process() {
  LVD_LOG_T() << section();

  Q_ASSERT(qprocess_ == nullptr);
  qprocess_ = new QProcess(this);

  connect(qprocess_, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
          this, &Handler::on_finished);

  connect(qprocess_, &QProcess::errorOccurred,
          this, &Handler::on_failure);

  connect(qprocess_, &QProcess::readyReadStandardOutput,
          this, &Handler::on_read_stdout);

  connect(qprocess_, &QProcess::readyReadStandardError,
          this, &Handler::on_read_stderr);
}

void Handler::remove_process() {
  LVD_LOG_T() << section();

  if (qprocess_) {
    qprocess_->disconnect();

    if (qprocess_->state() == QProcess::Starting) {
      qprocess_->waitForStarted();
    }

    if (qprocess_->state() == QProcess::Running) {
      qprocess_->terminate();
      qprocess_->waitForFinished();
    }

    if (qprocess_->state() == QProcess::Running) {
      qprocess_->kill();
      qprocess_->waitForFinished();
    }
  }

  if (qprocess_) {
    qprocess_->deleteLater();
    qprocess_ = nullptr;
  }
}

void Handler::create_hard_timer() {
  LVD_LOG_T() << section();

  remove_hard_timer();
  hard_timer_ = new QTimer(this);

  connect(hard_timer_, &QTimer::timeout,
          this, &Handler::on_hard_timeout);

  Q_ASSERT(hard_timeout_ >= 0);
  if (hard_timeout_ > 0) {
    hard_timer_->setInterval(hard_timeout_);
    hard_timer_->start();
  }
}

void Handler::remove_hard_timer() {
  LVD_LOG_T() << section();

  if (hard_timer_) {
    hard_timer_->deleteLater();
    hard_timer_ = nullptr;
  }
}

void Handler::create_soft_timer() {
  LVD_LOG_T() << section();

  remove_soft_timer();
  soft_timer_ = new QTimer(this);

  connect(soft_timer_, &QTimer::timeout,
          this, &Handler::on_soft_timeout);

  Q_ASSERT(soft_timeout_ >= 0);
  if (soft_timeout_ > 0) {
    soft_timer_->setInterval(soft_timeout_);
    soft_timer_->start();
  }
}

void Handler::remove_soft_timer() {
  LVD_LOG_T() << section();

  if (soft_timer_) {
    soft_timer_->deleteLater();
    soft_timer_ = nullptr;
  }
}

void Handler::notify_soft_timer() {
  LVD_LOG_T() << section();

  if (soft_timer_ && soft_timer_->interval() > 0) {
    soft_timer_->start();
  }
}

void Handler::ignore_soft_timer() {
  LVD_LOG_T() << section();

  if (soft_timer_) {
    soft_timer_->stop();
  }
}

void Handler::create_manager() {
  LVD_LOG_T() << section();

  Q_ASSERT(qmanager_ == nullptr);
  qmanager_ = new QNetworkAccessManager(this);
}

void Handler::remove_manager() {
  LVD_LOG_T() << section();

  if (qmanager_) {
    qmanager_->disconnect();

    qmanager_->deleteLater();
    qmanager_ = nullptr;
  }
}

// ----------

void Handler::on_finished() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  remove_soft_timer();
  remove_hard_timer();

  if (qprocess_) {
    on_read_stdout();
    on_read_stderr();
  }

  QMetaObject::invokeMethod(this, [&] {
    LVD_LOG_T() << section();
    LVD_SHIELD;

    remove_manager();
    remove_process();

    stdout_.clear();
    stderr_.clear();

    LVD_SHIELD_END;
  }, Qt::QueuedConnection);

//  emit success(); NO!
  return;

  LVD_SHIELD_FUN(&Handler::failure);
}

void Handler::on_failure(QProcess::ProcessError process_error) {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  if (process_error == QProcess::FailedToStart) {
    remove_soft_timer();
    remove_hard_timer();

    if (qprocess_) {
      on_read_stdout();
      on_read_stderr();
    }

    QMetaObject::invokeMethod(this, [&] {
      LVD_LOG_T() << section();
      LVD_SHIELD;

      remove_manager();
      remove_process();

      stdout_.clear();
      stderr_.clear();

      LVD_SHIELD_END;
    }, Qt::QueuedConnection);

    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "process failed to start"
                        << qprocess_->program()
                        << qprocess_->arguments();

    emit failure(message);
    return;
  }

  LVD_SHIELD_FUN(&Handler::failure);
}

void Handler::on_read_stdout() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  notify_soft_timer();

  QByteArray message = qprocess_->readAllStandardOutput();
  if (!message.isEmpty()) {
    LVD_LOG_D() << message;
    stdout_.append(message);

    if (stdout_.size() > config::Handler_Stdout_Size_Limit()) {
      stdout_.resize(config::Handler_Stdout_Size_Limit());
    }
  }

  LVD_SHIELD_FUN(&Handler::failure);
}

void Handler::on_read_stderr() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  notify_soft_timer();

  QByteArray message = qprocess_->readAllStandardError();
  if (!message.isEmpty()) {
    LVD_LOG_D() << message;
    stderr_.append(message);

    if (stderr_.size() > config::Handler_Stderr_Size_Limit()) {
      stderr_.resize(config::Handler_Stderr_Size_Limit());
    }
  }

  LVD_SHIELD_FUN(&Handler::failure);
}

void Handler::on_hard_timeout() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  remove_soft_timer();
  remove_hard_timer();

  if (qprocess_) {
    on_read_stdout();
    on_read_stderr();
  }

  QMetaObject::invokeMethod(this, [&] {
    LVD_LOG_T() << section();
    LVD_SHIELD;

    remove_manager();
    remove_process();

    stdout_.clear();
    stderr_.clear();

    LVD_SHIELD_END;
  }, Qt::QueuedConnection);

  if (qprocess_) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "hard timeout after"
                        << hard_timeout_
                        << "milliseconds during"
                        << qprocess_->program()
                        << qprocess_->arguments();

    emit failure(message);
    return;
  } else {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "hard timeout after"
                        << hard_timeout_
                        << "milliseconds";

    emit failure(message);
    return;
  }

  LVD_SHIELD_FUN(&Handler::failure);
}

void Handler::on_soft_timeout() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  remove_soft_timer();
  remove_hard_timer();

  if (qprocess_) {
    on_read_stdout();
    on_read_stderr();
  }

  QMetaObject::invokeMethod(this, [&] {
    LVD_LOG_T() << section();
    LVD_SHIELD;

    remove_manager();
    remove_process();

    stdout_.clear();
    stderr_.clear();

    LVD_SHIELD_END;
  }, Qt::QueuedConnection);

  if (qprocess_) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "soft timeout after"
                        << soft_timeout_
                        << "milliseconds during"
                        << qprocess_->program()
                        << qprocess_->arguments();

    emit failure(message);
    return;
  } else {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "soft timeout after"
                        << soft_timeout_
                        << "milliseconds";

    emit failure(message);
    return;
  }

  LVD_SHIELD_FUN(&Handler::failure);
}

}  // namespace lvd::release
