/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "release_commit_executor.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QStringList>
#include <QStringRef>

#include "lvd/shield.hpp"
#include "lvd/signal.hpp"

#include "version.hpp"

// ----------

namespace lvd::release {

Release::CommitExecutor::CommitExecutor(const Handlers& commit_handlers,
                                        Invoke          invoke,
                                        QObject*        parent)
    : QObject(parent),
      commit_handlers_(commit_handlers),
      invoke_         (invoke) {
  LVD_LOG_T();

  for (Handler* handler : commit_handlers) {
    LVD_LOG_D() << "handler"
                << handler->section();

    handler->setParent(this);

    connect(handler, &Handler::success,
            this, &CommitExecutor::on_handler_success);

    connect(handler, &Handler::failure,
            this, &CommitExecutor::on_handler_failure);
  }
}

Release::CommitExecutor::~CommitExecutor() {
  LVD_LOG_T();

  remove_soft_timer();
  remove_hard_timer();

  LVD_FINALLY {
    remove_process();
  };
}

// ----------

bool Release::CommitExecutor::execute(const StoragePtr& storage) {
  LVD_LOG_T();

  return
  LVD_SHIELD;

  Version prev_version = storage->prev_version();
  if (prev_version.valid()) {
    QString text = prev_version.to_string();

    create_process();
    qprocess_->start("git", { "log", text + "..HEAD" });
  } else {
    create_process();
    qprocess_->start("git", { "log",                 });
  }

  LVD_LOG_D() << "execute"
              << qprocess_->program()
              << qprocess_->arguments();

  create_hard_timer();
  create_soft_timer();

  return false;

  LVD_SHIELD_FUN([&] (const QString& message) {
    emit failure(message);
    return true;
  });
}

// ----------

bool Release::CommitExecutor::setup() {
  LVD_LOG_T();

  Q_ASSERT(queued_handlers_.isEmpty());
  queued_handlers_ = commit_handlers_;

  bool awake = shine();
  if (!awake && !qprocess_suspended_) {
    suspend_process();
  }

  return awake;
}

bool Release::CommitExecutor::shine() {
  LVD_LOG_T();

                asynchronous_ = false;
  LVD_FINALLY { asynchronous_ = true; };

  for (int i = 0; i < queued_handlers_.size() && !failed_; i ++) {
    Handler* handler = queued_handlers_[i];

    bool awake = invoke_(handler, commit_);
    if (!awake) {
      if (queued_handlers_.size() > i + 1) {
        queued_handlers_ = queued_handlers_.mid(i + 1);
        return false;
      }

      queued_handlers_.clear();
      return false;
    }
  }

  queued_handlers_.clear();
  return true;
}

bool Release::CommitExecutor::close() {
  LVD_LOG_T();

  if (queued_handlers_.isEmpty()) {
    bool awake = on_read_stdout();
    if (awake && qprocess_suspended_) {
      proceed_process();
    }

    if (awake && finished_) {
      if (!failed_) {
        emit success();
      }

      return awake;
    }

    return awake;
  }

  bool awake = shine();
  if (!awake && !qprocess_suspended_) {
    suspend_process();
  }

  return awake;
}

// ----------

void Release::CommitExecutor::create_process() {
  LVD_LOG_T();

  Q_ASSERT(qprocess_ == nullptr);

  remove_process();
  qprocess_ = new QProcess(this);

  connect(qprocess_, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
          this, &Release::CommitExecutor::on_finished);

  connect(qprocess_, &QProcess::errorOccurred,
          this, &Release::CommitExecutor::on_failure);

  connect(qprocess_, &QProcess::readyReadStandardOutput,
          this, &Release::CommitExecutor::on_read_stdout);

  connect(qprocess_, &QProcess::readyReadStandardError,
          this, &Release::CommitExecutor::on_read_stderr);
}

void Release::CommitExecutor::remove_process() {
  LVD_LOG_T();

  if (qprocess_) {
    qprocess_->disconnect();

    if (qprocess_->state() == QProcess::Starting) {
      qprocess_->waitForStarted();
    }

    if (qprocess_->state() == QProcess::Running) {
      qprocess_->terminate();
      qprocess_->waitForFinished();
    }

    if (qprocess_->state() == QProcess::Running) {
      qprocess_->kill();
      qprocess_->waitForFinished();
    }
  }

  if (qprocess_) {
    qprocess_->deleteLater();
    qprocess_ = nullptr;
  }
}

void Release::CommitExecutor::create_hard_timer() {
  LVD_LOG_T();

  Q_ASSERT(hard_timer_ == nullptr);

  remove_hard_timer();
  hard_timer_ = new QTimer(this);

  connect(hard_timer_, &QTimer::timeout,
          this, &Release::CommitExecutor::on_hard_timeout);

  Q_ASSERT(hard_timeout_ >= 0);
  if (hard_timeout_ > 0) {
    hard_timer_->setInterval(hard_timeout_);
    hard_timer_->start();
  }
}

void Release::CommitExecutor::remove_hard_timer() {
  LVD_LOG_T();

  if (hard_timer_) {
    hard_timer_->deleteLater();
    hard_timer_ = nullptr;
  }
}

void Release::CommitExecutor::create_soft_timer() {
  LVD_LOG_T();

  Q_ASSERT(soft_timer_ == nullptr);

  remove_soft_timer();
  soft_timer_ = new QTimer(this);

  connect(soft_timer_, &QTimer::timeout,
          this, &Release::CommitExecutor::on_soft_timeout);

  Q_ASSERT(soft_timeout_ >= 0);
  if (soft_timeout_ > 0) {
    soft_timer_->setInterval(soft_timeout_);
    soft_timer_->start();
  }
}

void Release::CommitExecutor::remove_soft_timer() {
  LVD_LOG_T();

  if (soft_timer_) {
    soft_timer_->deleteLater();
    soft_timer_ = nullptr;
  }
}

void Release::CommitExecutor::notify_soft_timer() {
  LVD_LOG_T();

  if (soft_timer_ && soft_timer_->interval() > 0) {
    soft_timer_->start();
  }
}

void Release::CommitExecutor::ignore_soft_timer() {
  LVD_LOG_T();

  if (soft_timer_) {
    soft_timer_->stop();
  }
}

void Release::CommitExecutor::suspend_process() {
  LVD_LOG_T();

  if (qprocess_ && !qprocess_suspended_) {
    if (qprocess_->state() == QProcess::Running) {
      qprocess_->blockSignals(true);

      LVD_LOG_D() << "suspending git log";

      kill(static_cast<pid_t>(qprocess_->processId()), SIGSTOP);
      qprocess_suspended_ = true;

      ignore_soft_timer();
    }
  }
}

void Release::CommitExecutor::proceed_process() {
  LVD_LOG_T();

  if (qprocess_ &&  qprocess_suspended_) {
    if (qprocess_->state() == QProcess::Running || true) {
      qprocess_->blockSignals(false);

      LVD_LOG_D() << "proceeding git log";

      kill(static_cast<pid_t>(qprocess_->processId()), SIGCONT);
      qprocess_suspended_ = false;

      notify_soft_timer();
    }
  }
}

// ----------

void Release::CommitExecutor::on_handler_success(const QString& message) {
  LVD_LOG_T() << LVD_LOGLINE(message);
  LVD_SHIELD;

  if (asynchronous_) {
    close();
    return;
  }

  LVD_SHIELD_FUN(&CommitExecutor::failure);
}

void Release::CommitExecutor::on_handler_failure(const QString& message) {
  LVD_LOG_T() << LVD_LOGLINE(message);
  LVD_SHIELD;

  failed_ = true;

  emit failure(message);
  return;

  LVD_SHIELD_FUN(&CommitExecutor::failure);
}

void Release::CommitExecutor::on_finished() {
  LVD_LOG_T();
  LVD_SHIELD;

  remove_soft_timer();
  remove_hard_timer();
  //  remove_process(); NO!

  if (!finished_) {
    finished_ = true;

    if (qprocess_) {
      LVD_LOG_D() << "process result"
                  << qprocess_->program()
                  << qprocess_->arguments()
                  << qprocess_->exitStatus()
                  << qprocess_->exitCode();

      if (   qprocess_->exitStatus() == QProcess::NormalExit
          && qprocess_->exitCode()   == 0) {
        close();
        return;
      }

      LVD_LOGBUF message;
      LVD_LOG_W(&message) << "process failed"
                          << qprocess_->program()
                          << qprocess_->arguments()
                          << qprocess_->exitStatus()
                          << qprocess_->exitCode();

      emit failure(message);
      return;
    }

    LVD_THROW_IMPOSSIBLE;
  }

  LVD_SHIELD_FUN(&CommitExecutor::failure);
}

void Release::CommitExecutor::on_failure(QProcess::ProcessError process_error) {
  LVD_LOG_T();
  LVD_SHIELD;

  if (process_error == QProcess::FailedToStart) {
    remove_soft_timer();
    remove_hard_timer();

    LVD_FINALLY {
      remove_process();
    };

    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "process failed to start"
                        << qprocess_->program()
                        << qprocess_->arguments();

    emit failure(message);
    return;
  }

  LVD_SHIELD_FUN(&CommitExecutor::failure);
}

bool Release::CommitExecutor::on_read_stdout() {
  LVD_LOG_T();

  return
  LVD_SHIELD;

  notify_soft_timer();
  qprocess_->setReadChannel(QProcess::StandardOutput);

  while (qprocess_->canReadLine()) {
    QByteArray line = qprocess_->readLine();

    LVD_FINALLY {
      commit_data_.append(line);
    };

    if (line.startsWith("commit ")) {
      LVD_LOG_D() << "starting commit";

      if (!commit_data_.isEmpty()) {
        commit_ = Commit(commit_data_);
        commit_data_.clear();

        LVD_LOG_D() << "handling commit";

        if (!setup()) {
          return false;
        }
      }
    }
  }

  if (qprocess_->state() != QProcess::Running) {
    if (!commit_data_.isEmpty()) {
      commit_ = Commit(commit_data_);
      commit_data_.clear();

      LVD_LOG_D() << "handling commit";

      if (!setup()) {
        return false;
      }
    }
  }

  return true;

  LVD_SHIELD_FUN([&] (const QString& message) {
    emit failure(message);
    return true;
  });
}

void Release::CommitExecutor::on_read_stderr() {
  LVD_LOG_T();
  LVD_SHIELD;

  notify_soft_timer();
  qprocess_->setReadChannel(QProcess::StandardError );

  while (qprocess_->canReadLine()) {
    QByteArray line = qprocess_->readLine();
    qStdErr() << line;
  }

  LVD_SHIELD_FUN(&CommitExecutor::failure);
}

void Release::CommitExecutor::on_hard_timeout() {
  LVD_LOG_T();
  LVD_SHIELD;

  remove_soft_timer();
  remove_hard_timer();

  LVD_FINALLY {
    remove_process();
  };

  LVD_LOGBUF message;
  LVD_LOG_W(&message) << "hard timeout after"
                      << hard_timeout_
                      << "milliseconds during"
                      << qprocess_->program()
                      << qprocess_->arguments();

  emit failure(message);
  return;

  LVD_SHIELD_FUN(&CommitExecutor::failure);
}

void Release::CommitExecutor::on_soft_timeout() {
  LVD_LOG_T();
  LVD_SHIELD;

  remove_soft_timer();
  remove_hard_timer();

  LVD_FINALLY {
    remove_process();
  };

  LVD_LOGBUF message;
  LVD_LOG_W(&message) << "soft timeout after"
                      << soft_timeout_
                      << "milliseconds during"
                      << qprocess_->program()
                      << qprocess_->arguments();

  emit failure(message);
  return;

  LVD_SHIELD_FUN(&CommitExecutor::failure);
}

}  // namespace lvd::release
