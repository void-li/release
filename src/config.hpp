/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QString>

// ----------

namespace lvd::release::config {

QString App_Name();
QString App_Vers();

QString Org_Name();
QString Org_Addr();

// ----------

int     Release_Hard_Timeout();
int     Release_Soft_Timeout();

int     Handler_Hard_Timeout();
int     Handler_Soft_Timeout();

int     Handler_Stdout_Size_Limit();
int     Handler_Stderr_Size_Limit();

}  // namespace lvd::release::config
