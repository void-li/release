/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QtGlobal>

#include <QMetaType>
#include <QString>

#include "lvd/logger.hpp"

// ----------

namespace lvd::release {

class Version {
  LVD_LOGGER

 public:
  Version();

  Version(      quint32  major,
                quint32  minor,
                quint32  patch,
          const QString& version_format);

  Version(const QString& descr,
          const QString& version_format);

 public:
  bool valid() const {
    return valid_;
  }

  quint32 major() const {
    assure_value();
    return major_;
  }
  void bump_major();

  quint32 minor() const {
    assure_value();
    return minor_;
  }
  void bump_minor();

  quint32 patch() const {
    assure_value();
    return patch_;
  }
  void bump_patch();

  QString to_string() const {
    assure_descr();
    return descr_;
  }

 private:
  void bump(quint32& src);

  void assure_value() const;
  void remove_value() { value_alive_ = false; }

  void assure_descr() const;
  void remove_descr() { descr_alive_ = false; }

 public:
  static const QString default_format;

 private:
  bool valid_ = false;

  mutable bool    value_alive_ = false;
  mutable quint32 major_ = 0;
  mutable quint32 minor_ = 0;
  mutable quint32 patch_ = 0;

  mutable bool    descr_alive_ = false;
  mutable QString descr_;

  QString version_format_;
};

// ----------

bool operator==(const Version& lhs, const Version& rhs);
inline
bool operator!=(const Version& lhs, const Version& rhs) {
  return !(lhs == rhs);
}

bool operator< (const Version& lhs, const Version& rhs);
inline
bool operator<=(const Version& lhs, const Version& rhs) {
  return   lhs <  rhs || lhs == rhs;
}

inline
bool operator> (const Version& lhs, const Version& rhs) {
  return   rhs <  lhs;
}

inline
bool operator>=(const Version& lhs, const Version& rhs) {
  return   rhs <= lhs;
}

}  // namespace lvd::release

Q_DECLARE_METATYPE(lvd::release::Version);
