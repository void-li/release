/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <functional>

#include <QByteArray>
#include <QObject>
#include <QProcess>
#include <QString>
#include <QTimer>

#include "lvd/logger.hpp"

#include "commit.hpp"
#include "handler.hpp"
#include "release.hpp"
#include "storage.hpp"

// ----------

namespace lvd::release {

class Release::CommitExecutor : public QObject {
  Q_OBJECT LVD_LOGGER

 public:
  using Invoke = std::function<bool(Handler*, const Commit& commit)>;

 public:
  CommitExecutor(const Handlers& commit_handlers,
                 Invoke          invoke,
                 QObject*        parent = nullptr);

  ~CommitExecutor() override;

 public slots:
  bool execute(const StoragePtr& storage);

 public:
  int hard_timeout() const { return hard_timeout_; }
  void set_hard_timeout(int hard_timeout) {
    hard_timeout_ = hard_timeout;
  }

  int soft_timeout() const { return soft_timeout_; }
  void set_soft_timeout(int soft_timeout) {
    soft_timeout_ = soft_timeout;
  }

 signals:
  void success(const QString& message = QString());
  void failure(const QString& message = QString());

 private:
  bool setup();
  bool shine();
  bool close();

 private:
  void create_process();
  void remove_process();

  void create_hard_timer();
  void remove_hard_timer();

  void create_soft_timer();
  void remove_soft_timer();

  void notify_soft_timer();
  void ignore_soft_timer();

  void suspend_process();
  void proceed_process();

 private slots:
  void on_handler_success(const QString& message);
  void on_handler_failure(const QString& message);

  void on_finished();
  void on_failure(QProcess::ProcessError process_error);

  bool on_read_stdout();
  void on_read_stderr();

  void on_hard_timeout();
  void on_soft_timeout();

 private:
  Handlers commit_handlers_;
  Handlers queued_handlers_;

  bool asynchronous_ = false;
  bool failed_       = false;

  Invoke invoke_;

 private:
  QProcess* qprocess_ = nullptr;
  bool      qprocess_suspended_ = false;

  QTimer* hard_timer_ = nullptr;
  int     hard_timeout_ = -1;

  QTimer* soft_timer_ = nullptr;
  int     soft_timeout_ = -1;

  Commit     commit_;
  QByteArray commit_data_;

  bool finished_ = false;
};

}  // namespace lvd::release
