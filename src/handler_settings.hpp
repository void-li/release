/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QtGlobal>

#include <QMetaType>
#include <QSet>
#include <QSettings>
#include <QString>
#include <QVariant>

#include "lvd/logger.hpp"

#include "handler.hpp"

// ----------

namespace lvd::release {

class Handler::Settings {
  LVD_LOGGER

 public:
  Settings(QSettings& qsettings);
  ~Settings();

 public:
  template <typename T>
  T optional(const QString& key) {
    QVariant value = optional_impl(key, QVariant(),
                                   qMetaTypeId<T>());

    Q_ASSERT(value.canConvert<T>() || value.isNull());
    return value.value<T>();
  }

  template <typename T>
  T optional(const QString& key,
             const T      & val) {
    QVariant value = optional_impl(key, QVariant::fromValue<T>(val),
                                   qMetaTypeId<T>());

    Q_ASSERT(value.canConvert<T>());
    return value.value<T>();
  }

  template <typename T>
  T required(const QString& key) {
    QVariant value = required_impl(key, QVariant(),
                                   qMetaTypeId<T>());

    Q_ASSERT(value.canConvert<T>());
    return value.value<T>();
  }

  template <typename T>
  T required(const QString& key,
             const T      & val) {
    QVariant value = required_impl(key, QVariant::fromValue<T>(val),
                                   qMetaTypeId<T>());

    Q_ASSERT(value.canConvert<T>());
    return value.value<T>();
  }

 private:
  QVariant optional_impl(const QString&  key,
                         const QVariant& val,
                         int             tid);

  QVariant required_impl(const QString&  key,
                         const QVariant& val,
                         int             tid);

 private:
  QSettings   & qsettings_;
  QSet<QString> used_keys_;
};

}  // namespace lvd::release
