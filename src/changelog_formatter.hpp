/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QString>

#include "storage.hpp"
#include "version.hpp"

// ----------

namespace lvd::release {

QString changelog_formatter(const Storage* storage,
                            const Version& version,
                            const QString& title_format,
                            const QString& group_format,
                            const QString& entry_format);

}  // namespace lvd::release
