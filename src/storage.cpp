/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "storage.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <algorithm>

// ----------

namespace lvd::release {

Storage::Storage(QObject* parent)
    : QObject(parent) {}

// ----------

void Storage::bump_explicit(ExplicitBump explicit_bump) {
  Q_ASSERT(explicit_bump.valid());

  if (  !explicit_bump_.valid()
      || explicit_bump_ > explicit_bump) {
    set_explicit_bump(explicit_bump);
  }
}

void Storage::bump_implicit(ImplicitBump implicit_bump) {
  Q_ASSERT(implicit_bump.valid());

  if (  !implicit_bump_.valid()
      || implicit_bump_ > implicit_bump) {
    set_implicit_bump(implicit_bump);
  }
}

// ----------

void Storage::add_changelog_group(const QString& grptext) {
  auto clg = std::find_if(changelog_.begin(),
                          changelog_.end(),
  [&grptext] (const ChangelogGroup& grp) {
    return grp.first == grptext;
  });

  if (clg == changelog_.end()) {
    changelog_ << ChangelogGroup (grptext, {});
//    clg = changelog_.end() - 1; NO!
  }
}

void Storage::add_changelog_entry(const QString& grptext,
                                  const QString& subject,
                                  const QString& message) {
  auto clg = std::find_if(changelog_.begin(),
                          changelog_.end(),
  [&grptext] (const ChangelogGroup& grp) {
    return grp.first == grptext;
  });

  if (clg == changelog_.end()) {
    changelog_ << ChangelogGroup (grptext, {});
    clg = changelog_.end() - 1;
  }

  if (clg != changelog_.end()) {
    clg->second << ChangelogEntry(subject, message);
  }
}

void Storage::set_changelog_entry(int group , int entry ,
                                  const QString& subject,
                                  const QString& message) {
  Q_ASSERT(group < changelog_              .size());
  Q_ASSERT(entry < changelog_[group].second.size());

  changelog_[group].second[entry].first  = subject;
  changelog_[group].second[entry].second = message;
}

}  // namespace lvd::release
