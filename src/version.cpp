/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "version.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include "formatter.hpp"

// ----------

namespace lvd::release {

Version::Version() = default;

Version::Version(      quint32  major,
                       quint32  minor,
                       quint32  patch,
                 const QString& version_format)
    : valid_(true ),
      major_(major),
      minor_(minor),
      patch_(patch) {
  value_alive_ = true;
  version_format_ = version_format;
}

Version::Version(const QString& descr,
                 const QString& version_format)
    : valid_(true ),
      descr_(descr) {
  descr_alive_ = true;
  version_format_ = version_format;
}

// ----------

void Version::bump_major() {
  assure_value();
  bump(major_);

  (void)(/*major_ =*/   minor_ =     patch_ =   0);
  remove_descr();
}

void Version::bump_minor() {
  assure_value();
  bump(minor_);

  (void)(/*major_ =*/ /*minor_ =*/   patch_ =   0);
  remove_descr();
}

void Version::bump_patch() {
  assure_value();
  bump(patch_);

  (void)(/*major_ =*/ /*minor_ =*/ /*patch_ =*/ 0);
  remove_descr();
}

// ----------

void Version::bump(quint32& src) {
  quint32 dst;

  bool overflow = __builtin_add_overflow(src, 1, &dst);
  if (overflow) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "version overflow";

    LVD_THROW_RUNTIME(message);
  }

  src = dst;
}

// ----------

void Version::assure_value() const {
  if (!value_alive_) {
    Q_ASSERT(descr_alive_);

    auto restr = QRegularExpression::escape(version_format_);
    if (restr.isEmpty()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << "missing version format";

      LVD_THROW_RUNTIME(message);
    }

    restr.replace("\\%\\{major\\}", "(?<major>[0-9]+)")
         .replace("\\%\\{minor\\}", "(?<minor>[0-9]+)")
         .replace("\\%\\{patch\\}", "(?<patch>[0-9]+)");

    auto rexpr = QRegularExpression(restr);
    if (!rexpr.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << "invalid version format"
                          << version_format_
                          << rexpr.errorString();

      LVD_THROW_RUNTIME(message);
    }

    auto match = rexpr.match(descr_);
    if (!match.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << "invalid version format"
                          << version_format_
                          << "version" << descr_;

      LVD_THROW_RUNTIME(message);
    }

    bool ok;

    major_ = match.captured("major").toUInt(&ok);
    Q_ASSERT(ok);

    minor_ = match.captured("minor").toUInt(&ok);
    Q_ASSERT(ok);

    patch_ = match.captured("patch").toUInt(&ok);
    Q_ASSERT(ok);

    value_alive_ = true;
  }
}

void Version::assure_descr() const {
  if (!descr_alive_) {
    Q_ASSERT(value_alive_);

    Formatter formatter;
    formatter.append("major", QString::number(major_));
    formatter.append("minor", QString::number(minor_));
    formatter.append("patch", QString::number(patch_));

    descr_ = formatter.format(version_format_);
    descr_alive_ = true;
  }
}

// ----------

const QString Version::default_format = "v%{major}.%{minor}.%{patch}";

// ----------

bool operator==(const Version& lhs, const Version& rhs) {
  return lhs.major() == rhs.major()
      && lhs.minor() == rhs.minor()
      && lhs.patch() == rhs.patch();
}

bool operator< (const Version& lhs, const Version& rhs) {
  return lhs.major() <  rhs.major()
      || lhs.minor() <  rhs.minor()
      || lhs.patch() <  rhs.patch();
}

}  // namespace lvd::release
