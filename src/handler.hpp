/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "lvd/core.hpp"
#include "lvd/core_enum.hpp"

#include <functional>

#include <QByteArray>
#include <QHash>
#include <QNetworkAccessManager>
#include <QObject>
#include <QProcess>
#include <QSettings>
#include <QString>
#include <QTimer>
#include <QVector>

#include "lvd/logger.hpp"

#include "commit.hpp"
#include "storage.hpp"
#include "version.hpp"

// ----------

namespace lvd::release {

class Handler;
using Handlers = QVector<Handler*>;

class Handler : public QObject {
  Q_OBJECT LVD_LOGGER

 public:
  LVD_ENUM(Handles,
           Before = 0x01,
           Commit = 0x02,
           Beyond = 0x04);

 public:
  class Settings;

 private:
  using Declaration  =                std::function<Handler*(
      const QString&, const StoragePtr&, QSettings&, QObject*)> ;

  using Declarations = QHash<QString, std::function<Handler*(
      const QString&, const StoragePtr&, QSettings&, QObject*)>>;

  static Declarations& declarations();

 public:
  template <class T>
  static bool declare(const QString& species) {
    declarations()[species] = &T::create;
    return true;
  }

 protected:
  Handler        (      Handles     handles,
                  const QString&    section,
                  const StoragePtr& storage,
                        QSettings&  settings,
                        QObject*    parent = nullptr);

 public:
  ~Handler() override;

  static
  Handler* create(const QString&    species,
                  const QString&    section,
                  const StoragePtr& storage,
                        QSettings&  settings,
                        QObject*    parent = nullptr);

 public:
  bool handle_before() {
    Q_ASSERT(handles_before());
    return handle_before_impl();
  }
  bool handles_before() const {
    return handles_.valid()
        && handles_ & Handles::Before;
  }

  bool handle_commit(const Commit& commit) {
    Q_ASSERT(handles_commit());
    return handle_commit_impl(commit);
  }
  bool handles_commit() const {
    return handles_.valid()
        && handles_ & Handles::Commit;
  }

  bool handle_beyond() {
    Q_ASSERT(handles_beyond());
    return handle_beyond_impl();
  }
  bool handles_beyond() const {
    return handles_.valid()
        && handles_ & Handles::Beyond;
  }

 public:
  const QString section() const {
    return section_;
  }

 signals:
  void success(const QString& message = QString());
  void failure(const QString& message = QString());

 protected:
  virtual bool handle_before_impl()              { return true; }
  virtual bool handle_commit_impl(const Commit&) { return true; }
  virtual bool handle_beyond_impl()              { return true; }

 protected:
  Version make_version() const;

  Storage* storage() const {
    return storage_.get();
  }

 protected:
  void create_process();
  void remove_process();

  void create_hard_timer();
  void remove_hard_timer();

  void create_soft_timer();
  void remove_soft_timer();

  void notify_soft_timer();
  void ignore_soft_timer();

  void create_manager();
  void remove_manager();

 protected slots:
  virtual void on_finished();
  virtual void on_failure(QProcess::ProcessError process_error);

  virtual void on_read_stdout();
  virtual void on_read_stderr();

  virtual void on_hard_timeout();
  virtual void on_soft_timeout();

 protected:
  QProcess* qprocess_ = nullptr;

  QByteArray stdout_;
  QByteArray stderr_;

  QTimer* hard_timer_ = nullptr;
  int     hard_timeout_ = -1;

  QTimer* soft_timer_ = nullptr;
  int     soft_timeout_ = -1;

 protected:
  QNetworkAccessManager* qmanager_ = nullptr;

 private:
  Handles handles_;
  QString section_;

  StoragePtr storage_;
};

#ifndef DECLARE_HANDLER
#define DECLARE_HANDLER(Type, Name) \
  inline const bool Name ## __ ## Decl = lvd::release::Handler::declare<Type>(#Name);
#endif

}  // namespace lvd::release
