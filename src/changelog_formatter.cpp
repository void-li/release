/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "changelog_formatter.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include "formatter.hpp"

// ----------

namespace {

using namespace lvd;
using namespace lvd::release;

const char* newline = "\n\n";

void append_title(const Storage* storage,
                  const Version& version,
                  const QString& title_format,
                        QString& changelog) {
  Q_UNUSED(storage)

  if (!title_format.isEmpty()) {
    Formatter formatter;
    formatter.append("major", QString::number(version.major()));
    formatter.append("minor", QString::number(version.minor()));
    formatter.append("patch", QString::number(version.patch()));
    formatter.append("version",               version.to_string());

    QString tbuffer = formatter.format(title_format);

    changelog.append(tbuffer);
    changelog.append(newline);
  }
}

void append_group(const Storage* storage,
                  const Version& version,
                  const QString& group_format,
                        QString& changelog,
                  int            i) {
  Q_UNUSED(version)

  if (!group_format.isEmpty()) {
    QString group = storage->changelog_group(i);

    Formatter formatter;
    formatter.append("group", group);

    QString gbuffer = formatter.format(group_format);

    changelog.append(gbuffer);
    changelog.append(newline);
  }
}

}  // namespace

// ----------

namespace lvd::release {

QString changelog_formatter(const Storage* storage,
                            const Version& version,
                            const QString& title_format,
                            const QString& group_format,
                            const QString& entry_format) {
  QString changelog;

  bool title_appended = false;

  for (int i = 0; i < storage->changelog_size(); i++) {
    bool group_appended = false;

    for (int j = 0; j < storage->changelog_group_size(i); j++) {
      QString entry_subject = storage->changelog_entry_subject(i, j);
      QString entry_message = storage->changelog_entry_message(i, j);

      int entry_index = entry_format.indexOf("%{entry}");
      if (entry_index < 0) {
        entry_index = 0;
      }

      QString space = QString(" ").repeated(entry_index);
      QString entry;

      if (!entry_subject.isEmpty()) {
        if (!entry.isEmpty()) {
          entry.append(newline);
          entry.append( space );
        }

        entry_subject.replace("\n", "\n" + space);
        entry.append(entry_subject);
      }

      if (!entry_message.isEmpty()) {
        if (!entry.isEmpty()) {
          entry.append(newline);
          entry.append( space );
        }

        entry_message.replace("\n", "\n" + space);
        entry.append(entry_message);
      }

      if (!entry_format.isEmpty() && !entry.isEmpty()) {
        Formatter formatter;
        formatter.append("entry", entry);

        if (!title_appended) {
          append_title(storage, version, title_format, changelog   );
          title_appended = true;
        }

        if (!group_appended) {
          append_group(storage, version, group_format, changelog, i);
          group_appended = true;
        }

        QString ebuffer = formatter.format(entry_format);

        changelog.append(ebuffer);
        changelog.append(newline);
      }
    }
  }

  return changelog;
}

}  // namespace lvd::release
