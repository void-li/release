find_package(Qt5 REQUIRED COMPONENTS Core Network)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

# ----------

if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp")
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp"
    "${CMAKE_CURRENT_BINARY_DIR}/config.cpp" @ONLY
  )
else()
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp.in"
    "${CMAKE_CURRENT_BINARY_DIR}/config.cpp" @ONLY
  )
endif()

# ----------

add_library(          release_a STATIC
  changelog_formatter.cpp
  changelog_formatter.hpp
  commit.cpp
  commit.hpp
  config.cpp.in "${CMAKE_CURRENT_BINARY_DIR}/config.cpp"
  config.hpp
  explicit_bump.hpp
  formatter.cpp
  formatter.hpp
  handler.cpp
  handler.hpp
  handler_settings.cpp
  handler_settings.hpp
  handlers/changelog_converter.cpp
  handlers/changelog_converter.hpp
  handlers/changelog_extractor.cpp
  handlers/changelog_extractor.hpp
  handlers/changelog_generator.cpp
  handlers/changelog_generator.hpp
  handlers/changelog_publisher.cpp
  handlers/changelog_publisher.hpp
  handlers/changelog_publisher_gitlab.cpp
  handlers/changelog_publisher_gitlab.hpp
  handlers/committer.cpp
  handlers/committer.hpp
  handlers/curr_version_extractor.cpp
  handlers/curr_version_extractor.hpp
  handlers/explicit_bumper.cpp
  handlers/explicit_bumper.hpp
  handlers/gitconfig.cpp
  handlers/gitconfig.hpp
  handlers/implicit_bumper.cpp
  handlers/implicit_bumper.hpp
  handlers/next_version_generator.cpp
  handlers/next_version_generator.hpp
  handlers/prev_version_extractor.cpp
  handlers/prev_version_extractor.hpp
  handlers/pusher.cpp
  handlers/pusher.hpp
  handlers/script.cpp
  handlers/script.hpp
  handlers/tagger.cpp
  handlers/tagger.hpp
  handlers/verifier.cpp
  handlers/verifier.hpp
  implicit_bump.hpp
  release.cpp
  release.hpp
  release_commit_executor.cpp
  release_commit_executor.hpp
  release_simple_executor.cpp
  release_simple_executor.hpp
  storage.cpp
  storage.hpp
  version.cpp
  version.hpp
)

target_link_libraries(release_a
  lvd-core
  Qt5::Core
  Qt5::Network
)

# ----------

add_executable(       release
  main.cpp
)

target_link_libraries(release
                      release_a)

install(TARGETS       release
  RUNTIME DESTINATION "${CMAKE_INSTALL_FULL_BINDIR}")
