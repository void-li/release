/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "release_simple_executor.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QStringRef>

#include "lvd/shield.hpp"

// ----------

namespace lvd::release {

Release::SimpleExecutor::SimpleExecutor(const Handlers& simple_handlers,
                                        Invoke          invoke,
                                        QObject*        parent)
    : QObject(parent),
      simple_handlers_(simple_handlers),
      invoke_         (invoke) {
  LVD_LOG_T();

  for (Handler* handler : simple_handlers) {
    LVD_LOG_D() << "handler"
                << handler->section();

    handler->setParent(this);

    connect(handler, &Handler::success,
            this, &SimpleExecutor::on_handler_success);

    connect(handler, &Handler::failure,
            this, &SimpleExecutor::on_handler_failure);
  }
}

Release::SimpleExecutor::~SimpleExecutor() {
  LVD_LOG_T();
}

// ----------

bool Release::SimpleExecutor::execute() {
  LVD_LOG_T();

  return
  LVD_SHIELD;

  bool awake = setup();
  if (awake) {
    emit success();
    return false;
  }

  return false;

  LVD_SHIELD_FUN([&] (const QString& message) {
    emit failure(message);
    return true;
  });
}

// ----------

bool Release::SimpleExecutor::setup() {
  LVD_LOG_T();

  Q_ASSERT(queued_handlers_.isEmpty());
  queued_handlers_ = simple_handlers_;

  bool awake = shine();
  return awake;
}

bool Release::SimpleExecutor::shine() {
  LVD_LOG_T();

                asynchronous_ = false;
  LVD_FINALLY { asynchronous_ = true; };

  for (int i = 0; i < queued_handlers_.size() && !failed_; i++) {
    Handler* handler = queued_handlers_[i];

    bool awake = invoke_(handler);
    if (!awake) {
      if (queued_handlers_.size() > i + 1) {
        queued_handlers_ = queued_handlers_.mid(i + 1);
        return false;
      }

      queued_handlers_.clear();
      return false;
    }
  }

  queued_handlers_.clear();
  return true;
}

bool Release::SimpleExecutor::close() {
  LVD_LOG_T();

  if (queued_handlers_.isEmpty()) {
    bool awake = true;
    if (awake) {
      if (!failed_) {
        emit success();
      }

      return awake;
    }
  }

  bool awake = shine();
  return awake;
}

// ----------

void Release::SimpleExecutor::on_handler_success(const QString& message) {
  LVD_LOG_T() << LVD_LOGLINE(message);
  LVD_SHIELD;

  if (asynchronous_) {
    close();
    return;
  }

  LVD_SHIELD_FUN(&SimpleExecutor::failure);
}

void Release::SimpleExecutor::on_handler_failure(const QString& message) {
  LVD_LOG_T() << LVD_LOGLINE(message);
  LVD_SHIELD;

  if (!failed_) {
    failed_ = true;

    emit failure(message);
    return;
  }

  LVD_SHIELD_FUN(&SimpleExecutor::failure);
}

}  // namespace lvd::release
