/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "lvd/core_task.hpp"

#include <QObject>
#include <QString>
#include <QVector>

#include "lvd/logger.hpp"

#include "handler.hpp"
#include "storage.hpp"

// ----------

namespace lvd::release {

class Release : public QObject, public Task<Release> {
  Q_OBJECT LVD_LOGGER

 private:
  class SimpleExecutor;
  class CommitExecutor;

 public:
  Release(const QString& config,
          QObject*       parent = nullptr);
  ~Release() override;

 public slots:
  void execute();

 signals:
  void success(const QString& message = QString());
  void failure(const QString& message = QString());

 private:
  bool state_settings();

  bool state_before();
  bool state_commit();
  bool state_beyond();

  bool state_finished();

 private slots:
  void on_success(const QString& message);
  void on_failure(const QString& message);

 public:
  QString config_;

  StoragePtr storage_;

  QVector<Handler*> before_handlers_;
  QVector<Handler*> commit_handlers_;
  QVector<Handler*> beyond_handlers_;

  int hard_timeout_ = -1;
  int soft_timeout_ = -1;
};

}  // namespace lvd::release
