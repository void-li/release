/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QtGlobal>

#include <QObject>
#include <QPair>
#include <QSharedPointer>
#include <QString>
#include <QVector>

#include "explicit_bump.hpp"
#include "implicit_bump.hpp"
#include "version.hpp"

// ----------

namespace lvd::release {

class Storage;
using StoragePtr = QSharedPointer<Storage>;

class Storage : public QObject {
  Q_OBJECT

 private:
  Storage(QObject* parent = nullptr);

 public:
  static StoragePtr create() {
    return StoragePtr(new Storage());
  }

 public:
  QString version_format() const { return version_format_; }
  void set_version_format(const QString& version_format) {
    version_format_ = version_format;
  }

 private:
  QString version_format_;

 public:
  const Version& curr_version() const { return curr_version_; }
  void set_curr_version(const Version& curr_version) {
    curr_version_ = curr_version;
  }

  const Version& prev_version() const { return prev_version_; }
  void set_prev_version(const Version& prev_version) {
    prev_version_ = prev_version;
  }

  const Version& next_version() const { return next_version_; }
  void set_next_version(const Version& next_version) {
    next_version_ = next_version;
  }

 private:
  Version curr_version_;
  Version prev_version_;
  Version next_version_;

 public:
  ExplicitBump explicit_bump() const { return explicit_bump_; }
  void set_explicit_bump(ExplicitBump explicit_bump) {
    explicit_bump_ = explicit_bump;
  }

  void bump_explicit(ExplicitBump explicit_bump);

  ImplicitBump implicit_bump() const { return implicit_bump_; }
  void set_implicit_bump(ImplicitBump implicit_bump) {
    implicit_bump_ = implicit_bump;
  }

  void bump_implicit(ImplicitBump implicit_bump);

 private:
  ExplicitBump explicit_bump_;
  ImplicitBump implicit_bump_;

 public:
  using ChangelogEntry   =         QPair<QString, QString>;
  using ChangelogEntries = QVector<QPair<QString, QString>>;

  using ChangelogGroup   =         QPair<QString, ChangelogEntries>;
  using ChangelogGroups  = QVector<QPair<QString, ChangelogEntries>>;

  using Changelog        = ChangelogGroups;

 public:
  void add_changelog_group(const QString& grptext);

  void add_changelog_entry(const QString& grptext,
                           const QString& subject,
                           const QString& message);

  void set_changelog_entry(int group , int entry ,
                           const QString& subject,
                           const QString& message);

  void set_changelog_entry_subject(int group, int entry,
                                   const QString& subject) {
    set_changelog_entry(group, entry, subject, changelog_entry_message(group, entry));
  }

  void set_changelog_entry_message(int group, int entry,
                                   const QString& message) {
    set_changelog_entry(group, entry, changelog_entry_subject(group, entry), message);
  }

  int     changelog_size() const {
    return changelog_.size();
  }

  QString changelog_group     (int group) const {
    Q_ASSERT(group < changelog_.size());
    return changelog_[group].first;
  }

  int     changelog_group_size(int group) const {
    Q_ASSERT(group < changelog_.size());
    return changelog_[group].second.size();
  }

  QString changelog_entry_subject(int group, int entry) const {
    Q_ASSERT(group < changelog_.size());
    Q_ASSERT(entry < changelog_[group].second.size());

    return changelog_[group].second[entry].first;
  }

  QString changelog_entry_message(int group, int entry) const {
    Q_ASSERT(group < changelog_.size());
    Q_ASSERT(entry < changelog_[group].second.size());

    return changelog_[group].second[entry].second;
  }

 private:
  Changelog changelog_;
};

}  // namespace lvd::release
