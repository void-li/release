/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "formatter.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>

// ----------

namespace lvd::release {

void    Formatter::append(const QString& key,
                          const QString& val) {
  mappings_[key] = val;
}

QString Formatter::format(const QString& str) const {
  QString outcome = str;

  QRegularExpression expression("%{([a-zA-Z_]+[a-zA-Z0-9_]*)}");
  Q_ASSERT(expression.isValid());

  auto   matches = expression.globalMatch(str);
  while (matches.hasNext()) {
    auto match   = matches.next();

    QString match_all = match.captured(0);
    QString match_key = match.captured(1);

    if (mappings_.contains(match_key)) {
      QString replace = mappings_[match_key];
      outcome.replace(match_all, replace);
    }
  }

  return outcome;
}

}  // namespace lvd::release
