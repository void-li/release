/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "commit.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QStringRef>
#include <QVector>

// ----------

namespace lvd::release {

Commit::Commit() = default;

Commit::Commit(const QString& data) {
  bool subject_flag = false;
  bool subject_done = false;

  bool message_flag = false;
  bool message_done = false;

  QVector<QStringRef> lines = data.splitRef("\n");
  for (QStringRef& line : lines) {
    if (author_name_.isEmpty()) {
      if (line.startsWith("Author: ")) {
        QRegularExpression expression("^Author: ([^<]+) <([^>]+)>$");
        Q_ASSERT(expression.isValid());

        auto match = expression.match(line);
        if (match.hasMatch()) {
          author_name_ = match.captured(1);
          author_mail_ = match.captured(2);
        }

        continue;
      }
    }

    if (commit_date_.isEmpty()) {
      if (line.startsWith("Date:   ")) {
        QRegularExpression expression("^Date:   (.+)$");
        Q_ASSERT(expression.isValid());

        auto match = expression.match(line);
        if (match.hasMatch()) {
          commit_date_ = match.captured(1);
        }

        continue;
      }
    }

    if      (subject_flag && !subject_done) {
      const QString pref = "    ";

      if (line.startsWith(pref)) {
        subject_.append(line.mid(pref.size()));
        subject_.append("\n");
      }
    }
    else if (message_flag && !message_done) {
      const QString pref = "    ";

      if (line.startsWith(pref)) {
        message_.append(line.mid(pref.size()));
        message_.append("\n");
      }
    }

    if (line.isEmpty()) {
      if      (!subject_flag) {
        subject_flag = true;
      }
      else if (!message_flag) {
        message_flag = true;
        subject_done = true;
      }
    }
  }

  subject_ = subject_.trimmed();
  message_ = message_.trimmed();
}

}  // namespace lvd::release
