/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "changelog_publisher_gitlab.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QGlobalStatic>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QMetaObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QStringRef>
#include <QUrl>

#include "lvd/shield.hpp"
#include "lvd/string.hpp"

#include "changelog_formatter.hpp"
#include "formatter.hpp"
#include "handler_settings.hpp"

// ----------

namespace {

Q_GLOBAL_STATIC_WITH_ARGS(const QString,
  GitlabCreateReleaseUrl, ( "%1/projects/%2/releases"    ));

Q_GLOBAL_STATIC_WITH_ARGS(const QString,
  GitlabUpdateReleaseUrl, ( "%1/projects/%2/releases/%3" ));

}  // namespace

// ----------

namespace lvd::release::handlers {

ChangelogPublisherGitlab::ChangelogPublisherGitlab(const QString&    section,
                                                   const StoragePtr& storage,
                                                         QSettings&  settings,
                                                         QObject*    parent)
    : Handler(Handles::Beyond,
              section,
              storage,
              settings,
              parent) {
  LVD_LOG_T() << section;

  Handler::Settings handler_settings(settings);

  gitlab_api_ = handler_settings.required<QString>("gitlab_api");
  gitlab_api_ = rstrip(gitlab_api_, "/").toString();

  LVD_LOG_D() << section << "gitlab_api:" << gitlab_api_;

  gitlab_pid_ = handler_settings.required<QString>("gitlab_pid");
  LVD_LOG_D() << section << "gitlab_pid:" << gitlab_pid_;

  gitlab_tok_ = handler_settings.required<QString>("gitlab_tok");
  LVD_LOG_D() << section << "gitlab_tok:" << gitlab_tok_;

  title_format_ = handler_settings.required<QString>("title_format");
  LVD_LOG_D() << section << "title_format:" << title_format_;

  group_format_ = handler_settings.required<QString>("group_format");
  LVD_LOG_D() << section << "group_format:" << group_format_;

  entry_format_ = handler_settings.required<QString>("entry_format");
  LVD_LOG_D() << section << "entry_format:" << entry_format_;

  assets_name_ = handler_settings.optional<QString>("assets_name");
  LVD_LOG_D() << section << "assets_name:" << assets_name_;

  assets_link_ = handler_settings.optional<QString>("assets_link");
  LVD_LOG_D() << section << "assets_link:" << assets_link_;

  if (assets_name_.isEmpty() != assets_link_.isEmpty()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section
                        << "assets_name and assets_link must be set together";

    LVD_THROW_RUNTIME(message);
  }

  subject_ = handler_settings.optional<QString>("subject",  "%{version}" );
  LVD_LOG_D() << section << "subject:" << subject_;

  message_ = handler_settings.optional<QString>("message", "%{changelog}");
  LVD_LOG_D() << section << "message:" << message_;
}

// ----------

QString ChangelogPublisherGitlab::create_release_subject(const QString& subject,
                                                         const Version& version) {
  Formatter formatter;
  formatter.append("major", QString::number(version.major()));
  formatter.append("minor", QString::number(version.minor()));
  formatter.append("patch", QString::number(version.patch()));
  formatter.append("version",               version.to_string());

  return formatter.format(subject).trimmed();
}

QString ChangelogPublisherGitlab::create_release_message(const QString& message,
                                                         const Version& version,
                                                         const QString& changelog) {
  Formatter formatter;
  formatter.append("major", QString::number(version.major()));
  formatter.append("minor", QString::number(version.minor()));
  formatter.append("patch", QString::number(version.patch()));
  formatter.append("version",               version.to_string());

  formatter.append("changelog", changelog);

  return formatter.format(message).trimmed();
}

ChangelogPublisherGitlab::~ChangelogPublisherGitlab() {
  LVD_LOG_T() << section();
}

// ----------

bool ChangelogPublisherGitlab::handle_beyond_impl() {
  LVD_LOG_T() << section();

  create_manager();

  QMetaObject::invokeMethod(this, [&] {
    LVD_LOG_T() << section();
    LVD_SHIELD;

    create_release();

    LVD_SHIELD_FUN(&ChangelogPublisherGitlab::failure);
  }, Qt::QueuedConnection);

  return false;
}

// ----------

void ChangelogPublisherGitlab::create_release() {
  LVD_LOG_T() << section();

  create_hard_timer();
  create_soft_timer();

  Version version = make_version();
  if (!version.valid()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "missing version";

    emit failure(message);
    return;
  }

  QNetworkRequest qnetworkrequest;
  qnetworkrequest.setRawHeader("PRIVATE-TOKEN", gitlab_tok_.toUtf8());
  qnetworkrequest.setRawHeader("Content-Type" , "application/json");

  QUrl qurl(GitlabCreateReleaseUrl->arg(gitlab_api_, gitlab_pid_));
  qnetworkrequest.setUrl(qurl);

  QString changelog = changelog_formatter(storage(),
                                          version,
                                          title_format_,
                                          group_format_,
                                          entry_format_);

  QString release_name = create_release_subject(subject_, version);
  LVD_LOG_D() << section()
              << "release_name"
              << LVD_LOGLINE(release_name);

  QString release_desc = create_release_message(message_, version, changelog);
  LVD_LOG_D() << section()
              << "release_desc"
              << LVD_LOGLINE(release_desc);

  if (release_desc.isEmpty()) {
    release_desc = "-";
  }

  QJsonObject qjsonobject;
  qjsonobject.insert("tag_name",    QJsonValue(version.to_string()));
  qjsonobject.insert("name",        QJsonValue(release_name));
  qjsonobject.insert("description", QJsonValue(release_desc));

  if (   !assets_name_.isEmpty()
      && !assets_link_.isEmpty()) {
    QJsonObject assetsObj;
    assetsObj.insert("name", QJsonValue(assets_name_));
    assetsObj.insert("url" , QJsonValue(assets_link_));

    QJsonArray  assetsArr;
    assetsArr.append(assetsObj);

    QJsonObject assetsLnk;
    assetsLnk.insert("links", assetsArr);

    qjsonobject.insert(
      "assets", assetsLnk
    );
  }

  QJsonDocument qjsondocument;
  qjsondocument.setObject(qjsonobject);

  QByteArray data = qjsondocument.toJson();
  LVD_LOG_D() << data;

  reply_ = qmanager_->post(qnetworkrequest, data);

  connect(reply_, &QNetworkReply::finished,
          this, &ChangelogPublisherGitlab::on_create_release_finished);
}

void ChangelogPublisherGitlab::update_release() {
  LVD_LOG_T() << section();

  create_hard_timer();
  create_soft_timer();

  Version version = make_version();
  if (!version.valid()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "missing version";

    emit failure(message);
    return;
  }

  QNetworkRequest qnetworkrequest;
  qnetworkrequest.setRawHeader("PRIVATE-TOKEN", gitlab_tok_.toUtf8());
  qnetworkrequest.setRawHeader("Content-Type" , "application/json");

  QUrl qurl(GitlabUpdateReleaseUrl->arg(gitlab_api_, gitlab_pid_, version.to_string()));
  qnetworkrequest.setUrl(qurl);

  QString changelog = changelog_formatter(storage(),
                                          version,
                                          title_format_,
                                          group_format_,
                                          entry_format_);

  QString release_name = create_release_subject(subject_, version);
  LVD_LOG_D() << section()
              << "release_name"
              << LVD_LOGLINE(release_name);

  QString release_desc = create_release_message(message_, version, changelog);
  LVD_LOG_D() << section()
              << "release_desc"
              << LVD_LOGLINE(release_desc);

  QJsonObject qjsonobject;
//  qjsonobject.insert("tag_name",    QJsonValue(version.to_string())); NO!
  qjsonobject.insert("name",        QJsonValue(release_name));
  qjsonobject.insert("description", QJsonValue(release_desc));

  QJsonDocument qjsondocument;
  qjsondocument.setObject(qjsonobject);

  QByteArray data = qjsondocument.toJson();
  LVD_LOG_D() << data;

  reply_ = qmanager_->put (qnetworkrequest, data);

  connect(reply_, &QNetworkReply::finished,
          this, &ChangelogPublisherGitlab::on_update_release_finished);
}

// ----------

void ChangelogPublisherGitlab::on_create_release_finished() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  remove_soft_timer();
  remove_hard_timer();

  LVD_FINALLY {
    reply_->deleteLater();
    reply_ = nullptr;
  };

  auto error = reply_->error();
  if (error != QNetworkReply::NoError) {
    LVD_LOG_D() << section()
                << "create error"
                << reply_->errorString();

    if (error == QNetworkReply::ContentConflictError) {
      QMetaObject::invokeMethod(this, [&] {
        LVD_LOG_T() << section();
        LVD_SHIELD;

        update_release();

        LVD_SHIELD_FUN(&ChangelogPublisherGitlab::failure);
      }, Qt::QueuedConnection);

      return;
    }

    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "create error"
                        << reply_->errorString()
                        << reply_->readAll();

    emit failure(message);
    return;
  }

  remove_manager();

  emit success();
  return;

  LVD_SHIELD_FUN(&ChangelogPublisherGitlab::failure);
}

void ChangelogPublisherGitlab::on_update_release_finished() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  remove_soft_timer();
  remove_hard_timer();

  LVD_FINALLY {
    reply_->deleteLater();
    reply_ = nullptr;
  };

  auto error = reply_->error();
  if (error != QNetworkReply::NoError) {
    LVD_LOG_D() << section()
                << "update error"
                << reply_->errorString();

    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "update error"
                        << reply_->errorString()
                        << reply_->readAll();

    emit failure(message);
    return;
  }

  remove_manager();

  emit success();
  return;

  LVD_SHIELD_FUN(&ChangelogPublisherGitlab::failure);
}

}  // namespace lvd::release::handlers
