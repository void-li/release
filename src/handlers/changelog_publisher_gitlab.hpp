/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QNetworkReply>
#include <QObject>
#include <QSettings>
#include <QString>

#include "lvd/logger.hpp"

#include "handler.hpp"
#include "storage.hpp"
#include "version.hpp"

// ----------

namespace lvd::release::handlers {

class ChangelogPublisherGitlab : public Handler {
  Q_OBJECT LVD_LOGGER

 public:
  ChangelogPublisherGitlab        (const QString&    section,
                                   const StoragePtr& storage,
                                         QSettings&  settings,
                                         QObject*    parent = nullptr);

  ~ChangelogPublisherGitlab() override;

  static
  ChangelogPublisherGitlab* create(const QString&    section,
                                   const StoragePtr& storage,
                                         QSettings&  settings,
                                         QObject*    parent = nullptr) {
    return new ChangelogPublisherGitlab(section, storage, settings, parent);
  }

 public:
  static QString create_release_subject(const QString& subject,
                                        const Version& version);

  static QString create_release_message(const QString& message,
                                        const Version& version,
                                        const QString& changelog);

 private:
  bool handle_beyond_impl() override;

 private:
  void create_release();
  void update_release();

 private slots:
  void on_create_release_finished();
  void on_update_release_finished();

 private:
  QString gitlab_api_;
  QString gitlab_pid_;
  QString gitlab_tok_;

  QString title_format_;
  QString group_format_;
  QString entry_format_;

  QString assets_name_;
  QString assets_link_;

  QString subject_;
  QString message_;

 private:
  QNetworkReply* reply_ = nullptr;
};

DECLARE_HANDLER(ChangelogPublisherGitlab, changelog_publisher_gitlab);

}  // namespace lvd::release::handlers
