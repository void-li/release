/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "next_version_generator.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include "explicit_bump.hpp"
#include "implicit_bump.hpp"
#include "version.hpp"

// ----------

namespace lvd::release::handlers {

NextVersionGenerator::NextVersionGenerator(const QString&    section,
                                           const StoragePtr& storage,
                                                 QSettings&  settings,
                                                 QObject*    parent)
    : Handler(Handles::Beyond,
              section,
              storage,
              settings,
              parent) {
  LVD_LOG_T() << section;
}

NextVersionGenerator::~NextVersionGenerator() {
  LVD_LOG_T() << section();
}

// ----------

bool NextVersionGenerator::handle_beyond_impl() {
  LVD_LOG_T() << section();

  Version curr_version = storage()->curr_version();
  if ( curr_version.valid()) {
    LVD_LOG_D() << section()
                << "curr version"
                << curr_version.to_string();
  } else {
    LVD_LOG_D() << section()
                << "curr version missing";
  }

  if ( curr_version.valid()) {
    return true;
  }

  Version prev_version = storage()->prev_version();
  if ( prev_version.valid()) {
    LVD_LOG_D() << section()
                << "prev version"
                << prev_version.to_string();
  } else {
    LVD_LOG_D() << section()
                << "prev version missing";
  }

//  if (!prev_version.valid()) { NO!
//    return true;
//  }

  Version next_version = prev_version;
  if (!next_version.valid()) {
    next_version = Version(0, 0, 0,
                           storage()->version_format());
  }

  ExplicitBump explicit_bump = storage()->explicit_bump();
  ImplicitBump implicit_bump = storage()->implicit_bump();

  if      (explicit_bump.valid()) {
    LVD_LOG_D() << section()
                << "explicit bump"
                << explicit_bump.to_string();

    switch (explicit_bump) {
      case ExplicitBump::Major:
        next_version.bump_major();
        break;

      case ExplicitBump::Minor:
        next_version.bump_minor();
        break;

      case ExplicitBump::Patch:
        next_version.bump_patch();
        break;

      LVD_DEFAULT:
        LVD_THROW_IMPOSSIBLE;
        LVD_DEFAULT_BREAK;
    }
  }
  else if (implicit_bump.valid() && prev_version.major() >= 1) {
    LVD_LOG_D() << section()
                << "implicit bump"
                << implicit_bump.to_string();

    switch (implicit_bump) {
      case ImplicitBump::Major:
        next_version.bump_major();
        break;

      case ImplicitBump::Minor:
        next_version.bump_minor();
        break;

      case ImplicitBump::Patch:
        next_version.bump_patch();
        break;

      LVD_DEFAULT:
        LVD_THROW_IMPOSSIBLE;
        LVD_DEFAULT_BREAK;
    }
  }
  else {
    LVD_LOG_D() << section()
                << "fallback bump";

    next_version.bump_patch();
  }

  storage()->set_next_version(next_version);

  return true;
}

}  // namespace lvd::release::handlers
