/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "gitconfig.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QMetaObject>
#include <QProcess>
#include <QStringList>
#include <QVariant>

#include "lvd/shield.hpp"

// ----------

namespace lvd::release::handlers {

Gitconfig::Gitconfig(const QString&    section,
                     const StoragePtr& storage,
                           QSettings&  settings,
                           QObject*    parent)
    : Handler(Handles(),
              section,
              storage,
              settings,
              parent) {
  LVD_LOG_T() << section;

  const QStringList child_keys = settings.childKeys();

  for (const QString& key : child_keys) {
    const QString  val = settings.value(key).toString();

    if (   key == "handles"
        || key == "hard_timeout"
        || key == "soft_timeout") {
      continue;
    }

    LVD_LOG_D() << section
                << "key" << key
                << "val" << val;

    configs_.append({ key, val });
  }

  if (configs_.isEmpty()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section
                        << "missing configs";

    LVD_THROW_RUNTIME(message);
  }
}

Gitconfig::~Gitconfig() {
  LVD_LOG_T() << section();
}

// ----------

bool Gitconfig::handle_before_impl() {
  LVD_LOG_T() << section();

  current_ = 0;
  return handle_things_impl();
}

bool Gitconfig::handle_commit_impl(const Commit&) {
  LVD_LOG_T() << section();

  current_ = 0;
  return handle_things_impl();
}

bool Gitconfig::handle_beyond_impl() {
  LVD_LOG_T() << section();

  current_ = 0;
  return handle_things_impl();
}

bool Gitconfig::handle_things_impl() {
  LVD_LOG_T() << section();

  create_process();
  qprocess_->start("git", { "config",
                           configs_[current_].first,
                           configs_[current_].second });

  LVD_LOG_D() << section()
              << "execute"
              << qprocess_->program()
              << qprocess_->arguments();

  create_hard_timer();
  create_soft_timer();

  return false;
}

// ----------

void Gitconfig::on_finished() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  Handler::on_finished();

  if (qprocess_) {
    LVD_LOG_D() << section()
                << "process result"
                << qprocess_->program()
                << qprocess_->arguments()
                << qprocess_->exitStatus()
                << qprocess_->exitCode();

    if (   qprocess_->exitStatus() == QProcess::NormalExit
        && qprocess_->exitCode()   == 0) {
      if (++current_ < configs_.size()) {
        QMetaObject::invokeMethod(this, [&] {
          LVD_LOG_T() << section();
          LVD_SHIELD;

          handle_things_impl();

          LVD_SHIELD_FUN(&Gitconfig::failure);
        }, Qt::QueuedConnection);

        return;
      }

      emit success();
      return;
    }

    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "process failed"
                        << qprocess_->program()
                        << qprocess_->arguments()
                        << qprocess_->exitStatus()
                        << qprocess_->exitCode()
                        << stderr_;

    emit failure(message);
    return;
  }

  LVD_THROW_IMPOSSIBLE;

  LVD_SHIELD_FUN(&Gitconfig::failure);
}

}  // namespace lvd::release::handlers
