/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QRegularExpression>
#include <QSettings>
#include <QString>

#include "lvd/logger.hpp"

#include "handler.hpp"
#include "storage.hpp"

// ----------

namespace lvd::release::handlers {

class ChangelogConverter : public Handler {
  Q_OBJECT LVD_LOGGER

 public:
  ChangelogConverter        (const QString&    section,
                             const StoragePtr& storage,
                                   QSettings&  settings,
                                   QObject*    parent = nullptr);

  ~ChangelogConverter() override;

  static
  ChangelogConverter* create(const QString&    section,
                             const StoragePtr& storage,
                                   QSettings&  settings,
                                   QObject*    parent = nullptr) {
    return new ChangelogConverter(section, storage, settings, parent);
  }

 private:
  bool handle_beyond_impl() override;

 private:
  QString convert(const QRegularExpression& match_thingy,
                  const QString&            match_thingy_format,
                  const QString&                  thingy);

 private:
  QRegularExpression match_subject_;
  bool               match_subject_alive_ = false;
  QString            match_subject_format_;

  QRegularExpression match_message_;
  bool               match_message_alive_ = false;
  QString            match_message_format_;
};

DECLARE_HANDLER(ChangelogConverter, changelog_converter);

}  // namespace lvd::release::handlers
