/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "changelog_extractor.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QIODevice>
#include <QList>
#include <QStringRef>

#include "handler_settings.hpp"
#include "version.hpp"

// ----------

namespace lvd::release::handlers {

ChangelogExtractor::ChangelogExtractor(const QString&    section,
                                       const StoragePtr& storage,
                                             QSettings&  settings,
                                             QObject*    parent)
    : Handler(Handles::Beyond,
              section,
              storage,
              settings,
              parent) {
  LVD_LOG_T() << section;

  Handler::Settings handler_settings(settings);

  changelog_ = handler_settings.required<QString>("changelog");
  LVD_LOG_D() << section << "changelog:" << changelog_;

  auto title_starts = handler_settings.required<QString>("title_starts");
  if (!title_starts.isEmpty()) {
    LVD_LOG_D() << section
                << "title_starts:"
                << title_starts;

    title_starts_.setPattern(title_starts);

    if (!title_starts_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid title_starts"
                          << title_starts
                          << title_starts_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  auto title_inside = handler_settings.optional<QString>("title_inside");
  if (!title_inside.isEmpty()) {
    LVD_LOG_D() << section
                << "title_inside:"
                << title_inside;

    title_inside_.setPattern(title_inside);
    title_inside_alive_ = true;

    if (!title_inside_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid title_inside"
                          << title_inside
                          << title_inside_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  auto title_parser = handler_settings.optional<QString>("title_parser");
  if (!title_parser.isEmpty()) {
    LVD_LOG_D() << section
                << "title_parser:"
                << title_parser;

    title_parser_.setPattern(title_parser);
    title_parser_alive_ = true;

    if (!title_parser_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid title_parser"
                          << title_parser
                          << title_parser_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  auto group_starts = handler_settings.required<QString>("group_starts");
  if (!group_starts.isEmpty()) {
    LVD_LOG_D() << section
                << "group_starts:"
                << group_starts;

    group_starts_.setPattern(group_starts);

    if (!group_starts_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid group_starts"
                          << group_starts
                          << group_starts_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  auto group_inside = handler_settings.optional<QString>("group_inside");
  if (!group_inside.isEmpty()) {
    LVD_LOG_D() << section
                << "group_inside:"
                << group_inside;

    group_inside_.setPattern(group_inside);
    group_inside_alive_ = true;

    if (!group_inside_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid group_inside"
                          << group_inside
                          << group_inside_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  auto group_parser = handler_settings.optional<QString>("group_parser");
  if (!group_parser.isEmpty()) {
    LVD_LOG_D() << section
                << "group_parser:"
                << group_parser;

    group_parser_.setPattern(group_parser);
    group_parser_alive_ = true;

    if (!group_parser_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid group_parser"
                          << group_parser
                          << group_parser_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  auto entry_starts = handler_settings.required<QString>("entry_starts");
  if (!entry_starts.isEmpty()) {
    LVD_LOG_D() << section
                << "entry_starts:"
                << entry_starts;

    entry_starts_.setPattern(entry_starts);

    if (!entry_starts_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid entry_starts"
                          << entry_starts
                          << entry_starts_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  auto entry_inside = handler_settings.optional<QString>("entry_inside");
  if (!entry_inside.isEmpty()) {
    LVD_LOG_D() << section
                << "entry_inside:"
                << entry_inside;

    entry_inside_.setPattern(entry_inside);
    entry_inside_alive_ = true;

    if (!entry_inside_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid entry_inside"
                          << entry_inside
                          << entry_inside_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }
}

ChangelogExtractor::~ChangelogExtractor() {
  LVD_LOG_T() << section();
}

// ----------

bool ChangelogExtractor::handle_beyond_impl() {
  LVD_LOG_T() << section();

  QFile qfile(changelog_);
  qfile.open(QIODevice::ReadOnly);

  if (!qfile.isOpen()) {
    LVD_LOG_D() << section()
                << "no such changelog"
                << changelog_;

    return true;
  }

  while (!qfile.atEnd()) {
    QByteArray line = qfile.readLine();

    if (line.endsWith('\n')) {
      line.chop(1);
    }

    do {
      QRegularExpressionMatch match;

      if      (           (match = title_starts_.match(line)).hasMatch()) {
        LVD_LOG_D() << section()
                    << "title starts"
                    << line;

        QString     title_data { line };
        line = handle_title(qfile, title_data, match);
      }
      else if (inside_ && (match = group_starts_.match(line)).hasMatch()) {
        LVD_LOG_D() << section()
                    << "group starts"
                    << line;

        QString     group_data { line };
        line = handle_group(qfile, group_data, match);
      }
      else if (inside_ && (match = entry_starts_.match(line)).hasMatch()) {
        LVD_LOG_D() << section()
                    << "entry starts"
                    << line;

        QStringList entry_data { line };
        line = handle_entry(qfile, entry_data, match);
      }
      else {
        line.clear();
        break;
      }
    } while (!line.isNull());
  }

  return true;
}

// ----------

QByteArray ChangelogExtractor::handle_title(QFile& qfile, QString    & title_data,
                                            const QRegularExpressionMatch& title_match) {
  LVD_LOG_T() << section();

  QByteArray line;

  if      (QStringRef title_texts = title_match.capturedRef("title");
           !title_texts.isNull()) {
    title_data = title_texts.toString();
  }
  else if (QStringRef title_texts = title_match.capturedRef(   1   );
           !title_texts.isNull()) {
    title_data = title_texts.toString();
  }

  while (!qfile.atEnd()) {
    line = qfile.readLine();

    if (line.endsWith('\n')) {
      line.chop(1);
    }

    QRegularExpressionMatch alter_match;

    if (   title_inside_alive_
        && (alter_match = title_inside_.match(line)).hasMatch()) {
      LVD_LOG_D() << section()
                  << "title inside"
                  << line;

      if      (QStringRef alter_texts = alter_match.capturedRef("title");
               !alter_texts.isNull()) {
        title_data.append('\n');
        title_data.append(alter_texts.toString());
      }
      else if (QStringRef alter_texts = alter_match.capturedRef(   1   );
               !alter_texts.isNull()) {
        title_data.append('\n');
        title_data.append(alter_texts.toString());
      }
      else {
        title_data.append('\n');
        title_data.append(line);
      }

      continue;
    }

    break;
  }

  QRegularExpression& rexpr = title_parser_alive_ ? title_parser_
                                                  : title_starts_;

  auto match = rexpr.match(title_data);
  if (!match.hasMatch()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "title expression"
                        << rexpr.pattern()
                        << "doesn't match"
                        << LVD_LOGLINE(title_data);

    LVD_THROW_RUNTIME(message);
  }

  Version version;

  if (!version.valid()) {
    LVD_LOG_D() << section()
                << "extracting version by value from"
                << LVD_LOGLINE(title_data);

    bool    major_ok;
    quint32 major = match.capturedRef("major").toUInt(&major_ok);

    bool    minor_ok;
    quint32 minor = match.capturedRef("minor").toUInt(&minor_ok);

    bool    patch_ok;
    quint32 patch = match.capturedRef("patch").toUInt(&patch_ok);

    if (   major_ok
        && minor_ok
        && patch_ok) {
      version = Version(major, minor, patch,
                        storage()->version_format());
    }
  }

  if (!version.valid()) {
    LVD_LOG_D() << section()
                << "extracting version by descr from"
                << LVD_LOGLINE(title_data);

    QStringRef vtext = match.capturedRef("version");
    if (!vtext.isNull()) {
      version = Version(vtext.toString(),
                        storage()->version_format());
    }
  }

  if (!version.valid()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "cannot extract version from"
                        << LVD_LOGLINE(title_data);

    LVD_THROW_RUNTIME(message);
  }

  Version next_version = storage()->next_version();
  if (next_version.valid()) {
    LVD_LOG_D() << section()
                << "comparing to next version"
                << next_version.to_string();

    inside_ = (next_version == version);
    beyond_ = (next_version >  version);
  }
  else {
    Version curr_version = storage()->curr_version();
    if (curr_version.valid()) {
      LVD_LOG_D() << section()
                  << "comparing to curr version"
                  << curr_version.to_string();

      inside_ = (curr_version == version);
      beyond_ = (curr_version >  version);
    }
    else {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section()
                          << "missing version";

      LVD_THROW_RUNTIME(message);
    }
  }

  return line;
}

QByteArray ChangelogExtractor::handle_group(QFile& qfile, QString    & group_data,
                                            const QRegularExpressionMatch& group_match) {
  LVD_LOG_T() << section();

  QByteArray line;

  if      (QStringRef group_texts = group_match.capturedRef("group");
           !group_texts.isNull()) {
    group_data = group_texts.toString();
  }
  else if (QStringRef group_texts = group_match.capturedRef(   1   );
           !group_texts.isNull()) {
    group_data = group_texts.toString();
  }

  while (!qfile.atEnd()) {
    line = qfile.readLine();

    if (line.endsWith('\n')) {
      line.chop(1);
    }

    QRegularExpressionMatch alter_match;

    if (   group_inside_alive_
        && (alter_match = group_inside_.match(line)).hasMatch()) {
      LVD_LOG_D() << section()
                  << "group inside"
                  << line;

      if      (QStringRef alter_texts = alter_match.capturedRef("group");
               !alter_texts.isNull()) {
        group_data.append('\n');
        group_data.append(alter_texts.toString());
      }
      else if (QStringRef alter_texts = alter_match.capturedRef(   1   );
               !alter_texts.isNull()) {
        group_data.append('\n');
        group_data.append(alter_texts.toString());
      }
      else {
        group_data.append('\n');
        group_data.append(line);
      }

      continue;
    }

    break;
  }

  QRegularExpression& rexpr = group_parser_alive_ ? group_parser_
                                                  : group_starts_;

  auto match = rexpr.match(group_data);
  if (!match.hasMatch()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "group expression"
                        << rexpr.pattern()
                        << "doesn't match"
                        << LVD_LOGLINE(group_data);

    LVD_THROW_RUNTIME(message);
  }

  group_ = match.captured("group").trimmed();
  LVD_LOG_D() << section() << "group:" << group_;

  return line;
}

QByteArray ChangelogExtractor::handle_entry(QFile& qfile, QStringList& entry_data,
                                            const QRegularExpressionMatch& entry_match) {
  LVD_LOG_T() << section();

  QByteArray line;

  if      (QStringRef entry_texts = entry_match.capturedRef("entry");
           !entry_texts.isNull()) {
    entry_data[0] = entry_texts.toString();
  }
  else if (QStringRef entry_texts = entry_match.capturedRef(   1   );
           !entry_texts.isNull()) {
    entry_data[0] = entry_texts.toString();
  }

  while (!qfile.atEnd()) {
    line = qfile.readLine();

    if (line.endsWith('\n')) {
      line.chop(1);
    }

    QRegularExpressionMatch alter_match;

    if (   entry_inside_alive_
        && (alter_match = entry_inside_.match(line)).hasMatch()) {
      LVD_LOG_D() << section()
                  << "entry inside"
                  << line;

      if      (QStringRef alter_texts = alter_match.capturedRef("entry");
               !alter_texts.isNull()) {
        entry_data.append(alter_texts.toString());
      }
      else if (QStringRef alter_texts = alter_match.capturedRef(   1   );
               !alter_texts.isNull()) {
        entry_data.append(alter_texts.toString());
      }
      else {
        entry_data.append(line);
      }

      continue;
    }

    break;
  }

  int i = 0;

  QString subject;
  for (; i < entry_data.size(); i++) {
    if (entry_data[i].isEmpty()) {
      break;
    }

    subject.append(entry_data[i]);
    subject.append('\n');
  }

  subject = subject.trimmed();
  LVD_LOG_D() << section()
              << "subject"
              << LVD_LOGLINE(subject);

  QString message;
  for (; i < entry_data.size(); i++) {
    message.append(entry_data[i]);
    message.append('\n');
  }

  message = message.trimmed();
  LVD_LOG_D() << section()
              << "message"
              << LVD_LOGLINE(message);

  storage()->add_changelog_entry(group_,
                                 subject,
                                 message);

  return line;
}

}  // namespace lvd::release::handlers
