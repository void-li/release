/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "changelog_publisher.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QIODevice>
#include <QSaveFile>
#include <QStringRef>

#include "changelog_formatter.hpp"
#include "handler_settings.hpp"
#include "version.hpp"

// ----------

namespace lvd::release::handlers {

ChangelogPublisher::ChangelogPublisher(const QString&    section,
                                       const StoragePtr& storage,
                                             QSettings&  settings,
                                             QObject*    parent)
    : Handler(Handles::Beyond,
              section,
              storage,
              settings,
              parent) {
  LVD_LOG_T() << section;

  Handler::Settings handler_settings(settings);

  changelog_ = handler_settings.required<QString>("changelog");
  LVD_LOG_D() << section << "changelog:" << changelog_;

  auto title_starts = handler_settings.required<QString>("title_starts");
  if (!title_starts.isEmpty()) {
    LVD_LOG_D() << section
                << "title_starts:"
                << title_starts;

    title_starts_.setPattern(title_starts);

    if (!title_starts_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid title_starts"
                          << title_starts
                          << title_starts_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  auto title_inside = handler_settings.optional<QString>("title_inside");
  if (!title_inside.isEmpty()) {
    LVD_LOG_D() << section
                << "title_inside:"
                << title_inside;

    title_inside_.setPattern(title_inside);
    title_inside_alive_ = true;

    if (!title_inside_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid title_inside"
                          << title_inside
                          << title_inside_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  auto title_parser = handler_settings.optional<QString>("title_parser");
  if (!title_parser.isEmpty()) {
    LVD_LOG_D() << section
                << "title_parser:"
                << title_parser;

    title_parser_.setPattern(title_parser);
    title_parser_alive_ = true;

    if (!title_parser_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid title_parser"
                          << title_parser
                          << title_parser_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  title_format_ = handler_settings.required<QString>("title_format");
  LVD_LOG_D() << section << "title_format:" << title_format_;

  group_format_ = handler_settings.required<QString>("group_format");
  LVD_LOG_D() << section << "group_format:" << group_format_;

  entry_format_ = handler_settings.required<QString>("entry_format");
  LVD_LOG_D() << section << "entry_format:" << entry_format_;
}

ChangelogPublisher::~ChangelogPublisher() {
  LVD_LOG_T() << section();
}

// ----------

bool ChangelogPublisher::handle_beyond_impl() {
  LVD_LOG_T() << section();

  QFile qfile(changelog_);
  qfile.open(QIODevice::ReadOnly);

  QStringList changelog_before;
  QStringList changelog_beyond;

  while (!qfile.atEnd()) {
    QByteArray line = qfile.readLine();

    if (line.endsWith('\n')) {
      line.chop(1);
    }

    do {
      QRegularExpressionMatch match;

      if ((match = title_starts_.match(line)).hasMatch()) {
        LVD_LOG_D() << section()
                    << "title starts"
                    << line;

        QStringList changelog_buffer;
        changelog_buffer.append(line);

        QString     title_data { line };
        line = handle_title(qfile, title_data, match, changelog_buffer);

        if      (!beyond_ && !inside_) {
          for (QString& line : (changelog_buffer)) {
            changelog_before.append(line);
          }
        }
        else if ( beyond_) {
          for (QString& line : (changelog_buffer)) {
            changelog_beyond.append(line);
          }
        }
      }
      else {
        if      (!beyond_ && !inside_) {
          changelog_before.append(line);
        }
        else if ( beyond_) {
          changelog_beyond.append(line);
        }

        line.clear();
        break;
      }
    } while (!line.isNull());
  }

  QSaveFile ofile(changelog_);
  ofile.open(QIODevice::WriteOnly);

  if (!ofile.isOpen()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "cannot save changelog"
                        << changelog_;

    LVD_THROW_RUNTIME(message);
  }

  bool something_done = false;

  if (!changelog_before.isEmpty()) {
    LVD_LOG_D() << section()
                << "saving changelog before";

    something_done = true;

    for (QString& line : changelog_before) {
      ofile.write(line.toUtf8());
      ofile.write("\n");
    }
  }

  Version version = make_version();
  if (!version.valid()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "missing version";

    LVD_THROW_RUNTIME(message);
  }

  QString changelog = changelog_formatter(storage(),
                                          version,
                                          title_format_,
                                          group_format_,
                                          entry_format_);

  if (!changelog.isEmpty()) {
    LVD_LOG_D() << section()
                << "saving changelog";

    something_done = true;

    ofile.write(changelog.toUtf8());
  }

  if (!changelog_beyond.isEmpty()) {
    LVD_LOG_D() << section()
                << "saving changelog beyond";

    something_done = true;

    for (QString& line : changelog_beyond) {
      ofile.write(line.toUtf8());
      ofile.write("\n");
    }
  }

  if (something_done) {
    ofile.commit();
  }

  return true;
}

// ----------

QByteArray ChangelogPublisher::handle_title(QFile& qfile, QString    & title_data,
                                            const QRegularExpressionMatch& title_match,
                                            QStringList& changelog_buffer) {
  LVD_LOG_T() << section();

  QByteArray line;

  if      (QStringRef title_texts = title_match.capturedRef("title");
           !title_texts.isNull()) {
    title_data = title_texts.toString();
  }
  else if (QStringRef title_texts = title_match.capturedRef(   1   );
           !title_texts.isNull()) {
    title_data = title_texts.toString();
  }

  while (!qfile.atEnd()) {
    line = qfile.readLine();

    if (line.endsWith('\n')) {
      line.chop(1);
    }

    QRegularExpressionMatch alter_match;

    if (   title_inside_alive_
        && (alter_match = title_inside_.match(line)).hasMatch()) {
      LVD_LOG_D() << section()
                  << "title inside"
                  << line;

      if      (QStringRef alter_texts = alter_match.capturedRef("title");
               !alter_texts.isNull()) {
        title_data.append('\n');
        title_data.append(alter_texts.toString());
      }
      else if (QStringRef alter_texts = alter_match.capturedRef(   1   );
               !alter_texts.isNull()) {
        title_data.append('\n');
        title_data.append(alter_texts.toString());
      }
      else {
        title_data.append('\n');
        title_data.append(line);
      }

      changelog_buffer.append(line);
      continue;
    }

    break;
  }

  QRegularExpression& rexpr = title_parser_alive_ ? title_parser_
                                                  : title_starts_;

  auto match = rexpr.match(title_data);
  if (!match.hasMatch()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "title expression"
                        << rexpr.pattern()
                        << "doesn't match"
                        << LVD_LOGLINE(title_data);

    LVD_THROW_RUNTIME(message);
  }

  Version version;

  if (!version.valid()) {
    LVD_LOG_D() << section()
                << "extracting version by value from"
                << LVD_LOGLINE(title_data);

    bool    major_ok;
    quint32 major = match.capturedRef("major").toUInt(&major_ok);

    bool    minor_ok;
    quint32 minor = match.capturedRef("minor").toUInt(&minor_ok);

    bool    patch_ok;
    quint32 patch = match.capturedRef("patch").toUInt(&patch_ok);

    if (   major_ok
        && minor_ok
        && patch_ok) {
      version = Version(major, minor, patch,
                        storage()->version_format());
    }
  }

  if (!version.valid()) {
    LVD_LOG_D() << section()
                << "extracting version by descr from"
                << LVD_LOGLINE(title_data);

    QStringRef vtext = match.capturedRef("version");
    if (!vtext.isNull()) {
      version = Version(vtext.toString(),
                        storage()->version_format());
    }
  }

  if (!version.valid()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "cannot extract version from"
                        << LVD_LOGLINE(title_data);

    LVD_THROW_RUNTIME(message);
  }

  Version next_version = storage()->next_version();
  if (next_version.valid()) {
    LVD_LOG_D() << section()
                << "comparing to next version"
                << next_version.to_string();

    inside_ = (next_version == version);
    beyond_ = (next_version >  version);
  }
  else {
    Version curr_version = storage()->curr_version();
    if (curr_version.valid()) {
      LVD_LOG_D() << section()
                  << "comparing to curr version"
                  << curr_version.to_string();

      inside_ = (curr_version == version);
      beyond_ = (curr_version >  version);
    }
    else {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section()
                          << "missing version";

      LVD_THROW_RUNTIME(message);
    }
  }

  return line;
}

}  // namespace lvd::release::handlers
