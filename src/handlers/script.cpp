/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "script.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QProcess>
#include <QStringList>

#include "lvd/shield.hpp"

#include "handler_settings.hpp"

// ----------

namespace {

QString escape(QString string) {
  return string.replace("\"", "\"\"\"");
}

}  // namespace

// ----------

namespace lvd::release::handlers {

Script::Script(const QString&    section,
               const StoragePtr& storage,
                     QSettings&  settings,
                     QObject*    parent)
    : Handler(Handles(),
              section,
              storage,
              settings,
              parent) {
  LVD_LOG_T() << section;

  Handler::Settings handler_settings(settings);

  command_ = handler_settings.required<QString>("command");
  LVD_LOG_D() << section << "command:" << command_;
}

Script::~Script() {
  LVD_LOG_T() << section();
}

// ----------

bool Script::handle_before_impl() {
  LVD_LOG_T() << section();

  QString command = command_;

  create_process();
  qprocess_->start(command);

  LVD_LOG_D() << section()
              << "execute"
              << qprocess_->program()
              << qprocess_->arguments();

  create_hard_timer();
  create_soft_timer();

  return false;
}

bool Script::handle_commit_impl(const Commit& commit)  {
  LVD_LOG_T() << section();

  QString command = command_;

  if (!commit.subject().isEmpty()) {
    QString subject = escape(commit.subject());

    command.append(" \"")
           .append(subject)
           .append("\"");
  }

  if (!commit.message().isEmpty()) {
    QString message = escape(commit.message());

    command.append(" \"")
           .append(message)
           .append("\"");
  }

  create_process();
  qprocess_->start(command);

  LVD_LOG_D() << section()
              << "execute"
              << qprocess_->program()
              << qprocess_->arguments();

  create_hard_timer();
  create_soft_timer();

  return false;
}

bool Script::handle_beyond_impl()  {
  LVD_LOG_T() << section();

  QString command = command_;

  create_process();
  qprocess_->start(command);

  LVD_LOG_D() << section()
              << "execute"
              << qprocess_->program()
              << qprocess_->arguments();

  create_hard_timer();
  create_soft_timer();

  return false;
}

// ----------

void Script::on_finished() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  Handler::on_finished();

  if (qprocess_) {
    LVD_LOG_D() << section()
                << "process result"
                << qprocess_->program()
                << qprocess_->arguments()
                << qprocess_->exitStatus()
                << qprocess_->exitCode();

    if (   qprocess_->exitStatus() == QProcess::NormalExit
        && qprocess_->exitCode()   == 0) {
      emit success();
      return;
    }

    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "process failed"
                        << qprocess_->program()
                        << qprocess_->arguments()
                        << qprocess_->exitStatus()
                        << qprocess_->exitCode()
                        << stderr_;

    emit failure(message);
    return;
  }

  LVD_THROW_IMPOSSIBLE;

  LVD_SHIELD_FUN(&Script::failure);
}

}  // namespace lvd::release::handlers
