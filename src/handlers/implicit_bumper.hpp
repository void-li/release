/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QRegularExpression>
#include <QSettings>
#include <QString>

#include "lvd/logger.hpp"

#include "commit.hpp"
#include "handler.hpp"
#include "implicit_bump.hpp"
#include "storage.hpp"

// ----------

namespace lvd::release::handlers {

class ImplicitBumper : public Handler {
  Q_OBJECT LVD_LOGGER

 public:
  ImplicitBumper        (const QString&    section,
                         const StoragePtr& storage,
                               QSettings&  settings,
                               QObject*    parent = nullptr);

  ~ImplicitBumper() override;

  static
  ImplicitBumper* create(const QString&    section,
                         const StoragePtr& storage,
                               QSettings&  settings,
                               QObject*    parent = nullptr) {
    return new ImplicitBumper(section, storage, settings, parent);
  }

 private:
  bool handle_commit_impl(const Commit& commit) override;

 private:
  ImplicitBump implicit_bump_;

  QRegularExpression match_subject_;
  bool               match_subject_alive_ = false;

  QRegularExpression match_message_;
  bool               match_message_alive_ = false;
};

DECLARE_HANDLER(ImplicitBumper, implicit_bumper);

}  // namespace lvd::release::handlers
