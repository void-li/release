/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "verifier.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QProcess>
#include <QStringList>

#include "lvd/shield.hpp"

#include "handler_settings.hpp"

// ----------

namespace lvd::release::handlers {

Verifier::Verifier(const QString&    section,
                   const StoragePtr& storage,
                         QSettings&  settings,
                         QObject*    parent)
    : Handler(Handles::Before,
              section,
              storage,
              settings,
              parent) {
  LVD_LOG_T() << section;

  Handler::Settings handler_settings(settings);

  options_ = handler_settings.optional<QString>("options", "HEAD");
  LVD_LOG_D() << section << "options:" << options_;
}

Verifier::~Verifier() {
  LVD_LOG_T() << section();
}

// ----------

bool Verifier::handle_before_impl() {
  LVD_LOG_T() << section();

  QString options = options_;

  QString command = QStringList({
    "git", "verify-commit", options
  }).join(" ").trimmed();

  create_process();
  qprocess_->start(command);

  LVD_LOG_D() << section()
              << "execute"
              << qprocess_->program()
              << qprocess_->arguments();

  create_hard_timer();
  create_soft_timer();

  return false;
}

// ----------

void Verifier::on_finished() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  Handler::on_finished();

  if (qprocess_) {
    LVD_LOG_D() << section()
                << "process result"
                << qprocess_->program()
                << qprocess_->arguments()
                << qprocess_->exitStatus()
                << qprocess_->exitCode();

    if (   qprocess_->exitStatus() == QProcess::NormalExit
        && qprocess_->exitCode()   == 0) {
      emit success();
      return;
    }

    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "process failed"
                        << qprocess_->program()
                        << qprocess_->arguments()
                        << qprocess_->exitStatus()
                        << qprocess_->exitCode()
                        << stderr_;

    emit failure(message);
    return;
  }

  LVD_THROW_IMPOSSIBLE;

  LVD_SHIELD_FUN(&Verifier::failure);
}

}  // namespace lvd::release::handlers
