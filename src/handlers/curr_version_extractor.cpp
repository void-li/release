/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "curr_version_extractor.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QProcess>
#include <QStringList>

#include "lvd/shield.hpp"

#include "version.hpp"

// ----------

namespace lvd::release::handlers {

CurrVersionExtractor::CurrVersionExtractor(const QString&    section,
                                           const StoragePtr& storage,
                                                 QSettings&  settings,
                                                 QObject*    parent)
    : Handler(Handles::Before,
              section,
              storage,
              settings,
              parent) {
  LVD_LOG_T() << section;
}

CurrVersionExtractor::~CurrVersionExtractor() {
  LVD_LOG_T() << section();
}

// ----------

bool CurrVersionExtractor::handle_before_impl() {
  LVD_LOG_T() << section();

  QString version_format = storage()->version_format();
  LVD_LOG_D() << section()
              << "version_format"
              << version_format;

  if (version_format.isEmpty()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "missing version format";

    emit failure(message);
    return true;
  }

  version_format.replace("%{major}", "*")
                .replace("%{minor}", "*")
                .replace("%{patch}", "*");



  create_process();
  qprocess_->start("git", { "describe", "--match", version_format, "--exact-match" });





  LVD_LOG_D() << section()
              << "execute"
              << qprocess_->program()
              << qprocess_->arguments();

  create_hard_timer();
  create_soft_timer();

  return false;
}

// ----------

void CurrVersionExtractor::on_finished() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  Handler::on_finished();

  if (qprocess_) {
    LVD_LOG_D() << section()
                << "process result"
                << qprocess_->program()
                << qprocess_->arguments()
                << qprocess_->exitStatus()
                << qprocess_->exitCode();

    if (   qprocess_->exitStatus() == QProcess::NormalExit
        && qprocess_->exitCode()   == 0) {
      Version version(stdout_.trimmed(),
                      storage()->version_format());

      LVD_LOG_D() << section()
                  << "curr version"
                  << version.to_string();

      storage()->set_curr_version(version);
    }

    emit success();
    return;
  }

  LVD_THROW_IMPOSSIBLE;

  LVD_SHIELD_FUN(&CurrVersionExtractor::failure);
}

}  // namespace lvd::release::handlers
