/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QSettings>
#include <QString>

#include "lvd/logger.hpp"

#include "handler.hpp"
#include "storage.hpp"

// ----------

namespace lvd::release::handlers {

class Tagger : public Handler {
  Q_OBJECT LVD_LOGGER

 public:
  Tagger        (const QString&    section,
                 const StoragePtr& storage,
                       QSettings&  settings,
                       QObject*    parent = nullptr);

  ~Tagger() override;

  static
  Tagger* create(const QString&    section,
                 const StoragePtr& storage,
                       QSettings&  settings,
                       QObject*    parent = nullptr) {
    return new Tagger(section, storage, settings, parent);
  }

 private:
  bool handle_beyond_impl() override;

 private slots:
  void on_finished() override;

 private:
  QString options_;
};

DECLARE_HANDLER(Tagger, tagger);

}  // namespace lvd::release::handlers
