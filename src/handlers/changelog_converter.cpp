/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "changelog_converter.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <QStringList>
#include <QStringRef>

#include "formatter.hpp"
#include "handler_settings.hpp"

// ----------

namespace lvd::release::handlers {

ChangelogConverter::ChangelogConverter(const QString&    section,
                                       const StoragePtr& storage,
                                             QSettings&  settings,
                                             QObject*    parent)
    : Handler(Handles::Beyond,
              section,
              storage,
              settings,
              parent) {
  LVD_LOG_T() << section;

  Handler::Settings handler_settings(settings);

  auto match_subject = handler_settings.optional<QString>("match_subject");
  if (!match_subject.isEmpty()) {
    LVD_LOG_D() << section
                << "match_subject:"
                << match_subject;

    match_subject_.setPattern(match_subject);
    match_subject_alive_ = true;

    if (!match_subject_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid match_subject"
                          << match_subject
                          << match_subject_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  if (!match_subject.isEmpty()) {
    match_subject_format_ = handler_settings.optional<QString>("match_subject_format");
    LVD_LOG_D() << section << "match_subject_format:" << match_subject_format_;
  }

  auto match_message = handler_settings.optional<QString>("match_message");
  if (!match_message.isEmpty()) {
    LVD_LOG_D() << section
                << "match_message:"
                << match_message;

    match_message_.setPattern(match_message);
    match_message_alive_ = true;

    if (!match_message_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid match_message"
                          << match_message
                          << match_message_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  if (!match_message.isEmpty()) {
    match_message_format_ = handler_settings.optional<QString>("match_message_format");
    LVD_LOG_D() << section << "match_message_format:" << match_message_format_;
  }

  if (   !match_subject_alive_
      && !match_message_alive_) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section
                        << "missing match_subject and match_message";

    LVD_THROW_RUNTIME(message);
  }
}

ChangelogConverter::~ChangelogConverter() {
  LVD_LOG_T() << section();
}

// ----------

bool ChangelogConverter::handle_beyond_impl() {
  LVD_LOG_T() << section();

  for (int i = 0; i < storage()->changelog_size(); i ++) {
    for (int j = 0; j < storage()->changelog_group_size(i); j ++) {
      if (match_subject_alive_) {
        QString subject = storage()->changelog_entry_subject(i, j);
        subject = convert(match_subject_, match_subject_format_, subject);
        storage()->set_changelog_entry_subject(i, j, subject);
      }

      if (match_message_alive_) {
        QString message = storage()->changelog_entry_message(i, j);
        message = convert(match_message_, match_message_format_, message);
        storage()->set_changelog_entry_message(i, j, message);
      }
    }
  }

  return true;
}

// ----------

QString ChangelogConverter::convert(const QRegularExpression& match_thingy,
                                    const QString&            match_thingy_format,
                                    const QString&                  thingy) {
  LVD_LOG_T() << section();

  QString result;
  int     pos = 0;

  bool logged = false;

  auto   match_it = match_thingy.globalMatch(thingy);
  while (match_it.hasNext()) {
    auto match = match_it.next();

    if (!logged) {
      LVD_LOG_D() << section()
                  << "converting"
                  << LVD_LOGLINE(thingy);

      logged = true;
    }

    int beg = match.capturedStart();
    if (beg > pos) {
      QStringRef data = thingy.midRef(pos, beg - pos);
      result.append(data);
    }

    Formatter formatter;
    QStringList names = match_thingy.namedCaptureGroups();

    for (const QString& name : names) {
      if (!name.isEmpty()) {
        const QString  text = match.captured(name);
        formatter.append(name, text);
      }
    }

    QString    data = formatter.format(match_thingy_format);
    result.append(data);

    int end = match.capturedEnd();
    if (end > pos) {
      pos = end;
    }
  }

  if (pos < thingy.size()) {
    QStringRef data = thingy.midRef(pos);
    result.append(data);
  }

  return result;
}

}  // namespace lvd::release::handlers
