/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "explicit_bumper.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QRegularExpressionMatch>
#include <QStringRef>

#include "handler_settings.hpp"

// ----------

namespace lvd::release::handlers {

ExplicitBumper::ExplicitBumper(const QString&    section,
                               const StoragePtr& storage,
                                     QSettings&  settings,
                                     QObject*    parent)
    : Handler(Handles::Commit,
              section,
              storage,
              settings,
              parent) {
  LVD_LOG_T() << section;

  Handler::Settings handler_settings(settings);

  auto explicit_bump = handler_settings.required<QString>("explicit_bump");
  if (!explicit_bump.isEmpty()) {
    LVD_LOG_D() << section
                << "explicit_bump:"
                << explicit_bump;

    explicit_bump_ = ExplicitBump::From_String(
        explicit_bump.toLocal8Bit().data());
  }

  auto match_subject = handler_settings.optional<QString>("match_subject");
  if (!match_subject.isEmpty()) {
    LVD_LOG_D() << section
                << "match_subject:"
                << match_subject;

    match_subject_.setPattern(match_subject);
    match_subject_alive_ = true;

    if (!match_subject_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid match_subject"
                          << match_subject
                          << match_subject_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  auto match_message = handler_settings.optional<QString>("match_message");
  if (!match_message.isEmpty()) {
    LVD_LOG_D() << section
                << "match_message:"
                << match_message;

    match_message_.setPattern(match_message);
    match_message_alive_ = true;

    if (!match_message_.isValid()) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << section
                          << "invalid match_message"
                          << match_message
                          << match_message_.errorString();

      LVD_THROW_RUNTIME(message);
    }
  }

  if (   !match_subject_alive_
      && !match_message_alive_) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section
                        << "missing match_subject and match_message";

    LVD_THROW_RUNTIME(message);
  }
}

ExplicitBumper::~ExplicitBumper() {
  LVD_LOG_T() << section();
}

// ----------

bool ExplicitBumper::handle_commit_impl(const Commit& commit) {
  LVD_LOG_T() << section();

  if (first_commit_ != false) {
    first_commit_  = false;
  } else { return true; }

  Q_ASSERT(explicit_bump_.valid());

  if (match_subject_alive_) {
    auto match = match_subject_.match(commit.subject());
    if (match.hasMatch()) {
      LVD_LOG_D() << section()
                  << "matched subject"
                  << LVD_LOGLINE(commit.subject());

      storage()->bump_explicit(explicit_bump_);
      return true;
    }
  }

  if (match_message_alive_) {
    auto match = match_message_.match(commit.message());
    if (match.hasMatch()) {
      LVD_LOG_D() << section()
                  << "matched message"
                  << LVD_LOGLINE(commit.message());

      storage()->bump_explicit(explicit_bump_);
      return true;
    }
  }

  return true;
}

}  // namespace lvd::release::handlers
