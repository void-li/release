/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QByteArray>
#include <QFile>
#include <QObject>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QSettings>
#include <QString>
#include <QStringList>

#include "lvd/logger.hpp"

#include "handler.hpp"
#include "storage.hpp"

// ----------

namespace lvd::release::handlers {

class ChangelogPublisher : public Handler {
  Q_OBJECT LVD_LOGGER

 public:
  ChangelogPublisher        (const QString&    section,
                             const StoragePtr& storage,
                                   QSettings&  settings,
                                   QObject*    parent = nullptr);

  ~ChangelogPublisher() override;

  static
  ChangelogPublisher* create(const QString&    section,
                             const StoragePtr& storage,
                                   QSettings&  settings,
                                   QObject*    parent = nullptr) {
    return new ChangelogPublisher(section, storage, settings, parent);
  }

 private:
  bool handle_beyond_impl() override;

 private:
  QByteArray handle_title(QFile& qfile, QString    & title_data,
                          const QRegularExpressionMatch& title_match,
                          QStringList& changelog_buffer);

 private:
  QString changelog_;

  QRegularExpression title_starts_;

  QRegularExpression title_inside_;
  bool               title_inside_alive_ = false;

  QRegularExpression title_parser_;
  bool               title_parser_alive_ = false;

  QString title_format_;
  QString group_format_;
  QString entry_format_;

 private:
  QString title_;
  QString group_;

  bool inside_ = false;
  bool beyond_ = false;
};

DECLARE_HANDLER(ChangelogPublisher, changelog_publisher);

}  // namespace lvd::release::handlers
