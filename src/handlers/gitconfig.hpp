/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QPair>
#include <QSettings>
#include <QString>
#include <QVector>

#include "lvd/logger.hpp"

#include "commit.hpp"
#include "handler.hpp"
#include "storage.hpp"

// ----------

namespace lvd::release::handlers {

class Gitconfig : public Handler {
  Q_OBJECT LVD_LOGGER

 public:
  Gitconfig        (const QString&    section,
                    const StoragePtr& storage,
                          QSettings&  settings,
                          QObject*    parent = nullptr);

  ~Gitconfig() override;

  static
  Gitconfig* create(const QString&    section,
                    const StoragePtr& storage,
                          QSettings&  settings,
                          QObject*    parent = nullptr) {
    return new Gitconfig(section, storage, settings, parent);
  }

 private:
  bool handle_before_impl()              override;
  bool handle_commit_impl(const Commit&) override;
  bool handle_beyond_impl()              override;

 private:
  bool handle_things_impl();

 private slots:
  void on_finished() override;

 private:
  using Config  =         QPair<QString, QString> ;
  using Configs = QVector<QPair<QString, QString>>;

  Configs configs_;
  int     current_ = -1;
};

DECLARE_HANDLER(Gitconfig, gitconfig);

}  // namespace lvd::release::handlers
