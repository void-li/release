/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "tagger.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QProcess>
#include <QStringList>

#include "lvd/shield.hpp"

#include "formatter.hpp"
#include "handler_settings.hpp"
#include "version.hpp"

// ----------

namespace {

QString escape(QString string) {
  return string.replace("\"", "\"\"\"") ;
}

}  // namespace

// ----------

namespace lvd::release::handlers {

Tagger::Tagger(const QString&    section,
               const StoragePtr& storage,
                     QSettings&  settings,
                     QObject*    parent)
    : Handler(Handles::Beyond,
              section,
              storage,
              settings,
              parent) {
  LVD_LOG_T() << section;

  Handler::Settings handler_settings(settings);

  options_ = handler_settings.required<QString>("options", "\"%{version}\"");
  LVD_LOG_D() << section << "options:" << options_;
}

Tagger::~Tagger() {
  LVD_LOG_T() << section();
}

// ----------

bool Tagger::handle_beyond_impl() {
  LVD_LOG_T() << section();

  Version version = make_version();
  if (!version.valid()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "missing version";

    emit failure(message);
    return true;
  }

  Formatter formatter;
  formatter.append("major", escape(QString::number(version.major())));
  formatter.append("minor", escape(QString::number(version.minor())));
  formatter.append("patch", escape(QString::number(version.patch())));
  formatter.append("version", escape(              version.to_string()));

  QString options = formatter.format(options_);

  QString command = QStringList({
    "git", "tag", "-a", "\"" % escape(version.to_string()) % "\"", "-m", options
  }).join(" ").trimmed();

  create_process();
  qprocess_->start(command);

  LVD_LOG_D() << section()
              << "execute"
              << qprocess_->program()
              << qprocess_->arguments();

  create_hard_timer();
  create_soft_timer();

  return false;
}

// ----------

void Tagger::on_finished() {
  LVD_LOG_T() << section();
  LVD_SHIELD;

  Handler::on_finished();

  if (qprocess_) {
    LVD_LOG_D() << section()
                << "process result"
                << qprocess_->program()
                << qprocess_->arguments()
                << qprocess_->exitStatus()
                << qprocess_->exitCode();

    if (   qprocess_->exitStatus() == QProcess::NormalExit
        && qprocess_->exitCode()   == 0) {
      emit success();
      return;
    }

    LVD_LOGBUF message;
    LVD_LOG_W(&message) << section()
                        << "process failed"
                        << qprocess_->program()
                        << qprocess_->arguments()
                        << qprocess_->exitStatus()
                        << qprocess_->exitCode()
                        << stderr_;

    emit failure(message);
    return;
  }

  LVD_THROW_IMPOSSIBLE;

  LVD_SHIELD_FUN(&Tagger::failure);
}

}  // namespace lvd::release::handlers
