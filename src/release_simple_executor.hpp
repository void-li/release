/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <functional>

#include <QObject>
#include <QString>

#include "lvd/logger.hpp"

#include "handler.hpp"
#include "release.hpp"

// ----------

namespace lvd::release {

class Release::SimpleExecutor : public QObject {
  Q_OBJECT LVD_LOGGER

 public:
  using Invoke = std::function<bool(Handler*)>;

 public:
  SimpleExecutor(const Handlers& simple_handlers,
                 Invoke          invoke,
                 QObject*        parent = nullptr);

  ~SimpleExecutor() override;

 public slots:
  bool execute();

 signals:
  void success(const QString& message = QString());
  void failure(const QString& message = QString());

 private:
  bool setup();
  bool shine();
  bool close();

 private slots:
  void on_handler_success(const QString& message);
  void on_handler_failure(const QString& message);

 private:
  Handlers simple_handlers_;
  Handlers queued_handlers_;

  bool asynchronous_ = false;
  bool failed_       = false;

  Invoke invoke_;
};

}  // namespace lvd::release
